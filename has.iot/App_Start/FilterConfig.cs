﻿using has.iot.Components;
using System.Web;
using System.Web.Mvc;

namespace has.iot
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new IsLoggedInAttribute());
            filters.Add(new HandleErrorAttribute());
        }
    }
}
