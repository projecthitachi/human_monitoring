﻿using has.iot.Components;
using has.iot.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace has.iot.Controllers
{
    public class UsersController : Controller
    {
        // GET: Users
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult UsersAdd()
        {
            IMongoDatabase database = Constant.client.GetDatabase(Constant.dbName);
            IMongoCollection<Moduls> Collections = database.GetCollection<Moduls>("Moduls");

            var vResult = Collections.Find(f => f.active == true).SortBy(s => s.order).ToList();

            ViewData["moduls"] = vResult;
            return View();
        }

        public ActionResult UsersDetail(string id)
        {
            IMongoDatabase database = Constant.client.GetDatabase(Constant.dbName);
            IMongoCollection<Moduls> Collections = database.GetCollection<Moduls>("Moduls");

            var vResult = Collections.Find(f => f.active == true).SortBy(s => s.order).ToList();

            ViewData["ActiveMenu"] = "Systems";
            ViewData["moduls"] = vResult;
            ViewData["id"] = id;
            return View();
        }

        //[HttpGet]
        public ActionResult UsersRead([DataSourceRequest] DataSourceRequest poRequest)
        {
            poRequest.Page = (poRequest.Page - 1) * poRequest.PageSize;
            IMongoDatabase database = Constant.client.GetDatabase(Constant.dbName);
            IMongoCollection<UsersGrid> Collections = database.GetCollection<UsersGrid>("Users");
            IMongoCollection<Users> Coll = database.GetCollection<Users>("Users");

            var filter = new BsonDocument() { };
            List<Users> res = Coll.Find(filter).ToList();
            return Json(res.ToDataSourceResult(poRequest), JsonRequestBehavior.AllowGet);

            /*var Flt = GlobalFunction.gridFilter<UsersGrid>(poRequest);
            var FltCount = GlobalFunction.gridFilter<Users>(poRequest);

            var sort = "{ $sort: { crtOn : 1} }";
            if (poRequest.Sorts.Count > 0)
            {
                string field = poRequest.Sorts[0].Member;
                string ord = poRequest.Sorts[0].SortDirection.ToString();
                int direction = (ord == "Descending") ? -1 : 1;
                sort = "{ $sort: {" + field + ": " + direction + ", _id : -1} }";
            }
            var project = "{ $project: {id : 1, usrId :1,usrName:1,usrPass:1,fstName:1,lstName:1,rlId:1,rlName:1,crtOn:1,crtBy:1,crtAt:1, uptOn: {$cond: {if : {$eq: [\"$uptOn\", ISODate(\"0001-01-01T00:00:00.000Z\")]},then: \"01/01/0001 00:00:00\",else : {\"$toString\": {$dateToString: {format: \"%m/%d/%Y %H:%M:%S\",date: \"$uptOn\",timezone: \"Asia/Singapore\"}}}}},uptAt:1,uptBy:1}}";
            var vResult = Collections.Aggregate().AppendStage<UsersGrid>(sort).AppendStage<UsersGrid>(project).Match(Flt).Skip(poRequest.Page).Limit(poRequest.PageSize).ToList();

            long Total = (poRequest.Filters.Count <= 0) ? Coll.CountDocuments(FltCount) : Coll.CountDocuments(FltCount);
            var gridRes = new GridResponses();
            gridRes.Data = vResult;
            gridRes.Total = Total;

            var jsonResult = Json(gridRes, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;*/
        }

        [HttpGet]
        public ActionResult UsersReadDetail(Users poRequest)
        {
            ApiResponses ResultData = new ApiResponses();
            try
            {
                Dictionary<string, object> response = new Dictionary<string, object>();
                IMongoDatabase database = Constant.client.GetDatabase(Constant.dbName);
                IMongoCollection<Users> Collections = database.GetCollection<Users>("Users");
                IMongoCollection<Role> CollRoles = database.GetCollection<Role>("Roles");

                var evntBld = Builders<Users>.Filter;
                var evntFlt = evntBld.Eq<long>("id", poRequest.id);
                var evntRes = Collections.Find(evntFlt).FirstOrDefault();
                if (evntRes == null)
                {
                    throw new NullReferenceException("User not found");
                }

                var RolesBld = Builders<Role>.Filter;
                var RolesFlt = RolesBld.Eq<string>("rlId", evntRes.rlId);
                var RolesRes = CollRoles.Find(RolesFlt).FirstOrDefault();
                if (RolesRes == null)
                {
                    throw new NullReferenceException("Roles not found");
                }

                response.Add("user", evntRes);
                response.Add("role", RolesRes);
                ResultData.errorcode = 0;
                ResultData.title = "Success";
                ResultData.message = "read Data success";
                ResultData.data = response;
            }
            catch (Exception ex)
            {
                ResultData.errorcode = 100;
                ResultData.title = "failed";
                ResultData.message = ex.Message;
            }
            return Json(ResultData, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult UsersCreate(Users voTable)
        {
            ApiResponses ResultData = new ApiResponses();
            var session = Constant.client.StartSession();
            try
            {
                IMongoDatabase database = session.Client.GetDatabase(Constant.dbName);
                IMongoCollection<Users> Collections = database.GetCollection<Users>("Users");


                if (voTable.usrName.Trim() == null) { throw new NullReferenceException("please insert user id!"); }
                if (voTable.usrPass.Trim() == null) { throw new NullReferenceException("please insert password!"); }
                if (voTable.rlId.Trim() == null) { throw new NullReferenceException("please insert role!"); }
                string RoleId = System.Web.HttpContext.Current.Session["RoleID"] as string;
                if (RoleId != "R001")
                {
                    if (voTable.rlId == "R001") { throw new NullReferenceException("You cannot create users with admin roles"); }
                }
                voTable.usrName = voTable.usrName;

                var evntBld = Builders<Users>.Filter;
                var evntFlt = evntBld.Eq<string>("usrName", voTable.usrName);
                var evntRes = Collections.Find(evntFlt).FirstOrDefault();
                if (evntRes != null)
                {
                    throw new NullReferenceException("The User Id you have requested is already in use. please try another name");
                }
                if (voTable.usrName == null || voTable.usrName == "")
                {
                    throw new NullReferenceException("User Id Cannot Empty");
                }
                if (voTable.fstName.Trim() == null || voTable.fstName.Trim() == "")
                {
                    throw new NullReferenceException("Username Cannot Empty");
                }
                if (voTable.usrName.Length > 30)
                {
                    throw new NullReferenceException("User Id is too long max 30 character");
                }
                if (voTable.fstName.Length > 50)
                {
                    throw new NullReferenceException("Username is too long max 50 character");
                }
                if (voTable.usrName.Length > 0)
                {
                    if (Constant.formulaInj.Contains(voTable.usrName[0]))
                    {
                        throw new NullReferenceException("formula detected!");
                    }
                }
                if (voTable.fstName.Length > 0)
                {
                    if (Constant.formulaInj.Contains(voTable.fstName[0]))
                    {
                        throw new NullReferenceException("formula detected!");
                    }
                }
                voTable.usrName = Server.HtmlEncode(voTable.usrName);
                voTable.fstName = Server.HtmlEncode(voTable.fstName);

                string patternNumeric = @"^(?=(.*\d){1}).{1,}$";
                string patternLetter = @"^(?=.*[a-z])(?=.*[A-Z]).{1,}$";
                string patternspecial = @"^(?=.*[^a-zA-Z\d]).{1,}$";
                if (!Regex.IsMatch(voTable.usrPass, patternNumeric))
                {
                    throw new NullReferenceException(String.Format("The Password must have at least one numeric"));
                }
                if (!Regex.IsMatch(voTable.usrPass, patternLetter))
                {
                    throw new NullReferenceException(String.Format("The Password must have at least one uppercase and one lowercase "));
                }
                if (!Regex.IsMatch(voTable.usrPass, patternspecial))
                {
                    throw new NullReferenceException(String.Format("The Password must have at least one special character"));
                }
                if (String.IsNullOrEmpty(voTable.usrPass) || voTable.usrPass.Length < 8)
                {
                    throw new NullReferenceException(String.Format("Minimum Password Length Required is:{0}", 8));
                }
                else if (voTable.usrPass.Length > 128)
                {
                    throw new NullReferenceException(String.Format("Maximum Password Length is:{0}", 128));
                }

                voTable.id = new Sequence() { }.GetNextSequenceValue("Users", Constant.dbName);
                voTable.crtOn = DateTime.Now;
                voTable.usrId = "USR" + voTable.id.ToString("D3");
                voTable.usrPass = Constant.MD5Hash(voTable.usrPass);
                Collections.InsertOne(session, voTable);

                ResultData.errorcode = 0;
                ResultData.title = "Success";
                ResultData.message = "Add Users success";
                ResultData.data = voTable;
            }
            catch (Exception ex)
            {
                ResultData.errorcode = 100;
                ResultData.title = "failed";
                ResultData.message = ex.Message;
            }
            return Json(ResultData, JsonRequestBehavior.DenyGet);
        }

        [HttpPost]
        public ActionResult UsersUpdate(Users poData, string usrPassConfirm, string usrPassCurrent)
        {
            ApiResponses ResultData = new ApiResponses();
            var session = Constant.client.StartSession();
            try
            {
                IMongoDatabase database = session.Client.GetDatabase(Constant.dbName);
                IMongoCollection<Users> Collections = database.GetCollection<Users>("Users");

                if (poData.usrName.Trim() == null) { throw new NullReferenceException("please insert user id!"); }
                if (poData.rlId.Trim() == null) { throw new NullReferenceException("please insert role!"); }

                string RoleId = System.Web.HttpContext.Current.Session["RoleID"] as string;
                if (RoleId != "R001")
                {
                    if (poData.rlId == "R001") { throw new NullReferenceException("You cannot update users with admin roles"); }
                }
                poData.usrName = poData.usrName.Trim();

                var evntBld = Builders<Users>.Filter;
                var evntFlt = evntBld.Eq<long>("id", poData.id);
                var evntRes = Collections.Find(evntFlt).FirstOrDefault();
                if (evntRes == null)
                {
                    throw new NullReferenceException("user not found!");
                }

                if (evntRes.uptOn.ToString("dd/MM/yyyy hh:mm:ss") != poData.uptOn.ToString("dd/MM/yyyy hh:mm:ss"))
                {
                    throw new NullReferenceException("Another user has modified or deleted this record.");
                }

                if (evntRes.usrName.Trim() != poData.usrName.Trim())
                {
                    var checkBld = Builders<Users>.Filter;
                    var checkFlt = checkBld.Eq<string>("usrName", poData.usrName);
                    var checkRes = Collections.Find(checkFlt).FirstOrDefault();
                    if (checkRes != null)
                    {
                        throw new NullReferenceException("the user id you have requested is already in use. please try another name");
                    }
                }
                if (poData.usrName == null || poData.usrName == "")
                {
                    throw new NullReferenceException("User Id Cannot Empty");
                }
                if (poData.fstName.Trim() == null || poData.fstName.Trim() == "")
                {
                    throw new NullReferenceException("Username Cannot Empty");
                }
                if (poData.usrName.Length > 30)
                {
                    throw new NullReferenceException("User Id is too long max 30 character");
                }
                if (poData.fstName.Length > 50)
                {
                    throw new NullReferenceException("Username is too long max 50 character");
                }
                if (poData.usrName.Length > 0)
                {
                    if (Constant.formulaInj.Contains(poData.usrName[0]))
                    {
                        throw new NullReferenceException("formula detected!");
                    }
                }
                if (poData.fstName.Length > 0)
                {
                    if (Constant.formulaInj.Contains(poData.fstName[0]))
                    {
                        throw new NullReferenceException("formula detected!");
                    }
                }
                poData.usrName = Server.HtmlEncode(poData.usrName);
                poData.fstName = Server.HtmlEncode(poData.fstName);

                var set = Builders<Users>.Update
                    .Set("uptOn", DateTime.Now)
                    .Set("uptBy", poData.uptBy)
                    .Set("uptAt", poData.uptAt)
                    .Set("fstName", poData.fstName)
                    .Set("lstName", poData.lstName)
                    .Set("usrName", poData.usrName)
                    .Set("rlId", poData.rlId)
                    .Set("rlName", poData.rlName);
                if (poData.usrPass != null)
                {
                    string Password = Constant.MD5Hash(usrPassCurrent);
                    if (Password != evntRes.usrPass)
                    {
                        throw new NullReferenceException("incorrect current password.!");
                    }
                    if (poData.usrPass != usrPassConfirm)
                    {
                        throw new NullReferenceException("password and confirm password does not match!");
                    }

                    string patternNumeric = @"^(?=(.*\d){1}).{1,}$";
                    string patternLetter = @"^(?=.*[a-z])(?=.*[A-Z]).{1,}$";
                    string patternspecial = @"^(?=.*[^a-zA-Z\d]).{1,}$";
                    if (!Regex.IsMatch(poData.usrPass, patternNumeric))
                    {
                        throw new NullReferenceException(String.Format("The Password must have at least one numeric"));
                    }
                    if (!Regex.IsMatch(poData.usrPass, patternLetter))
                    {
                        throw new NullReferenceException(String.Format("The Password must have at least one uppercase and one lowercase "));
                    }
                    if (!Regex.IsMatch(poData.usrPass, patternspecial))
                    {
                        throw new NullReferenceException(String.Format("The Password must have at least one special character"));
                    }
                    if (String.IsNullOrEmpty(poData.usrPass) || poData.usrPass.Length < 8)
                    {
                        throw new NullReferenceException(String.Format("Minimum Password Length Required is:{0}", 8));
                    }
                    else if (poData.usrPass.Length > 128)
                    {
                        throw new NullReferenceException(String.Format("Maximum Password Length is:{0}", 128));
                    }

                    set = Builders<Users>.Update
                        .Set("uptOn", DateTime.Now)
                        .Set("uptBy", poData.uptBy)
                        .Set("uptAt", poData.uptAt)
                        .Set("usrName", poData.usrName)
                        .Set("usrPass", Constant.MD5Hash(poData.usrPass))
                        .Set("fstName", poData.fstName)
                        .Set("lstName", poData.lstName)
                        .Set("rlId", poData.rlId)
                        .Set("rlName", poData.rlName);
                }
                Collections.UpdateOne(session, evntFlt, set);

                ResultData.errorcode = 0;
                ResultData.title = "Success";
                ResultData.message = "Update Users success";
                ResultData.data = poData;
            }
            catch (Exception ex)
            {
                ResultData.errorcode = 100;
                ResultData.title = "failed";
                ResultData.message = ex.Message;
            }
            return Json(ResultData, JsonRequestBehavior.DenyGet);
        }

        [HttpPost]
        public ActionResult UsersDelete(Users poData)
        {
            ApiResponses ResultData = new ApiResponses();
            var session = Constant.client.StartSession();
            try
            {
                IMongoDatabase database = session.Client.GetDatabase(Constant.dbName);
                IMongoCollection<Users> Collections = database.GetCollection<Users>("Users");

                var evntBld = Builders<Users>.Filter;
                var evntFlt = evntBld.Eq<long>("id", poData.id);
                var evntRes = Collections.Find(evntFlt).FirstOrDefault();
                if (evntRes == null)
                {
                    throw new NullReferenceException("user not found");
                }

                if (evntRes.usrId == "USR001") { throw new NullReferenceException("You cannot delete admin user"); }

                Collections.DeleteOne(session, evntFlt);

                ResultData.errorcode = 0;
                ResultData.title = "Success";
                ResultData.message = "Delete Data success";
                ResultData.data = evntRes;
            }
            catch (Exception ex)
            {
                ResultData.errorcode = 100;
                ResultData.title = "failed";
                ResultData.message = ex.Message;
            }
            return Json(ResultData, JsonRequestBehavior.DenyGet);
        }


        public ActionResult RolesDDL()
        {
            IMongoDatabase database = Constant.client.GetDatabase(Constant.dbName);
            IMongoCollection<Role> Collections = database.GetCollection<Role>("Roles");

            var vResult = Collections.Find(_ => true).ToList();
            var jsonResult = Json(vResult, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        public ActionResult RolesReadDetail(string id)
        {
            IMongoDatabase database = Constant.client.GetDatabase(Constant.dbName);
            IMongoCollection<Role> Collections = database.GetCollection<Role>("Roles");

            var vResult = Collections.Find(x => x.rlId == id).FirstOrDefault();
            if (vResult == null)
            {
                vResult = Collections.Find(x => x.id == Convert.ToInt64(id)).FirstOrDefault();
            }
            var jsonResult = Json(vResult, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }
    }
}