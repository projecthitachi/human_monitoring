﻿using has.iot.Components;
using has.iot.Models;
using has.iot.Models.System;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using MongoDB.Driver;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using uPLibrary.Networking.M2Mqtt.Messages;

namespace has.iot.Controllers
{
    public class SystemsController : Controller
    {
        // GET: Systems
        public ActionResult Login()
        {
            return View();
        }
        public ActionResult Logout()
        {
            HttpContext.Session.Clear();
            sessionClear();
            return RedirectToAction("Login", "Systems");
        }
        private void sessionClear()
        {
            if (Constant.bypass == "false")
            {
                System.Web.HttpContext.Current.Session.Clear();
                Session.Abandon();
                System.Web.HttpContext.Current.Request.Cookies.Clear();
            }
        }
        public ActionResult User()
        {
            return View();
        }

        public ActionResult GetUserList([DataSourceRequest]DataSourceRequest poRequest)
        {
            Dictionary<string, string> ResultData = new Dictionary<string, string>();
            IMongoDatabase database = Constant.client.GetDatabase(Constant.dbName);
            var CollectDevice = database.GetCollection<UsersView_REC>("Users");
            var insertDoc = database.GetCollection<Users_REC>("Users");

            var builder = Builders<UsersView_REC>.Filter;
            var filter = builder.Eq<int>("recordStatus", 1);
            var vResult = CollectDevice.Find(filter).ToList();

            var jsonResult = Json(vResult.ToDataSourceResult(poRequest));
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        public async Task<ActionResult> Ping(string domain)
        {
            bool pingable = false;
            Ping pinger = null;

            try
            {
                pinger = new Ping();
                PingReply reply = pinger.Send(domain);
                pingable = reply.Status == IPStatus.Success;
            }
            catch (PingException)
            {
                // Discard PingExceptions and return false;
            }
            finally
            {
                if (pinger != null)
                {
                    pinger.Dispose();
                }
            }

            return Json(new { msg = pingable }, JsonRequestBehavior.AllowGet);
        }
    }
}