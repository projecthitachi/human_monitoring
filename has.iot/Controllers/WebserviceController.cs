﻿using has.iot.Components;
using has.iot.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Security.Cryptography;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text;
using uPLibrary.Networking.M2Mqtt.Messages;

namespace has.iot.Controllers
{
    public class WebserviceController : Controller
    {
        public ActionResult Login(string uuid, string password)
        {
            sessionClear();
            Dictionary<string, string> ResultData = new Dictionary<string, string>();
            try
            {
                IMongoDatabase database = Constant.client.GetDatabase(Constant.dbName);
                var CollectDevice = database.GetCollection<Users>("Users");
                var collection = database.GetCollection<Persons>("Person");
                IMongoCollection<Role> CollRole = database.GetCollection<Role>("Roles");

                password = Constant.MD5Hash(password);
                var res = CollectDevice.Find(x => x.usrName == uuid && x.usrPass == password).FirstOrDefault();

                if (res != null)
                {

                    var ExistRole = CollRole.Find(x => x.rlId == res.rlId).FirstOrDefault();
                    System.Web.HttpContext.Current.Session["UserID"] = res.usrId;
                    System.Web.HttpContext.Current.Session["Username"] = res.usrName;
                    System.Web.HttpContext.Current.Session["FirstName"] = res.fstName;
                    System.Web.HttpContext.Current.Session["Name"] = res.fstName + res.lstName;
                    System.Web.HttpContext.Current.Session["Password"] = res;
                    System.Web.HttpContext.Current.Session["RoleId"] = res.rlId;
                    System.Web.HttpContext.Current.Session["RoleName"] = res.rlName;
                    System.Web.HttpContext.Current.Session["accessMod"] = ExistRole.acsMod;
                    System.Web.HttpContext.Current.Session["UserAgent"] = System.Web.HttpContext.Current.Request.UserAgent;
                    System.Web.HttpContext.Current.Session["IPAddress"] = Constant.GetIPAddress();

                    ResultData.Add("data", JsonConvert.SerializeObject(res).ToString());
                    ResultData.Add("msg", "Hi "+ uuid + ", Thank you very much");
                    ResultData.Add("errorcode", "0");
                    ResultData.Add("title", "Login Success");
                }
                else
                {
                    ResultData.Add("msg", "this username " + uuid + " is not exist or password is wrong");
                    ResultData.Add("errorcode", "100");
                    ResultData.Add("title", "login failed");
                }
            }
            catch (Exception e)
            {
                ResultData.Add("msg", e.Message);
                ResultData.Add("errorcode", "100");
                ResultData.Add("title", "login failed");
            }
            return Json(ResultData, JsonRequestBehavior.AllowGet);
        }
        private void sessionClear()
        {
            if (Constant.bypass == "false")
            {
                System.Web.HttpContext.Current.Session.Clear();
                Session.Abandon();
                System.Web.HttpContext.Current.Request.Cookies.Clear();
            }
        }
        public ActionResult Register(Users_REC poData)
        {
            Dictionary<string, string> ResultData = new Dictionary<string, string>();
            try
            {
                IMongoDatabase database = Constant.client.GetDatabase(Constant.dbName);
                var CollectDevice = database.GetCollection<UsersView_REC>("Users");
                var insertDoc = database.GetCollection<Users_REC>("Users");

                if (poData.uuid == null || poData.uuid == ""){
                    Random rnd = new Random();
                    int myRandomNo = rnd.Next(10000000, 99999999);
                    poData.uuid = myRandomNo.ToString();
                }
                var builder = Builders<UsersView_REC>.Filter;
                var filter = builder.Eq<string>("uuid", poData.uuid);
                var res = CollectDevice.Find(filter).SingleOrDefault();
                if (res == null)
                {
                    poData.recordStatus = 1;
                    poData.recordTimestamp = DateTime.Now;
                    poData.password = Constant.MD5Hash(poData.password);
                    insertDoc.InsertOne(poData);
                }
                else
                {
                    var update = Builders<UsersView_REC>.Update.Set("birthDate", poData.birthDate).Set("age", poData.age)
                        .Set("gender", poData.gender).Set("race", poData.race).Set("raceSpecify", poData.raceSpecify)
                        .Set("height", poData.height).Set("weight", poData.weight);
                    CollectDevice.UpdateOne(filter, update);
                }

                ResultData.Add("msg", "Hi " + poData.uuid + ", Thank you very much you are registered");
                ResultData.Add("errorcode", "0");
                ResultData.Add("title", "Register Success");
            }
            catch (Exception e)
            {
                ResultData.Add("msg", e.Message);
                ResultData.Add("errorcode", "100");
                ResultData.Add("title", "Register failed");
            }
            return Json(ResultData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult UpdateProfile(Users_REC poData)
        {
            Dictionary<string, string> ResultData = new Dictionary<string, string>();
            try
            {
                IMongoDatabase database = Constant.client.GetDatabase(Constant.dbName);
                var CollectDevice = database.GetCollection<UsersView_REC>("Users");
                var insertDoc = database.GetCollection<Users_REC>("Users");

                var builder = Builders<UsersView_REC>.Filter;
                var filter = builder.Eq<string>("uuid", poData.uuid);
                var res = CollectDevice.Find(filter).SingleOrDefault();

                if (res != null)
                {
                    if (poData.password == null || poData.password == "")
                    {
                        var update = Builders<UsersView_REC>.Update.Set("birthDate", poData.birthDate).Set("age", poData.age)
                            .Set("gender", poData.gender).Set("race", poData.race).Set("raceSpecify", poData.raceSpecify)
                            .Set("height", poData.height).Set("weight", poData.weight);
                        CollectDevice.UpdateOne(filter, update);
                    }
                    else
                    {
                        poData.password = Constant.MD5Hash(poData.password);
                        var update = Builders<UsersView_REC>.Update.Set("password", poData.password).Set("birthDate", poData.birthDate).Set("age", poData.age)
                            .Set("gender", poData.gender).Set("race", poData.race).Set("raceSpecify", poData.raceSpecify)
                            .Set("height", poData.height).Set("weight", poData.weight);
                        CollectDevice.UpdateOne(filter, update);
                    }

                    ResultData.Add("msg", "profile updated");
                    ResultData.Add("errorcode", "0");
                    ResultData.Add("title", "Update Success");
                }
                else
                {
                    ResultData.Add("msg", "this uuid " + poData.uuid + " is not exist");
                    ResultData.Add("errorcode", "100");
                    ResultData.Add("title", "Update failed");
                }
            }
            catch (Exception e)
            {
                ResultData.Add("msg", e.Message);
                ResultData.Add("errorcode", "100");
                ResultData.Add("title", "Update failed");
            }
            return Json(ResultData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult getUser(string uuid)
        {
            Dictionary<string, string> ResultData = new Dictionary<string, string>();
            try
            {
                IMongoDatabase database = Constant.client.GetDatabase(Constant.dbName);
                var CollectDevice = database.GetCollection<UsersView_REC>("Users");
                var insertDoc = database.GetCollection<Users_REC>("Users");

                var builder = Builders<UsersView_REC>.Filter;
                var filter = builder.Eq<string>("uuid", uuid);
                var res = CollectDevice.Find(filter).SingleOrDefault();
                if (res != null)
                {
                    string strValue = Convert.ToString(JsonConvert.SerializeObject(res));
                    ResultData.Add("data", strValue);
                    ResultData.Add("msg", "profile updated");
                    ResultData.Add("errorcode", "0");
                    ResultData.Add("title", "Update Success");
                }
                else
                {
                    ResultData.Add("msg", "this uuid " + uuid + " is not exist");
                    ResultData.Add("errorcode", "100");
                    ResultData.Add("title", "Update failed");
                }
            }
            catch (Exception e)
            {
                ResultData.Add("msg", e.Message);
                ResultData.Add("errorcode", "100");
                ResultData.Add("title", "Update failed");
            }
            return Json(ResultData, JsonRequestBehavior.AllowGet);
        }

        public class submiFeedback {
            public FeedbackView_REC feedbackData { get; set;}
            public List<QuestionsAnswerFeedback_REC> qaData { get; set; }
        }
        public ActionResult Feedback(submiFeedback listData)
        {
            FeedbackView_REC poData = listData.feedbackData;
            List<QuestionsAnswerFeedback_REC> qaData = listData.qaData;
            
            Dictionary<string, string> ResultData = new Dictionary<string, string>();
            IMongoDatabase database = Constant.client.GetDatabase(Constant.dbName);
            var insertDoc = database.GetCollection<FeedbackView_REC>("UserFeedback");
            var insertDocQA = database.GetCollection<QuestionsAnswerFeedback_REC>("UserFeedbackQA");
            var collectionAlert = database.GetCollection<Alert_REC>("Alert");
            var collectionUser = database.GetCollection<Users_REC>("Users");
            
            var SensorData = database.GetCollection<Sensor_REC>("SensorDataDevice");

            try
            {
                poData.recordTimestamp = DateTime.Now;
                insertDoc.InsertOne(poData);
                foreach(QuestionsAnswerFeedback_REC qa in qaData)
                {
                    qa.parentID = poData._id.ToString();
                    insertDocQA.InsertOne(qa);
                }

                if (poData.thermalComfortSatisfactionID != null && poData.thermalComfortSatisfactionID.Value < 0)
                {
                    var builder = Builders<Sensor_REC>.Filter;
                    var filter = builder.Eq<string>("Node", "25001");
                    var resData = SensorData.Find(filter).ToList();
                    var temperature = resData.Where(x => x.Chanel == "ch7").SingleOrDefault().PresentValue;
                    var humidity = resData.Where(x => x.Chanel == "ch8").SingleOrDefault().PresentValue;
                    var co2 = resData.Where(x => x.Chanel == "ch6").SingleOrDefault().PresentValue;

                    var builder2 = Builders<Sensor_REC>.Filter;
                    var filter2 = builder2.Eq<string>("Node", "25003");
                    var resData2 = SensorData.Find(filter2).ToList();
                    var airVelocity = resData2.Where(x => x.Chanel == "ch6").SingleOrDefault().PresentValue;

                    Random random = new Random();

                    Alert_REC alertData = new Alert_REC();
                    alertData.recordTimestamp = DateTime.Now;
                    alertData.recordStatus = 0;
                    alertData.temperature = temperature;
                    alertData.humidity = humidity;
                    alertData.co2 = co2;
                    alertData.airVelocity = airVelocity;
                    alertData.skinTemperature = random.Next(29, 35).ToString();
                    alertData.heartRate = random.Next(60, 80).ToString();
                    alertData.uuid = poData.uuid;
                    alertData.thermalComfortSatisfaction = poData.thermalComfortSatisfaction;
                    alertData.thermalSensation = poData.thermalSensation;
                    alertData.location = poData.location;
                    collectionAlert.InsertOne(alertData);

                    FilterDefinitionBuilder<Users_REC> whereclause_builder = Builders<Users_REC>.Filter;
                    FilterDefinition<Users_REC> where_clause = whereclause_builder.Eq<string>("uuid", poData.uuid);
                    var update = Builders<Users_REC>.Update
                        .Set(p => p.recordStatus, 0);
                    collectionUser.UpdateOne(where_clause, update);

                    string msg = JsonConvert.SerializeObject(alertData).ToString();
                    GlobalFunction.MqttClient.Publish("mobileapp/feedback/alert/satisfaction", Encoding.UTF8.GetBytes(msg), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                }
                else
                {
                    FilterDefinitionBuilder<Users_REC> whereclause_builder = Builders<Users_REC>.Filter;
                    FilterDefinition<Users_REC> where_clause = whereclause_builder.Eq<string>("uuid", poData.uuid);
                    var update = Builders<Users_REC>.Update
                        .Set(p => p.recordStatus, 1);
                    collectionUser.UpdateOne(where_clause, update);
                }

                ResultData.Add("msg", "Hi, Thank you very much for feedback ");
                ResultData.Add("errorcode", "0");
                ResultData.Add("title", "submit Success");
            }
            catch (Exception e)
            {
                ResultData.Add("msg", e.Message);
                ResultData.Add("errorcode", "100");
                ResultData.Add("title", "submit failed");
            }

            return Json(ResultData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetOneFeedback(string _id)
        {
            Dictionary<string, string> ResultData = new Dictionary<string, string>();
            try
            {
                IMongoDatabase database = Constant.client.GetDatabase(Constant.dbName);
                var collect = database.GetCollection<FeedbackView_REC>("UserFeedback");
                var insertDoc = database.GetCollection<Feedback_REC>("UserFeedback");
                var qaDoc = database.GetCollection<QuestionsAnswerFeedback_REC>("UserFeedbackQA");

                var builder = Builders<FeedbackView_REC>.Filter;
                var filter = builder.Eq<ObjectId>("_id", ObjectId.Parse(_id));
                var res = collect.Find(filter).SingleOrDefault();

                if (res != null)
                {
                    string resString = JsonConvert.SerializeObject(res);
                    var resData = JsonConvert.DeserializeObject(resString);
                    JObject voFeedback = JObject.Parse(resData.ToString());

                    var builderQA = Builders<QuestionsAnswerFeedback_REC>.Filter;
                    var filterQA = builderQA.Eq<string>("parentID", _id);
                    var qa = qaDoc.Find(filterQA).ToList();

                    List<QuestionsAnswerFeedback_REC> voList = new List<QuestionsAnswerFeedback_REC>();
                    foreach(QuestionsAnswerFeedback_REC voQuestion in qa)
                    {
                        JToken value;
                        voFeedback.TryGetValue(voQuestion.question_id, out value);

                        Answer_REC c = voQuestion.answer.Find(w => w.answer == value.ToString());
                        if(c != null)
                        {
                            voQuestion.answer[0].color = c.color;
                            voQuestion.recordTimestamp = res.recordTimestamp;
                            voQuestion.answer[0].answer = value.ToString();
                            voList.Add(voQuestion);
                        }
                    }
                    
                    string strValue = Convert.ToString(JsonConvert.SerializeObject(voList));
                    ResultData.Add("data", strValue);
                    ResultData.Add("errorcode", "0");
                    ResultData.Add("title", "Success");
                }
                else
                {
                    ResultData.Add("msg", "empty data");
                    ResultData.Add("errorcode", "100");
                    ResultData.Add("title", "empty data");
                }
            }
            catch (Exception e)
            {
                ResultData.Add("msg", e.Message);
                ResultData.Add("errorcode", "100");
                ResultData.Add("title", "collected data failed");
            }
            return Json(ResultData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetManyFeedbackUser(Feedback_REC poData)
        {
            Dictionary<string, string> ResultData = new Dictionary<string, string>();
            try
            {
                IMongoDatabase database = Constant.client.GetDatabase(Constant.dbName);
                var collect = database.GetCollection<FeedbackView_REC>("UserFeedback");

                var builder = Builders<FeedbackView_REC>.Filter;
                var filter_mongo = new BsonDocument();
                Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                obj_filter.Add("uuid", poData.uuid);
                obj_filter.Add("recordTimestamp", new BsonDocument() {
                    { "$gte", DateTime.Parse(poData.recordTimestamp.ToString("yyyy-MM-dd 00:00:00")) },
                    { "$lte", DateTime.Parse(poData.recordTimestamp.ToString("yyyy-MM-dd 23:59:59")) }
                });
                filter_mongo.AddRange(obj_filter);
                var sort = new BsonDocument();
                var obj_sort = new Dictionary<string, object>();
                obj_sort.Add("recordTimestamp", -1);
                sort.AddRange(obj_sort);
                var res = collect.Find(filter_mongo).Sort(sort).ToList();

                if (res != null)
                {
                    string strValue = Convert.ToString(JsonConvert.SerializeObject(res));
                    ResultData.Add("data", strValue);
                    ResultData.Add("errorcode", "0");
                    ResultData.Add("title", "Success");
                }
                else
                {
                    ResultData.Add("msg", "empty data");
                    ResultData.Add("errorcode", "100");
                    ResultData.Add("title", "empty data");
                }
            }
            catch (Exception e)
            {
                ResultData.Add("msg", e.Message);
                ResultData.Add("errorcode", "100");
                ResultData.Add("title", "collected data failed");
            }
            return Json(ResultData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetQuestionAnswer()
        {
            Dictionary<string, string> ResultData = new Dictionary<string, string>();
            IMongoDatabase database = Constant.client.GetDatabase(Constant.dbName);
            var collect = database.GetCollection<QuestionsAnswer_REC>("QuestionsAnswer");

            var filter_mongo = new BsonDocument();
            Dictionary<string, object> obj_filter = new Dictionary<string, object>();
            obj_filter.Add("question_type", new BsonDocument() {
                    { "$eq", "MOBILE" },
                });
            filter_mongo.AddRange(obj_filter);
            List<QuestionsAnswer_REC> oTable = collect.Find(filter_mongo).ToList();

            ResultData.Add("msg", "");
            ResultData.Add("errorcode", "0");
            ResultData.Add("data", JsonConvert.SerializeObject(oTable).ToString());

            return Json(ResultData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetQuestionAnswerServer()
        {
            Dictionary<string, string> ResultData = new Dictionary<string, string>();
            IMongoDatabase database = Constant.client.GetDatabase(Constant.dbName);
            var collect = database.GetCollection<QuestionsAnswer_REC>("QuestionsAnswer");
            var collectTime = database.GetCollection<MobileNotification_REC>("MobileNotification");

            var filter_mongo_time = new BsonDocument();
            string time = DateTime.Now.ToString("HH:mm");
            Dictionary<string, object> obj_filter_time = new Dictionary<string, object>();
            obj_filter_time.Add("notifTime", new BsonDocument() {
                    { "$eq", time },
                });
            filter_mongo_time.AddRange(obj_filter_time);
            MobileNotification_REC oTableTime = collectTime.Find(filter_mongo_time).SingleOrDefault();
            if (oTableTime != null)
            {
                var filter_mongo = new BsonDocument();
                Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                obj_filter.Add("question_type", new BsonDocument() {
                    { "$eq", "SERVER" },
                });
                obj_filter.Add("question_time", new BsonDocument() {
                    { "$eq", oTableTime.notifTime },
                });
                filter_mongo.AddRange(obj_filter);
                List<QuestionsAnswer_REC> oTable = collect.Find(filter_mongo).ToList();
                if (oTable.Count > 0)
                {
                    ResultData.Add("msg", "");
                    ResultData.Add("errorcode", "0");
                    ResultData.Add("data", JsonConvert.SerializeObject(oTable).ToString());

                    string strValue = JsonConvert.SerializeObject(oTable).ToString();
                    GlobalFunction.MqttClient.Publish("mobileapp/feedback/data/" + time, Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                }
            }
            return Json(ResultData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult test(string topic, string message)
        {
            GlobalFunction.MqttClient.Publish(topic, Encoding.UTF8.GetBytes(message), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
            return Json(new { topic = topic, message = message }, JsonRequestBehavior.AllowGet);
        }
    }
}