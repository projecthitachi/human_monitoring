﻿using has.iot.Components;
using has.iot.Models;
using has.iot.Models.System;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using MongoDB.Bson;
using MongoDB.Driver;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace has.iot.Controllers
{
    public class PartialController : Controller
    {
        public ActionResult _MainMenu()
        {
            string RoleId = System.Web.HttpContext.Current.Session["RoleID"] as string;
            if (!string.IsNullOrEmpty(RoleId))
            {
                IMongoDatabase database = Constant.client.GetDatabase(Constant.dbName);
                IMongoCollection<Role> CollRole = database.GetCollection<Role>("Roles");
                IMongoCollection<Moduls> Collections = database.GetCollection<Moduls>("Moduls");

                var evntBld = Builders<Role>.Filter;
                var evntFlt = evntBld.Eq<string>("rlId", RoleId);
                var evntRes = CollRole.Find(evntFlt).SingleOrDefault();
                if (evntRes == null)
                {
                    throw new NullReferenceException("role not found");
                }

                var vResult = Collections.Find(f => f.active == true && f.type == "module").SortBy(s => s.order).ToList();
                List<featuresAccess> result = JsonConvert.DeserializeObject<List<featuresAccess>>(evntRes.acsMod);
                List<Moduls> navigation = new List<Moduls>();
                foreach (featuresAccess obj in result)
                {
                    if (obj.e)
                    {
                        Moduls vMenu = Collections.Find(f => f.active == true && f.alias == obj.modul).FirstOrDefault();
                        if (vMenu != null)
                        {
                            navigation.Add(vMenu);
                        }
                    }
                }

                ViewData["nav"] = navigation.ToList();
                //ViewData["navAll"] = navigation.OrderBy(s => s.order).ToList();
                return PartialView("Partials/_MainMenu");
            }
            else
            {
                List<Moduls> navigation = new List<Moduls>();
                ViewData["nav"] = navigation;
                //ViewData["navAll"] = navigation;
                return PartialView("Partials/_MainMenu");
            }
        }
        public ActionResult _MainMenuIotCategory()
        {
            Category oClass = new Category();
            var vResult = oClass.GetList().ToList();
            ViewData["Data"] = vResult;
            return PartialView("Partials/_MainMenuIotCategory");
        }
        public ActionResult _LocationPopup()
        {
            return PartialView("WindowsPopup/_Location");
        }
    }
}