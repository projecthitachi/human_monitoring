﻿using has.iot.Components;
using has.iot.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace has.iot.Controllers
{
    public class LocationController : Controller
    {
        // GET: Location
        public ActionResult Index()
        {
            //ViewData["activeMenu"] = "LocationMenu";
            return View();
        }

        public ActionResult Read([DataSourceRequest] DataSourceRequest poRequest)
        {
            IMongoDatabase database = Constant.client.GetDatabase(Constant.dbName);
            var collection = database.GetCollection<SmartWatchGatewayModelDatabase>("SmartwatchGateway");
            var filter = new BsonDocument() { };
            List<SmartWatchGatewayModelDatabase> res = collection.Find(filter).ToList();
            return Json(res.ToDataSourceResult(poRequest), JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create([DataSourceRequest] DataSourceRequest poRequest, SmartWatchGatewayView poRecord)
        {
            IMongoDatabase database = Constant.client.GetDatabase(Constant.dbName);
            var collectionPersonTag = database.GetCollection<SmartWatchGatewayView>("SmartwatchGateway");
            if ((poRecord != null) && (ModelState.IsValid))
            {
                try
                {
                    var builderPersonTag = Builders<SmartWatchGatewayView>.Filter;
                    var flt = builderPersonTag.Eq<string>("gatewayID", poRecord.gatewayID);
                    SmartWatchGatewayView dataTags = collectionPersonTag.Find(flt).FirstOrDefault();
                    if (dataTags != null)
                    {
                        ModelState.AddModelError("Error", "Gateway ID already registered");
                    }
                    else
                    {
                        if (poRecord.gatewayID != null)
                        {
                            SmartWatchGatewayView oData = new SmartWatchGatewayView();
                            oData.RecordTimestamp = DateTime.Now;
                            oData.gatewayID = poRecord.gatewayID;
                            oData.gatewayName = poRecord.gatewayName;
                            oData.ipAddress = poRecord.ipAddress;
                            oData.location = poRecord.location;
                            oData.status = poRecord.status;
                            collectionPersonTag.InsertOne(oData);
                        }
                        else
                        {
                            ModelState.AddModelError("Error", "Gateway ID is empty");
                        }
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("Error", ex.Message);
                }

            }
            return Json(new[] { poRecord }.ToDataSourceResult(poRequest, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update([DataSourceRequest] DataSourceRequest poRequest, SmartWatchGatewayView poRecord)
        {
            if ((poRecord != null) && (ModelState.IsValid))
            {
                IMongoDatabase database = Constant.client.GetDatabase(Constant.dbName);
                var collection = database.GetCollection<SmartWatchGatewayView>("SmartwatchGateway");
                try
                {
                    var builderPersonTag = Builders<SmartWatchGatewayView>.Filter;
                    var fltCheck = builderPersonTag.Eq<string>("gatewayID", poRecord.gatewayID);
                    SmartWatchGatewayView dataLocation = collection.Find(fltCheck).FirstOrDefault();
                    if (dataLocation == null)
                    {
                        ModelState.AddModelError("Error", "Gateway ID not registered");
                    }
                    else
                    {
                        var update = Builders<SmartWatchGatewayView>.Update.Set("RecordTimestamp", DateTime.Now)
                            .Set("gatewayName", poRecord.gatewayName)
                            .Set("ipAddress", poRecord.ipAddress)
                            .Set("location", poRecord.location)
                            .Set("status", poRecord.status);
                        collection.UpdateOne(fltCheck, update);
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("Error", ex.Message);
                }
            }

            return Json(new[] { poRecord }.ToDataSourceResult(poRequest, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Destroy([DataSourceRequest] DataSourceRequest poRequest, SmartWatchGatewayView poRecord)
        {
            if (poRecord != null)
            {
                IMongoDatabase database = Constant.client.GetDatabase(Constant.dbName);
                var collection = database.GetCollection<SmartWatchGatewayView>("SmartwatchGateway");
                try
                {
                    var builder = Builders<SmartWatchGatewayView>.Filter;
                    var flt = builder.Eq<string>("gatewayID", poRecord.gatewayID);
                    collection.DeleteOne(flt);
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("Error", ex.Message);
                }
            }

            return Json(new[] { poRecord }.ToDataSourceResult(poRequest, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult getLocation()
        {
            IMongoDatabase database = Constant.client.GetDatabase(Constant.dbName);
            var collection = database.GetCollection<SmartWatchGatewayView>("SmartwatchGateway");
            var filter = new BsonDocument() { };
            List<SmartWatchGatewayView> res = collection.Find(filter).SortByDescending(val => val.location).ToList();
            List<SmartWatchGatewayView> listData = new List<SmartWatchGatewayView>();
            if (res != null)
            {
                foreach (SmartWatchGatewayView data in res)
                {
                    SmartWatchGatewayView dataLocation = new SmartWatchGatewayView();
                    dataLocation.gatewayID = data.location;
                    dataLocation.location = data.location;
                    listData.Add(dataLocation);

                }
            }

            return Json(listData, JsonRequestBehavior.AllowGet);
        }
    }
}