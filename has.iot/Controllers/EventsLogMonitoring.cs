﻿using has.iot.Components;
using has.iot.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using MongoDB.Bson;
using MongoDB.Driver;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace has.iot.Controllers
{
    public class EventsLogMonitoringController : Controller
    {
        // GET: PersonRealtime
        public ActionResult Index()
        {
            ViewData["activeMenu"] = "EventsLogMonitoring";
            return View();
        }

        public ActionResult Read([DataSourceRequest] DataSourceRequest poRequest)
        {
            IMongoDatabase database = Constant.client.GetDatabase(Constant.dbName);
            var collection = database.GetCollection<EventsLogMonitoring>("EventsLog");
            var filter = new BsonDocument() { };
            List<EventsLogMonitoring> res = collection.Find(filter).SortByDescending(val => val.RecordTimestamp).ToList();

            List<EventsLogMonitoringView> AllRow = new List<EventsLogMonitoringView>();
            foreach (EventsLogMonitoring data in res)
            {
                EventsLogMonitoringView dataRow = new EventsLogMonitoringView();
                dataRow._id = data._id;
                dataRow.RecordTimestamp = data.RecordTimestamp;
                dataRow.EventType = data.EventType;
                dataRow.TagID = data.TagID;
                dataRow.personName = data.personName;
                dataRow.NRIC = data.NRIC;
                dataRow.Temp = data.Temp;
                dataRow.HR = data.HR;
                dataRow.Location = data.Location;
                dataRow.Tampered = data.Tampered;
                dataRow.Acknowledge = data.Acknowledge;
                dataRow.ActionRemarks = data.ActionRemarks;
                dataRow.Status = data.Status;
                dataRow.SOS = data.SOS;
                dataRow.Battery = data.Battery;
                dataRow.Geofencing = data.Geofencing;
                dataRow.dataID = data._id.ToString();
                dataRow.stationNo = data.stationNo;
                dataRow.Blood = data.Blood;
                dataRow.FallDown = data.FallDown;
                AllRow.Add(dataRow);
            }

            return Json(AllRow.ToDataSourceResult(poRequest), JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult updateAcknowledge([DataSourceRequest] DataSourceRequest poRequest, updateAcknowledge poRecord)
        {
            ApiResponses ResultData = new ApiResponses();
            try
            {

                if (poRecord.id == null || poRecord.id == "")
                {
                    throw new NullReferenceException("Event not found!");
                }

                if (poRecord.remarks == null || poRecord.remarks == "")
                {
                    throw new NullReferenceException("Remarks required!");
                }

                if (poRecord != null)
                {
                IMongoDatabase database = Constant.client.GetDatabase(Constant.dbName);
                var collection = database.GetCollection<EventsLogMonitoring>("EventsLog");
                
                    var objectId = new ObjectId(poRecord.id);
                    var builder = Builders<EventsLogMonitoring>.Filter;
                    var flt = builder.Eq<ObjectId>("_id", objectId);
                    EventsLogMonitoring dataTags = collection.Find(flt).FirstOrDefault();
                    if (dataTags != null)
                    {
                        var update = Builders<EventsLogMonitoring>.Update
                            .Set("RecordTimestamp", DateTime.Now)
                            .Set("Acknowledge", "Yes")
                            .Set("Status", "Resolved")
                            .Set("ActionRemarks", poRecord.remarks);
                        collection.UpdateOne(flt, update);


                        ResultData.errorcode = 0;
                        ResultData.title = "Success";
                        ResultData.message = "Update Event Remarks success";
                    }
                }
            }
            catch (Exception ex)
            {
                ResultData.errorcode = 100;
                ResultData.title = "failed";
                ResultData.message = ex.Message;
            }
            return Json(ResultData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ClearDataEvent()
        {
            ApiResponses ResultData = new ApiResponses();
            try
            {
                var database = Constant.client.GetDatabase(Constant.dbName);
                var collection = database.GetCollection<EventsLogMonitoring>("EventsLog");
                var collectionTemp = database.GetCollection<EventsLogMonitoringTemp>("EventsLogTemp");
                var collectionAlertTotal = database.GetCollection<countRealtimeAlert>("EventsLogAlert");
                var collectionRealtimeDashboard = database.GetCollection<RealtimeDashboard>("RealtimeDashboard");
                var filter = new BsonDocument() { };
                List<EventsLogMonitoring> res = collection.Find(filter).SortByDescending(val => val.RecordTimestamp).ToList();
                List<EventsLogMonitoringTemp> resTemp = collectionTemp.Find(filter).SortByDescending(val => val.RecordTimestamp).ToList();
                List<RealtimeDashboard> resRealtimeDashboard = collectionRealtimeDashboard.Find(filter).SortByDescending(val => val.RecordTimestamp).ToList();

                foreach (EventsLogMonitoring data in res)
                {
                    var evntBld = Builders<EventsLogMonitoring>.Filter;
                    var evntFlt = evntBld.Eq<ObjectId>("_id", data._id);
                    collection.DeleteOne(evntFlt);
                }

                foreach (EventsLogMonitoringTemp dataTemp in resTemp)
                {
                    var evntBld2 = Builders<EventsLogMonitoringTemp>.Filter;
                    var evntFlt2 = evntBld2.Eq<ObjectId>("_id", dataTemp._id);
                    collectionTemp.DeleteOne(evntFlt2);
                }

                foreach (RealtimeDashboard dataDashboard in resRealtimeDashboard)
                {
                    var evntBld3 = Builders<RealtimeDashboard>.Filter;
                    var evntFlt3 = evntBld3.Eq<ObjectId>("_id", dataDashboard._id);
                    collectionRealtimeDashboard.DeleteOne(evntFlt3);
                }

                countRealtimeAlert dataTags = collectionAlertTotal.Find(filter).FirstOrDefault();
                var update = Builders<countRealtimeAlert>.Update
                            .Set("RecordTimestamp", DateTime.Now)
                            .Set("totalAlert", 0)
                            .Set("statusAlert", 0);
                collectionAlertTotal.UpdateOne(filter, update);

                ResultData.errorcode = 0;
                ResultData.title = "Success";
                ResultData.message = "Delete Data success";
            }
            catch (Exception ex)
            {
                ResultData.errorcode = 100;
                ResultData.title = "failed";
                ResultData.message = ex.Message;
            }

            return Json(ResultData, JsonRequestBehavior.DenyGet);
        }

        public ActionResult totalEventLog()
        {
            var database = Constant.client.GetDatabase(Constant.dbName);
            var EventsLogAlert = database.GetCollection<countRealtimeAlert>("EventsLogAlert");
            var builderAlert = Builders<countRealtimeAlert>.Filter;
            var fltAlert = builderAlert.Eq<string>("alertName", "Event");
            countRealtimeAlert resAlert = EventsLogAlert.Find(fltAlert).SortByDescending(val => val.RecordTimestamp).FirstOrDefault();

            return Json(resAlert, JsonRequestBehavior.AllowGet);
        }
    }
}