﻿using has.iot.Components;
using has.iot.Models;
using has.stayathome.monitoring.Components;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace has.iot.Controllers
{
    public class ProfileMasterController : Controller
    {
        // GET: PersonRealtime
        public ActionResult Index()
        {
            ViewData["activeMenu"] = "ProfileMaster";
            return View();
        }

        public ActionResult Edit(string id)
        {
            ViewData["activeMenu"] = "ProfileMaster";
            //var _id = new ObjectId(id);
            IMongoDatabase database = Constant.client.GetDatabase(Constant.dbName);
            var collection = database.GetCollection<ProfileMaster>("Person");
            var filter = new BsonDocument() { };
            ProfileMaster res = collection.Find(f => f.stationNo == id).FirstOrDefault();
            if (res == null)
            {
                return RedirectToAction("Index", "ProfileMaster");
            }
            ViewData["ProfileDetail"] = res;
            return View();
        }

        public ActionResult Detail(string id)
        {
            ViewData["activeMenu"] = "ProfileMaster";
            //var _id = new ObjectId(id);
            IMongoDatabase database = Constant.client.GetDatabase(Constant.dbName);
            var collection = database.GetCollection<ProfileMaster>("Person");
            var collectionSWLog = database.GetCollection<BLESmartWatchData>("SmartwatchDataLogs");

            var filter = new BsonDocument() { };
            ProfileMaster res = collection.Find(f => f.stationNo == id).FirstOrDefault();

            BLESmartWatchData wearableLog = collectionSWLog.Find(f => f.person == id).Limit(1).SortByDescending(x => x.RecordTimestamp).FirstOrDefault();
            
            ViewData["ProfileDetail"] = res;
            ViewData["WearableLog"] = wearableLog;
            
            return View();
        }

        public ActionResult Read([DataSourceRequest] DataSourceRequest poRequest)
        {
            IMongoDatabase database = Constant.client.GetDatabase(Constant.dbName);
            var collection = database.GetCollection<ProfileMaster>("Person");
            var filter = new BsonDocument() { };
            List<ProfileMaster> res = collection.Find(filter).ToList();
            return Json(res.ToDataSourceResult(poRequest), JsonRequestBehavior.AllowGet);
        }

        // Update 
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update([DataSourceRequest] DataSourceRequest poRequest, ProfileMaster_REC poRecord)
        {
            ApiResponses ResultData = new ApiResponses();
            try
            {

                if (poRecord.DefaultWearable == null || poRecord.DefaultWearable == "")
                {
                    throw new NullReferenceException("Wearable Tags required!");
                }

                if (poRecord.defaultZone == null || poRecord.defaultZone == "")
                {
                    throw new NullReferenceException("Location required!");
                }

                IMongoDatabase database = Constant.client.GetDatabase(Constant.dbName);
                var collection = database.GetCollection<ProfileMaster>("Person");
                var collectionTags = database.GetCollection<WearableTagModel_REC>("PersonTags");
                var object_id = new ObjectId(poRecord.id);
                var builder = Builders<ProfileMaster>.Filter;
                var flt = builder.Eq<ObjectId>("_id", object_id);
                ProfileMaster personData = collection.Find(flt).FirstOrDefault();


                if (poRecord.status == "Inactive")
                {
                    var builderTags = Builders<WearableTagModel_REC>.Filter;
                    var fltTags = builderTags.Eq<string>("stationNo", personData.stationNo);
                    var updateTags = Builders<WearableTagModel_REC>.Update.Set("stationNo", "");
                    collectionTags.UpdateOne(fltTags, updateTags);
                }
                else
                {
                    var builderCheck = Builders<ProfileMaster>.Filter;
                    var fltCheck = builderCheck.Eq<string>("status", "Active");
                    fltCheck &= builderCheck.Eq<string>("DefaultWearable", poRecord.DefaultWearable);
                    fltCheck &= builder.Ne<ObjectId>("_id", object_id);
                    ProfileMaster checkPerson = collection.Find(fltCheck).FirstOrDefault();
                    if (checkPerson != null)
                    {
                        throw new NullReferenceException("Wearable Register Active");
                    }
                    else
                    {
                        var builderTags = Builders<WearableTagModel_REC>.Filter;
                        var fltTags2 = builderTags.Eq<string>("tagID", poRecord.DefaultWearable);
                        var updateTags2 = Builders<WearableTagModel_REC>.Update.Set("RecordTimestamp", DateTime.Now)
                            .Set("stationNo", personData.stationNo);
                        collectionTags.UpdateOne(fltTags2, updateTags2);
                    }
                }

                if (personData != null)
                {

                    var update = Builders<ProfileMaster>.Update.Set("RecordTimestamp", DateTime.Now)
                    .Set("HealthRemarks", poRecord.HealthRemarks)
                    .Set("CaseRemarks", poRecord.CaseRemarks)
                    .Set("DefaultWearable", poRecord.DefaultWearable)
                    .Set("defaultZone", poRecord.defaultZone)
                    .Set("WearableActivated", poRecord.WearableActivated)
                    .Set("SosButton", poRecord.SosButton)
                    .Set("status", poRecord.status);
                    collection.UpdateOne(flt, update);

                    if (personData.DefaultWearable != poRecord.DefaultWearable)
                    {
                        var builderTags = Builders<WearableTagModel_REC>.Filter;
                        var fltTags = builderTags.Eq<string>("stationNo", personData.stationNo);
                        var updateTags = Builders<WearableTagModel_REC>.Update.Set("stationNo", "");
                        collectionTags.UpdateOne(fltTags, updateTags);

                        var fltTags2 = builderTags.Eq<string>("tagID", poRecord.DefaultWearable);
                        var updateTags2 = Builders<WearableTagModel_REC>.Update.Set("RecordTimestamp", DateTime.Now)
                            .Set("stationNo", personData.stationNo);
                        collectionTags.UpdateOne(fltTags2, updateTags2);
                    }

                    ResultData.errorcode = 0;
                    ResultData.title = "Success";
                    ResultData.message = "Update Data success";
                    ResultData.data = poRecord.stationNo;
                }
                else
                {
                    throw new NullReferenceException("Station No not found!");
                }
            }catch (Exception ex)
            {
                ResultData.errorcode = 100;
                ResultData.title = "failed";
                ResultData.message = ex.Message;
            }
            return Json(ResultData, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Destroy([DataSourceRequest] DataSourceRequest poRequest, ProfileMaster_REC poRecord)
        {
            ApiResponses ResultData = new ApiResponses();
            try
            {
                if (poRecord.stationNo == null || poRecord.stationNo == "")
                {
                    throw new NullReferenceException("Station No not found!");
                }

                if (poRecord != null)
                {
                    IMongoDatabase database = Constant.client.GetDatabase(Constant.dbName);
                    var collection = database.GetCollection<ProfileMaster>("Person");
                    var collectionTags = database.GetCollection<WearableTagModel_REC>("PersonTags");
                    var builder = Builders<ProfileMaster>.Filter;
                    var flt = builder.Eq<string>("stationNo", poRecord.stationNo);
                    collection.DeleteOne(flt);

                    var builderTags = Builders<WearableTagModel_REC>.Filter;
                    var fltTags = builderTags.Eq<string>("stationNo", poRecord.stationNo);
                    var updateTags = Builders<WearableTagModel_REC>.Update.Set("stationNo", "");
                    collectionTags.UpdateOne(fltTags, updateTags);

                    ResultData.errorcode = 0;
                    ResultData.title = "Success";
                    ResultData.message = "Delete Data Station No :" + poRecord.stationNo + " success";
                }
                else
                {
                    ResultData.errorcode = 100;
                    ResultData.title = "failed";
                    ResultData.message = "Delete Data Station No :" + poRecord.stationNo + " failed, Station No NotFound";
                }
            }
            catch (Exception ex)
            {
                ResultData.errorcode = 100;
                ResultData.title = "failed";
                ResultData.message = ex.Message;
            }

            return Json(ResultData, JsonRequestBehavior.DenyGet);
        }

        public ActionResult RealtimeChart(string personID)
        {
            IMongoDatabase database = Constant.client.GetDatabase(Constant.dbName);
            var collection = database.GetCollection<BLESmartWatchData>("SmartwatchDataLogs");

            BLESmartWatchData ResultData = collection.Find(f => f.person == personID)
                .SortByDescending(x => x.RecordTimestamp).Limit(1)
                .FirstOrDefault();
            realtimeChart dataCart = new realtimeChart();
            try
            {
                dataCart.temperature = ResultData.temperature;
                dataCart.heartrate = ResultData.heartrate;
                
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Error", ex.Message);
            }
            return Json(dataCart, JsonRequestBehavior.AllowGet);
        }

        public ActionResult HeartRateChart(string personID)
        {
            IMongoDatabase database = Constant.client.GetDatabase(Constant.dbName);
            var collection = database.GetCollection<BLESmartWatchData>("SmartwatchDataLogs");

            List<heartRateChart> ResultChart = new List<heartRateChart>();

            List<BLESmartWatchData> ResultData = collection.Find(f => f.person == personID && f.RecordTimestamp >= DateTime.Parse(DateTime.Now.AddMinutes(-10).ToString("yyyy-MM-dd hh:mm:ss")))
                .SortBy(x => x.RecordTimestamp).Limit(20)
                .ToList();
            try
            {
                foreach (BLESmartWatchData voTemp in ResultData)
                {
                    heartRateChart dataCart = new heartRateChart();
                    dataCart.EventDate = voTemp.RecordTimestamp.ToString("mm:ss");
                    dataCart.Baseline = voTemp.heartrate + 10;
                    dataCart.Current = voTemp.heartrate;
                    ResultChart.Add(dataCart);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Error", ex.Message);
            }
            return Json(ResultChart, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RealtimeData(string personID)
        {
            IMongoDatabase database = Constant.client.GetDatabase(Constant.dbName);
            var collectionPerson = database.GetCollection<ProfileMaster_REC>("Person");
            var collection = database.GetCollection<BLESmartWatchData>("SmartwatchDataLogs");
            var AvatarLocation = database.GetCollection<AvatarLocation>("AvatarLocation");
            Location result = new Location();

            ProfileMaster_REC res = collectionPerson.Find(f => f.personID == personID).FirstOrDefault();
            result.personID = res.personID;
            if (res != null)
            {
                BLESmartWatchData resLog = collection.Find(f => f.person == personID).Limit(1).SortByDescending(x => x.RecordTimestamp).FirstOrDefault(); ;
                result.battery = resLog.battery;
                result.heartrate = resLog.heartrate;
                result.temperature = resLog.temperature;
                result.sos = resLog.sos;
                AvatarLocation resLocation = AvatarLocation.Find(f => f.location == resLog.location).FirstOrDefault();
                result.location = resLocation.location;
                result.left = resLocation.left;
                result.top = resLocation.top;
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult getWearableTagsEdit(string stationNo)
        {
            var database = Constant.client.GetDatabase(Constant.dbName);
            var collection = database.GetCollection<WearableTagModel_REC>("PersonTags");
            List<WearableTagModel_REC> res = collection.Find(f => f.stationNo == null || f.stationNo == stationNo || f.stationNo == "").ToList();
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public ActionResult checkAdministratorPassword(string username, string password, string stationNo)
        {
            ApiResponses ResultData = new ApiResponses();
            var session = Constant.client.StartSession();
            try
            {

                if (username == null || username == "")
                {
                    throw new NullReferenceException("Username required!");
                }

                if (password == null || password == "")
                {
                    throw new NullReferenceException("Password required!");
                }

                IMongoDatabase database = session.Client.GetDatabase(Constant.dbName);
                var CollectDevice = database.GetCollection<Users>("Users");
                var collectionTags = database.GetCollection<WearableTagModel_REC>("PersonTags");
                password = Constant.MD5Hash(password);
                var res = CollectDevice.Find(x => x.usrName == username && x.usrPass == password).FirstOrDefault();

                if (res != null)
                {
                    var collection = database.GetCollection<ProfileMaster>("Person");
                    try
                    {
                        var builder = Builders<ProfileMaster>.Filter;
                        var flt = builder.Eq<string>("stationNo", stationNo);
                        collection.DeleteOne(flt);

                        var builderTags = Builders<WearableTagModel_REC>.Filter;
                        var fltTags = builderTags.Eq<string>("stationNo", stationNo);
                        var updateTags = Builders<WearableTagModel_REC>.Update.Set("stationNo", "");
                        collectionTags.UpdateOne(fltTags, updateTags);

                        ResultData.errorcode = 0;
                        ResultData.title = "Success";
                        ResultData.message = "Delete Data Station No :"+ stationNo + " success";
                    }
                    catch (Exception ex)
                    {
                        ResultData.errorcode = 100;
                        ResultData.title = "failed";
                        ResultData.message = ex.Message;
                    }
                }
                else
                {
                    ResultData.errorcode = 110;
                    ResultData.title = "failed";
                    ResultData.message = "Wrong Username Or Password!";
                }
            }catch (Exception ex){
                ResultData.errorcode = 100;
                ResultData.title = "failed";
                ResultData.message = ex.Message;
            }
            return Json(ResultData, JsonRequestBehavior.DenyGet);
        }
    }
}