﻿using has.iot.Components;
using has.iot.Models;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using uPLibrary.Networking.M2Mqtt.Messages;
using System.Web.Configuration;

namespace has.iot.Controllers
{
    [IsLoggedIn]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            DeviceModel oClass = new DeviceModel();
            var vResult = oClass.GetList().ToList();

            ViewData["deviceData"] = vResult;
            return View();
        }
        public ActionResult Overview()
        {
            ViewData["activeMenu"] = "Overview";
            return View();
        }
        public static void Receive()
        {
            var factory = new ConnectionFactory();
            factory.HostName = "10.1.1.127";
            factory.UserName = "admin";
            factory.Password = "admin";
            string message = "";
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                //    channel.ExchangeDeclare(exchange: "espurna-exchange", type: "topic", durable: true);
                //    /*
                //     * +		$exception	{"The AMQP operation was interrupted: AMQP close-reason, initiated by Peer, code=406, text=\"PRECONDITION_FAILED - inequivalent arg 'durable' for exchange 'espurna-exchange' in vhost '/': received 'false' but current is 'true'\", classId=40, methodId=10, cause="}	RabbitMQ.Client.Exceptions.OperationInterruptedException

                //     */
                //    //message = GetMessage(args);
                //    var body = Encoding.UTF8.GetBytes(message);
                //    channel.BasicPublish(exchange: "espurna-exchange",
                //                         routingKey: "",
                //                         basicProperties: null,
                //                         body: body);


                channel.ExchangeDeclare(exchange: "espurna-exchange/relay/0/set", type: "topic", durable: true);

                var queueName = channel.QueueDeclare().QueueName;
                channel.QueueBind(queue: queueName,
                                  exchange: "espurna-exchange/relay/0/set",
                                  routingKey: "");


                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += (model, ea) =>
                {
                    var body = ea.Body;
                    message = Encoding.UTF8.GetString(body);
                };
                channel.BasicConsume(queue: queueName,
                                     autoAck: true,
                                     consumer: consumer);
            }
        }

        public ActionResult BuildingFirst()
        {
            DeviceModel oClass = new DeviceModel();
            var vResult = oClass.GetList().ToList();

            ViewData["deviceData"] = vResult;
            return PartialView("BuildingFirst");
        }

        [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
        public ActionResult Modeling()
        {
            DeviceModel oClass = new DeviceModel();
            var vResult = oClass.GetList().ToList();

            ViewData["deviceData"] = vResult;
            return PartialView("BuildingDashboard");
        }

        public ActionResult checkRelay(string PluginID, string DeviceID)
        {
            Dictionary<string, string> ResultData = new Dictionary<string, string>();
            if (PluginID == "SmartSwitch")
            {
                string strValue = Convert.ToString("query");
                GlobalFunction.MqttClient.Publish(DeviceID + "/relay/0/set", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
            }
            else
            {
                string strValue = Convert.ToString("");
                //GlobalFunction.MqttClient.Publish(DeviceID + "/cmnd/POWER", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
            }
            ResultData.Add("msg", "Success");
            ResultData.Add("errorcode", "0");
            ResultData.Add("title", "Success");
            return Json(ResultData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SetSwitch(string PluginID, string DeviceID, string id)
        {
            Dictionary<string, string> ResultData = new Dictionary<string, string>();
            if (PluginID == "SmartSwitch")
            {
                string strValue = Convert.ToString(id);
                //GlobalFunction.MqttClient.Publish(DeviceID + "/relay/0/set", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
            }
            else
            {
                string strValue = Convert.ToString(id);
                if (strValue == "1") { strValue = "ON"; } else { strValue = "OFF"; }
                //GlobalFunction.MqttClient.Publish(DeviceID + "/cmnd/POWER", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
            }
            ResultData.Add("msg", "Success");
            ResultData.Add("errorcode", "0");
            ResultData.Add("title", "Success");
            return Json(ResultData, JsonRequestBehavior.AllowGet);

        }

        public ActionResult About()
        {
            ViewData["activeMenu"] = "About";
            return View();
        }

    }
}