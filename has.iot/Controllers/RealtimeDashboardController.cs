﻿using has.iot.Components;
using has.iot.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace has.iot.Controllers
{
    public class RealtimeDashboardController : Controller
    {
        // GET: PersonRealtime
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Read([DataSourceRequest] DataSourceRequest poRequest)
        {
            IMongoDatabase database = Constant.client.GetDatabase(Constant.dbName);
            var collection = database.GetCollection<EventsLogMonitoring>("RealtimeDashboard");
            var filter = new BsonDocument() { };
            List<EventsLogMonitoring> result = new List<EventsLogMonitoring>();
            List<EventsLogMonitoring> res = collection.Find(filter).SortBy(f => f.stationNo).ToList();

            if (res != null)
            {
                foreach (EventsLogMonitoring data in res)
                {
                    EventsLogMonitoring val = new EventsLogMonitoring();
                    val.EventType = data.EventType;
                    val.TagID = data.TagID;
                    val.personName = data.personName;
                    val.NRIC = data.NRIC;
                    val.Temp = data.Temp;
                    val.HR = data.HR;
                    val.Location = data.Location;
                    val.Tampered = data.Tampered;
                    val.Acknowledge = data.Acknowledge;
                    val.ActionRemarks = data.ActionRemarks;
                    val.Status = data.Status;
                    val.SOS = data.SOS;
                    val.Battery = data.Battery;
                    val.Geofencing = data.Geofencing;
                    val.stationNo = data.stationNo;
                    val.Blood = data.Blood;
                    val.FallDown = data.FallDown;
                    result.Add(val);
                }
            }

            return Json(result.ToDataSourceResult(poRequest), JsonRequestBehavior.AllowGet);
        }

        /*public ActionResult Read([DataSourceRequest] DataSourceRequest poRequest)
        {
            IMongoDatabase database = Constant.client.GetDatabase(Constant.dbName);
            var collectionPerson = database.GetCollection<ProfileMaster_REC>("Person");
            var collection = database.GetCollection<EventsLogMonitoring>("EventsLog");
            var filter = new BsonDocument() { };
            List<ProfileMaster_REC> personData = collectionPerson.Find(filter).ToList();

            List<EventsLogMonitoring> dataAlert = new List<EventsLogMonitoring>();

            foreach (ProfileMaster_REC voData in personData)
            {
                EventsLogMonitoring checkData = collection.Find(val => val.Acknowledge != "Yes" && val.stationNo == voData.stationNo).FirstOrDefault();
                if (checkData != null)
                {
                    EventsLogMonitoring alert = new EventsLogMonitoring();
                    alert.stationNo = checkData.stationNo;
                    alert.Location = checkData.Location;
                    alert.personName = checkData.personName;
                    EventsLogMonitoring resHR = collection.Find(val => val.Acknowledge != "Yes"
                    && val.stationNo == voData.stationNo
                    && val.EventType == "High Heart Rate").SortByDescending(val => val.RecordTimestamp).FirstOrDefault();

                    EventsLogMonitoring resHRLow = collection.Find(val => val.Acknowledge != "Yes"
                    && val.stationNo == voData.stationNo
                    && val.EventType == "Low Heart Rate").SortByDescending(val => val.RecordTimestamp).FirstOrDefault();
                    if (resHR != null && resHRLow != null)
                    {
                        if (resHR.RecordTimestamp > resHRLow.RecordTimestamp)
                        {
                            alert.HR = resHR.HR;
                        }
                        else
                        {
                            alert.HR = resHRLow.HR;
                        }
                    }
                    else if (resHR != null && resHRLow == null)
                    {
                        alert.HR = resHR.HR;
                    }
                    else if (resHR == null && resHRLow != null)
                    {
                        alert.HR = resHRLow.HR;
                    }
                    else
                    {
                        alert.HR = 1;
                    }

                    EventsLogMonitoring resTemp = collection.Find(val => val.Acknowledge != "Yes"
                    && val.stationNo == voData.stationNo
                    && val.EventType == "High Temperature").SortByDescending(val => val.RecordTimestamp).FirstOrDefault();

                    EventsLogMonitoring resTempLow = collection.Find(val => val.Acknowledge != "Yes"
                    && val.stationNo == voData.stationNo
                    && val.EventType == "Low Temperature").SortByDescending(val => val.RecordTimestamp).FirstOrDefault();
                    if (resTemp != null && resTempLow != null)
                    {
                        if (resTemp.RecordTimestamp > resTempLow.RecordTimestamp)
                        {
                            alert.Temp = resTemp.Temp;
                        }
                        else
                        {
                            alert.Temp = resTempLow.Temp;
                        }
                    }
                    else if (resTemp != null && resTempLow == null)
                    {
                        alert.Temp = resTemp.Temp;
                    }
                    else if (resTemp == null && resTempLow != null)
                    {
                        alert.Temp = resTempLow.Temp;
                    }
                    else
                    {
                        alert.Temp = 1;
                    }

                    EventsLogMonitoring resTampered = collection.Find(val => val.Acknowledge != "Yes"
                    && val.stationNo == voData.stationNo
                    && val.EventType == "Tampered alert").SortByDescending(val => val.RecordTimestamp).FirstOrDefault();
                    if (resTampered != null)
                    {
                        alert.Tampered = resTampered.Tampered;
                    }
                    else
                    {
                        alert.Tampered = "";
                    }



                    EventsLogMonitoring resBattery = collection.Find(val => val.Acknowledge != "Yes"
                    && val.stationNo == voData.stationNo
                    && val.EventType == "Low Battery").SortByDescending(val => val.RecordTimestamp).FirstOrDefault();
                    if (resBattery != null)
                    {
                        alert.Battery = resBattery.Battery;
                    }
                    else
                    {
                        alert.Battery = 1;
                    }

                    EventsLogMonitoring resFallDown = collection.Find(val => val.Acknowledge != "Yes"
                    && val.stationNo == voData.stationNo
                    && val.EventType == "Fall Down").SortByDescending(val => val.RecordTimestamp).FirstOrDefault();
                    if (resFallDown != null)
                    {
                        alert.FallDown = resFallDown.FallDown;
                    }
                    else
                    {
                        alert.FallDown = "";
                    }


                    EventsLogMonitoring resBloodHigh = collection.Find(val => val.Acknowledge != "Yes"
                    && val.stationNo == voData.stationNo
                    && val.EventType == "High Blood Pressure").SortByDescending(val => val.RecordTimestamp).FirstOrDefault();

                    EventsLogMonitoring resBloodLow = collection.Find(val => val.Acknowledge != "Yes"
                    && val.stationNo == voData.stationNo
                    && val.EventType == "Low Blood Pressure").SortByDescending(val => val.RecordTimestamp).FirstOrDefault();
                    if (resBloodHigh != null && resBloodLow != null)
                    {
                        if (resBloodHigh.RecordTimestamp > resBloodLow.RecordTimestamp)
                        {
                            alert.Blood = resBloodHigh.Blood;
                        }
                        else
                        {
                            alert.Blood = resBloodLow.Blood;
                        }
                    }
                    else if (resBloodHigh != null && resBloodLow == null)
                    {
                        alert.Blood = resBloodHigh.Blood;
                    }
                    else if (resBloodHigh == null && resBloodLow != null)
                    {
                        alert.Blood = resBloodLow.Blood;
                    }
                    else
                    {
                        alert.Blood = 1;
                    }


                    EventsLogMonitoring resGeofencing = collection.Find(val => val.Acknowledge != "Yes"
                    && val.stationNo == voData.stationNo
                    && val.EventType == "Geofencing alert zone" || val.EventType == "Geofencing alert not detected").SortByDescending(val => val.RecordTimestamp).FirstOrDefault();
                    if (resGeofencing != null)
                    {
                        alert.Geofencing = resGeofencing.Geofencing;
                    }
                    else
                    {
                        alert.Geofencing = "";
                    }

                    alert.SOS = "";
                    if (alert.HR != 1 || alert.Temp != 1 || alert.Tampered != "" || alert.Geofencing != "" || alert.Battery != 1 || alert.FallDown != "" || alert.Blood != 1)
                    {
                        dataAlert.Add(alert);
                    }
                }
            }
            return Json(dataAlert.ToDataSourceResult(poRequest), JsonRequestBehavior.AllowGet);
        }*/
    }
}