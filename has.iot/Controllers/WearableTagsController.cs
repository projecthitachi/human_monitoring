﻿using has.iot.Components;
using has.iot.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace has.iot.Controllers
{
    public class WearableTagsController : Controller
    {
        // GET: WearableTags
        public ActionResult Index()
        {
            ViewData["activeMenu"] = "WearableTags";
            return View();
        }

        public ActionResult Read([DataSourceRequest] DataSourceRequest poRequest)
        {
            IMongoDatabase database = Constant.client.GetDatabase(Constant.dbName);
            var collection = database.GetCollection<WearableTagModel_REC>("PersonTags");
            var filter = new BsonDocument() { };
            List<WearableTagModel_REC> res = collection.Find(filter).ToList();
            return Json(res.ToDataSourceResult(poRequest), JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create([DataSourceRequest] DataSourceRequest poRequest, WearableTagModel_REC poRecord)
        {
            IMongoDatabase database = Constant.client.GetDatabase(Constant.dbName);
            var collectionPersonTag = database.GetCollection<WearableTagModel_REC>("PersonTags");
            if ((poRecord != null) && (ModelState.IsValid))
            {
                try
                {
                    var builderPersonTag = Builders<WearableTagModel_REC>.Filter;
                    var flt = builderPersonTag.Eq<string>("tagID", poRecord.tagID);
                    WearableTagModel_REC dataTags = collectionPersonTag.Find(flt).FirstOrDefault();
                    if (poRecord.tagID == null)
                    {
                        ModelState.AddModelError("Error", "Wearable Tags Required");
                    }else if (poRecord.active == null)
                    {
                        ModelState.AddModelError("Error", "Active Status Required");
                    }else if (dataTags != null)
                    {
                        ModelState.AddModelError("Error", "Wearable ID already registered");
                    }
                    else
                    {
                        WearableTagModel_REC oData = new WearableTagModel_REC();
                        oData.RecordTimestamp = DateTime.Now;
                        oData.personID = poRecord.personID;
                        oData.tagID = poRecord.tagID;
                        oData.active = poRecord.active;
                        oData.stationNo = poRecord.stationNo;
                        collectionPersonTag.InsertOne(oData);
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("Error", ex.Message);
                }

            }
            return Json(new[] { poRecord }.ToDataSourceResult(poRequest, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update([DataSourceRequest] DataSourceRequest poRequest, WearableTagModel_REC poRecord)
        {
            if ((poRecord != null) && (ModelState.IsValid))
            {
                IMongoDatabase database = Constant.client.GetDatabase(Constant.dbName);
                var collection = database.GetCollection<WearableTagModel_REC>("PersonTags");
                try
                {
                    var builderPersonTag = Builders<WearableTagModel_REC>.Filter;
                    var fltCheck = builderPersonTag.Eq<string>("stationNo", poRecord.stationNo);
                    fltCheck &= builderPersonTag.Ne<string>("tagID", poRecord.tagID);
                    WearableTagModel_REC dataTags = collection.Find(fltCheck).FirstOrDefault();
                    if (poRecord.active == null)
                    {
                        ModelState.AddModelError("Error", "Active Status Required");
                    }else if (dataTags == null)
                    {

                        var builder = Builders<WearableTagModel_REC>.Filter;
                        var flt = builder.Eq<string>("tagID", poRecord.tagID);
                        var update = Builders<WearableTagModel_REC>.Update.Set("RecordTimestamp", DateTime.Now)
                            .Set("active", poRecord.active);
                        collection.UpdateOne(flt, update);
                    }
                    else
                    {
                        if (poRecord.personID != null)
                        {
                            ModelState.AddModelError("Error", "Wearable Tags already registered");
                        }
                        else
                        {
                            var builder = Builders<WearableTagModel_REC>.Filter;
                            var flt = builder.Eq<string>("tagID", poRecord.tagID);
                            var update = Builders<WearableTagModel_REC>.Update.Set("RecordTimestamp", DateTime.Now)
                                .Set("active", poRecord.active);
                            collection.UpdateOne(flt, update);
                        }
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("Error", ex.Message);
                }
            }

            return Json(new[] { poRecord }.ToDataSourceResult(poRequest, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Destroy([DataSourceRequest] DataSourceRequest poRequest, WearableTagModel_REC poRecord)
        {
            if (poRecord != null)
            {
                IMongoDatabase database = Constant.client.GetDatabase(Constant.dbName);
                var collection = database.GetCollection<WearableTagModel_REC>("PersonTags");
                try
                {
                    var builder = Builders<WearableTagModel_REC>.Filter;
                    var flt = builder.Eq<string>("tagID", poRecord.tagID);
                    collection.DeleteOne(flt);
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("Error", ex.Message);
                }
            }

            return Json(new[] { poRecord }.ToDataSourceResult(poRequest, ModelState));
        }

        // GET: WearableTags
        public ActionResult DataLog()
        {
            ViewData["activeMenu"] = "WearableTags";
            return View();
        }
        
        public ActionResult ReadLog([DataSourceRequest] DataSourceRequest poRequest)
        {
            IMongoDatabase database = Constant.client.GetDatabase(Constant.dbName);
            var collection = database.GetCollection<BLESmartWatchData>("SmartwatchDataLogs");
            var filter = new BsonDocument() { };
            List<BLESmartWatchData> res = collection.Find(filter).SortByDescending(f => f.RecordTimestamp).ToList();
            return Json(res.ToDataSourceResult(poRequest), JsonRequestBehavior.AllowGet);
        }
    }
}