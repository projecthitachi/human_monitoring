﻿using has.iot.Components;
using has.iot.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace has.iot.Controllers
{
    public class HumanConfigurationController : Controller
    {
        // GET: HumanConfiguration
        public ActionResult Index()
        {
            IMongoDatabase database = Constant.client.GetDatabase(Constant.dbName);
            var collection = database.GetCollection<HumanConfiguration>("HumanConfiguration");
            var filter = new BsonDocument() { };
            HumanConfiguration res = collection.Find(filter).Limit(1).FirstOrDefault();
            ViewData["Configuration"] = res;
            ViewData["activeMenu"] = "HumanConfiguration";
            return View();
        }

        public ActionResult Update(HumanConfiguration poData)
        {
            Dictionary<string, string> ResultData = new Dictionary<string, string>();
            try
            {
                IMongoDatabase database = Constant.client.GetDatabase(Constant.dbName);
                var collection = database.GetCollection<HumanConfiguration>("HumanConfiguration");
                var filter = new BsonDocument() { };
                HumanConfiguration res = collection.Find(filter).Limit(1).FirstOrDefault();
                var activatedHR = poData.activatedHR == "Yes" ? "Y" : "N";
                var activatedTM = poData.activatedTM == "Yes" ? "Y" : "N";
                var activatedSOS = poData.activatedSOS == "Yes" ? "Y" : "N";
                var activatedTEM = poData.activatedTEM == "Yes" ? "Y" : "N";
                var activatedBattery = poData.activatedBattery == "Yes" ? "Y" : "N";
                var activatedGeofencing = poData.activatedGeofencing == "Yes" ? "Y" : "N";
                var activatedFallDown = poData.activatedFallDown == "Yes" ? "Y" : "N";
                var activatedBP = poData.activatedBP == "Yes" ? "Y" : "N";
                if (res == null)
                {
                    ResultData.Add("msg", "No Data");
                    ResultData.Add("errorcode", "0");
                }
                else
                {
                    var update = Builders<HumanConfiguration>.Update.Set("recordTimestamp", DateTime.Now)
                        .Set("activatedHR", activatedHR)
                        .Set("whenBelowHR", poData.whenBelowHR)
                        .Set("whenAboveHR", poData.whenAboveHR)
                        .Set("conPeriodHR", poData.conPeriodHR)
                        .Set("resetAlertHR", poData.resetAlertHR)
                        .Set("activatedTM", activatedTM)
                        .Set("whenBelowTM", poData.whenBelowTM)
                        .Set("whenAboveTM", poData.whenAboveTM)
                        .Set("conPeriodTM", poData.conPeriodTM)
                        .Set("resetAlertTM", poData.resetAlertTM)
                        .Set("activatedSOS", activatedSOS)
                        .Set("resetAlertSOS", poData.resetAlertSOS)
                        .Set("activatedTEM", activatedTEM)
                        .Set("resetAlertTEM", poData.resetAlertTEM)
                        .Set("activatedBattery", activatedBattery)
                        .Set("whenBelowBattery", poData.whenBelowBattery)
                        .Set("conPeriodBattery", poData.conPeriodBattery)
                        .Set("resetAlertBattery", poData.resetAlertBattery)
                        .Set("activatedGeofencing", activatedGeofencing)
                        .Set("resetAlertGeofencing", poData.resetAlertGeofencing)
                        .Set("conPeriodGeofencing", poData.conPeriodGeofencing)
                        .Set("activatedFallDown", activatedFallDown)
                        .Set("conPeriodFallDown", poData.conPeriodFallDown)
                        .Set("resetAlertFallDown", poData.resetAlertFallDown)
                        .Set("activatedBP", activatedBP)
                        .Set("whenBelowBP", poData.whenBelowBP)
                        .Set("whenAboveBP", poData.whenAboveBP)
                        .Set("whenBelowBP2", poData.whenBelowBP2)
                        .Set("whenAboveBP2", poData.whenAboveBP2)
                        .Set("conPeriodBP", poData.conPeriodBP)
                        .Set("resetAlertBP", poData.resetAlertBP);
                    collection.UpdateOne(filter, update);
                }
            }
            catch (Exception e)
            {
                ResultData.Add("msg", e.Message);
                ResultData.Add("errorcode", "100");
            }
            return Json(ResultData, JsonRequestBehavior.AllowGet);
        }
    }
}