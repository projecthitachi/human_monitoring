﻿using has.iot.Components;
using has.iot.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace has.iot.Controllers
{
    public class RemarkController : Controller
    {
        // GET: Remark
        public ActionResult Index()
        {
            ViewData["activeMenu"] = "Remark";
            return View();
        }


        public ActionResult Read([DataSourceRequest] DataSourceRequest poRequest)
        {
            IMongoDatabase database = Constant.client.GetDatabase(Constant.dbName);
            var collection = database.GetCollection<RemarkModelDatabase>("Remarks");
            var filter = new BsonDocument() { };
            List<RemarkModelDatabase> res = collection.Find(filter).ToList();
            return Json(res.ToDataSourceResult(poRequest), JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create([DataSourceRequest] DataSourceRequest poRequest, RemarkModelView poRecord)
        {
            IMongoDatabase database = Constant.client.GetDatabase(Constant.dbName);
            var collectionRemarks = database.GetCollection<RemarkModelView>("Remarks");
            if ((poRecord != null) && (ModelState.IsValid))
            {
                try
                {
                    var builderRemarks = Builders<RemarkModelView>.Filter;
                    var flt = builderRemarks.Eq<string>("Remarks", poRecord.Remarks);
                    RemarkModelView dataRemarks = collectionRemarks.Find(flt).FirstOrDefault();
                    if (dataRemarks != null)
                    {
                        ModelState.AddModelError("Error", "Remarks already registered");
                    }else if (poRecord.Remarks == null)
                    {
                        ModelState.AddModelError("Error", "Remarks Required");
                    }
                    else if (poRecord.status == null)
                    {
                        ModelState.AddModelError("Error", "Active Status Required");
                    }
                    else
                    {
                        if (poRecord.Remarks != null)
                        {
                            RemarkModelView oData = new RemarkModelView();
                            oData.RecordTimestamp = DateTime.Now;
                            oData.Remarks = poRecord.Remarks;
                            oData.status = poRecord.status;
                            collectionRemarks.InsertOne(oData);
                        }
                        else
                        {
                            ModelState.AddModelError("Error", "Remarks is empty");
                        }
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("Error", ex.Message);
                }

            }
            return Json(new[] { poRecord }.ToDataSourceResult(poRequest, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update([DataSourceRequest] DataSourceRequest poRequest, RemarkModelView poRecord)
        {
            if ((poRecord != null) && (ModelState.IsValid))
            {
                IMongoDatabase database = Constant.client.GetDatabase(Constant.dbName);
                var collection = database.GetCollection<RemarkModelView>("Remarks");
                try
                {
                    var builderRemarks = Builders<RemarkModelView>.Filter;
                    var fltCheck = builderRemarks.Eq<string>("Remarks", poRecord.Remarks);
                    RemarkModelView dataRemarks = collection.Find(fltCheck).FirstOrDefault();
                    if (dataRemarks == null)
                    {
                        ModelState.AddModelError("Error", "Remarks not found");
                    }
                    else if (poRecord.Remarks == null)
                    {
                        ModelState.AddModelError("Error", "Remarks Required");
                    }
                    else if (poRecord.status == null)
                    {
                        ModelState.AddModelError("Error", "Active Status Required");
                    }
                    else
                    {
                        var update = Builders<RemarkModelView>.Update.Set("RecordTimestamp", DateTime.Now)
                            .Set("status", poRecord.status);
                        collection.UpdateOne(fltCheck, update);
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("Error", ex.Message);
                }
            }

            return Json(new[] { poRecord }.ToDataSourceResult(poRequest, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Destroy([DataSourceRequest] DataSourceRequest poRequest, RemarkModelView poRecord)
        {
            if (poRecord != null)
            {
                IMongoDatabase database = Constant.client.GetDatabase(Constant.dbName);
                var collection = database.GetCollection<RemarkModelView>("Remarks");
                try
                {
                    var builder = Builders<RemarkModelView>.Filter;
                    var flt = builder.Eq<string>("Remarks", poRecord.Remarks);
                    collection.DeleteOne(flt);
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("Error", ex.Message);
                }
            }

            return Json(new[] { poRecord }.ToDataSourceResult(poRequest, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult getRemarks()
        {
            IMongoDatabase database = Constant.client.GetDatabase(Constant.dbName);
            var collection = database.GetCollection<RemarkModelView>("Remarks");
            List<RemarkModelView> res = collection.Find(val => val.status == "Active").SortBy(val => val.Remarks).ToList();
            List<RemarkModelView> listData = new List<RemarkModelView>();
            if (res != null)
            {
                foreach (RemarkModelView data in res)
                {
                    RemarkModelView dataRemarks = new RemarkModelView();
                    dataRemarks.Remarks = data.Remarks;
                    listData.Add(dataRemarks);
                }
            }

            return Json(listData, JsonRequestBehavior.AllowGet);
        }
    }
}