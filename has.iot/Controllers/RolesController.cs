﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using MongoDB.Driver;
using has.iot.Components;
using has.iot.Models;
using MongoDB.Bson;

namespace has.iot.Controllers
{
    public class RolesController : Controller
    {
        // GET: Roles
        public ActionResult Index()
        {
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Read([DataSourceRequest] DataSourceRequest poRequest)
        {
            var database = Constant.client.GetDatabase(Constant.dbName);
            var collection = database.GetCollection<Role>("Roles");
            var filter = new BsonDocument() { };
            List<Role> resDb = collection.Find(filter).ToList();
            List<RoleGrid> res = new List<RoleGrid>();
            foreach (Role voData in resDb)
            {
                RoleGrid resval = new RoleGrid();
                resval.id = voData.id;
                resval.rlId = voData.rlId;
                resval.rlName = voData.rlName;
                resval.acsMod = voData.acsMod;
                resval.crtOn = voData.crtOn;
                resval.crtAt = voData.crtAt;
                resval.crtBy = voData.crtBy;
                resval.uptOn = voData.uptOn.ToString("dd/MM/yyyy hh:mm:ss");
                resval.uptAt = voData.uptAt;
                resval.uptBy = voData.uptBy;
                res.Add(resval);
            }


            return Json(res.ToDataSourceResult(poRequest), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Add([DataSourceRequest] DataSourceRequest poRequest)
        {
            IMongoDatabase database = Constant.client.GetDatabase(Constant.dbName);
            IMongoCollection<Moduls> Collections = database.GetCollection<Moduls>("Moduls");

            var vResult = Collections.Find(f => f.active == true).SortBy(s => s.order).ToList();

            ViewData["moduls"] = vResult;
            return View();
        }


        public ActionResult Detail(string id)
        {
            IMongoDatabase database = Constant.client.GetDatabase(Constant.dbName);
            IMongoCollection<Moduls> Collections = database.GetCollection<Moduls>("Moduls");

            var vResult = Collections.Find(f => f.active == true).SortBy(s => s.order).ToList();

            ViewData["moduls"] = vResult;
            ViewData["id"] = id;
            return View();
        }

        [HttpGet]
        public ActionResult RolesReadDetail(string id)
        {
            IMongoDatabase database = Constant.client.GetDatabase(Constant.dbName);
            IMongoCollection<Role> Collections = database.GetCollection<Role>("Roles");

            var vResult = Collections.Find(x => x.rlId == id).SingleOrDefault();
            if (vResult == null)
            {
                vResult = Collections.Find(x => x.id == Convert.ToInt64(id)).SingleOrDefault();
            }
            var jsonResult = Json(vResult, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        public ActionResult RolesCreate(Role voTable)
        {
            ApiResponses ResultData = new ApiResponses();
            var session = Constant.client.StartSession();
            try
            {
                IMongoDatabase database = session.Client.GetDatabase(Constant.dbName);
                IMongoCollection<Role> Collections = database.GetCollection<Role>("Roles");

                if (voTable.rlName == null) { throw new NullReferenceException("please insert role name!"); }

                if (voTable.rlName.Length > 20)
                {
                    throw new NullReferenceException("role name is too long max 20 character");
                }
                if (voTable.rlName.Length > 0)
                {
                    if (Constant.formulaInj.Contains(voTable.rlName[0]))
                    {
                        throw new NullReferenceException("formula detected!");
                    }
                }
                voTable.rlName = voTable.rlName;

                voTable.id = new Sequence() { }.GetNextSequenceValue("Roles", Constant.dbName);
                voTable.crtOn = DateTime.Now;
                voTable.rlId = "R" + voTable.id.ToString("D3");
                Collections.InsertOne(session, voTable);

                ResultData.errorcode = 0;
                ResultData.title = "Success";
                ResultData.message = "Add Roles success";
                ResultData.data = voTable;
            }
            catch (Exception ex)
            {
                ResultData.errorcode = 100;
                ResultData.title = "failed";
                ResultData.message = ex.Message;
            }
            return Json(ResultData, JsonRequestBehavior.DenyGet);
        }

        public ActionResult RolesUpdate(Role poData)
        {
            ApiResponses ResultData = new ApiResponses();
            var session = Constant.client.StartSession();
            try
            {
                IMongoDatabase database = session.Client.GetDatabase(Constant.dbName);
                IMongoCollection<Role> Collections = database.GetCollection<Role>("Roles");

                if (poData.rlName == null) { throw new NullReferenceException("please insert role name!"); }

                var evntBld = Builders<Role>.Filter;
                var evntFlt = evntBld.Eq<long>("id", poData.id);
                var evntRes = Collections.Find(evntFlt).SingleOrDefault();
                if (evntRes == null)
                {
                    throw new NullReferenceException("role not found!");
                }

                if (evntRes.uptOn.ToString("dd/MM/yyyy hh:mm:ss") != poData.uptOn.ToString("dd/MM/yyyy hh:mm:ss"))
                {
                    throw new NullReferenceException("Another user has modified or deleted this record.");
                }
                if (poData.rlName.Length > 20)
                {
                    throw new NullReferenceException("role name is too long max 20 character");
                }
                if (poData.rlName.Length > 0)
                {
                    if (Constant.formulaInj.Contains(poData.rlName[0]))
                    {
                        throw new NullReferenceException("formula detected!");
                    }
                }
                poData.rlName = poData.rlName;

                var set = Builders<Role>.Update
                    .Set("uptOn", DateTime.Now)
                    .Set("uptBy", poData.uptBy)
                    .Set("uptAt", poData.uptAt)
                    .Set("rlName", poData.rlName)
                    .Set("acsMod", poData.acsMod);
                Collections.UpdateOne(session, evntFlt, set);


                ResultData.errorcode = 0;
                ResultData.title = "Success";
                ResultData.message = "Update Roles success";
                ResultData.data = poData;
            }
            catch (Exception ex)
            {
                ResultData.errorcode = 100;
                ResultData.title = "failed";
                ResultData.message = ex.Message;
            }
            return Json(ResultData, JsonRequestBehavior.DenyGet);
        }

        public ActionResult RolesDelete(Role poData)
        {
            ApiResponses ResultData = new ApiResponses();
            var session = Constant.client.StartSession();
            try
            {
                IMongoDatabase database = session.Client.GetDatabase(Constant.dbName);
                IMongoCollection<Users> CollUser = database.GetCollection<Users>("Users");
                IMongoCollection<Role> Collections = database.GetCollection<Role>("Roles");

                var evntBld = Builders<Role>.Filter;
                var evntFlt = evntBld.Eq<long>("id", poData.id);
                var evntRes = Collections.Find(evntFlt).SingleOrDefault();
                if (evntRes == null)
                {
                    throw new NullReferenceException("role not found");
                }

                if (poData.rlId == "R001") { throw new NullReferenceException("You cannot delete admin roles"); }

                var checkBld = Builders<Users>.Filter;
                var checkFlt = checkBld.Eq<string>("rlId", evntRes.rlId);
                var checkRes = CollUser.Find(checkFlt).ToList();
                if (checkRes.Count > 0)
                {
                    throw new NullReferenceException("cannot delete, the role is in use by the user!");
                }

                Collections.DeleteOne(session, evntFlt);

                ResultData.errorcode = 0;
                ResultData.title = "Success";
                ResultData.message = "Delete Roles success";
                ResultData.data = evntRes;
            }
            catch (Exception ex)
            {

                ResultData.errorcode = 100;
                ResultData.title = "failed";
                ResultData.message = ex.Message;
            }
            return Json(ResultData, JsonRequestBehavior.DenyGet);
        }
    }
}