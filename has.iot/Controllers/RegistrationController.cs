﻿using has.iot.Components;
using has.iot.Models;
using has.stayathome.monitoring.Components;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
namespace has.iot.Controllers
{
    public class RegistrationController : Controller
    {
        // GET: AHU
        public ActionResult Index()
        {
            ViewData["activeMenu"] = "Registration";
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create([DataSourceRequest] DataSourceRequest poRequest, ProfileMaster_REC poRecord)
        {
            ApiResponses ResultData = new ApiResponses();
            try{
                IMongoDatabase database = Constant.client.GetDatabase(Constant.dbName);
                var collection = database.GetCollection<ProfileMaster>("Person");
                var collectionTags = database.GetCollection<WearableTagModel_REC>("PersonTags");


                if (poRecord.stationNo == null || poRecord.stationNo == "")
                {
                    throw new NullReferenceException("Station No required!");
                }

                if (poRecord.DefaultWearable == null || poRecord.DefaultWearable == "")
                {
                    if (poRecord.status == "Active")
                    {
                        throw new NullReferenceException("Wearable Tags required!");
                    }
                }

                if (poRecord.defaultZone == null || poRecord.defaultZone == "")
                {
                    throw new NullReferenceException("Location required!");
                }


                var builder = Builders<ProfileMaster>.Filter;
                var flt = builder.Eq<string>("stationNo", poRecord.stationNo);
                ProfileMaster personData = collection.Find(flt).FirstOrDefault();
                if (personData == null)
                {
                    if ((poRecord != null) && (ModelState.IsValid))
                    {
                        ProfileMaster oData = new ProfileMaster();
                        oData.RecordTimestamp = DateTime.Now;
                        oData.personID = poRecord.personID;
                        oData.personName = poRecord.personName;
                        oData.LastName = poRecord.LastName;
                        oData.Age = poRecord.Age;
                        oData.Gender = poRecord.Gender;
                        oData.HealthRemarks = poRecord.HealthRemarks;
                        oData.CaseRemarks = poRecord.CaseRemarks;
                        oData.PeriodStart = poRecord.PeriodStart;
                        oData.PeriodEnd = poRecord.PeriodEnd;
                        oData.DefaultWearable = poRecord.DefaultWearable;
                        oData.WearableActivated = poRecord.WearableActivated;
                        oData.SosButton = poRecord.SosButton;
                        oData.avatarID = "f0-_o-person--";
                        oData.defaultZone = poRecord.defaultZone;
                        oData.photo = poRecord.photo;
                        oData.stationNo = poRecord.stationNo;
                        oData.status = poRecord.status;
                        collection.InsertOne(oData);

                        var builderTags2 = Builders<WearableTagModel_REC>.Filter;
                        var fltTags2 = builderTags2.Eq<string>("tagID", poRecord.DefaultWearable);
                        var updateTags2 = Builders<WearableTagModel_REC>.Update.Set("RecordTimestamp", DateTime.Now)
                            .Set("stationNo", poRecord.stationNo);
                        collectionTags.UpdateOne(fltTags2, updateTags2);

                        ResultData.errorcode = 0;
                        ResultData.title = "Success";
                        ResultData.message = "Register Data success";
                        ResultData.data = poRecord.stationNo;
                    }
                }
                else
                {
                    ResultData.errorcode = 100;
                    ResultData.title = "failed";
                    ResultData.message = "Station No already exist";
                    ResultData.data = poRecord.stationNo;
                }
            }
            catch (Exception ex)
            {
                ResultData.errorcode = 100;
                ResultData.title = "failed";
                ResultData.message = ex.Message;
            }
            return Json(ResultData, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult getWearableTags()
        {
            IMongoDatabase database = Constant.client.GetDatabase(Constant.dbName);
            var collection = database.GetCollection<WearableTagModel_REC>("PersonTags");
            List<WearableTagModel_REC> res = collection.Find(f => f.stationNo == null || f.stationNo == "" && f.active == "yes").ToList();

            return Json(res, JsonRequestBehavior.AllowGet);
        }
    }
}