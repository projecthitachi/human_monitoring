﻿using has.iot.Components;
using has.iot.Models.System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace has.iot.Models
{
    public class SmartWatchGatewayView
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public string gatewayID { get; set; }
        public string gatewayName { get; set; }
        public string ipAddress { get; set; }
        public string location { get; set; }
        public string status { get; set; }
    }
    public class SmartWatchGatewayModel
    {
        public int ErrorNo { get; set; }
        public string ErrorMessage { get; set; }

        public SmartWatchGatewayModel()
        {
            ErrorNo = 0;
            ErrorMessage = "";
        }
        [BsonId]
        public ObjectId _id { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public string gatewayID { get; set; }
        public string gatewayName { get; set; }
        public string ipAddress { get; set; }
        public string location { get; set; }
        public string status { get; set; }
    }
    public class SmartWatchGatewayModelDatabase
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public string gatewayID { get; set; }
        public string gatewayName { get; set; }
        public string ipAddress { get; set; }
        public string location { get; set; }
        public string status { get; set; }
    }
}