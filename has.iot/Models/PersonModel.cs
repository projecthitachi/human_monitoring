﻿using has.iot.Components;
using has.iot.Models.System;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace has.iot.Models
{
    public class Person_REC
    {
        public long RecordID { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        //------------------------------
        [Key]
        public string PersonID { get; set; }
        public string PersonName { get; set; }
        public string EPC { get; set; }
    }

    public class PersonModel
    {
        public int ErrorNo { get; set; }
        public string ErrorMessage { get; set; }

        public PersonModel()
        {
            ErrorNo = 0;
            ErrorMessage = "";
        }
        #region "t8040fieldselect"
        private string t8040fieldselect = @"
            t8040r001 as RecordID,
            t8040r002 as RecordTimestamp,
            t8040r003 as RecordStatus,
            t8040f001 as PersonID,
            t8040f002 as PersonName,
            t8040f003 as EPC
        ";
        #endregion
        public DbRawSqlQuery<Person_REC> GetList()
        {
            DatabaseContext oRemoteDB = new DatabaseContext();
            string sSQL = "select "+t8040fieldselect+" from t8040 order by t8040r001 desc";
            var vQuery = oRemoteDB.Database.SqlQuery<Person_REC>(sSQL);
            return vQuery;
        }
        public DbRawSqlQuery<Person_REC> GetPersonByEPC(string sEPC)
        {
            DatabaseContext oRemoteDB = new DatabaseContext();
            string sSQL = "SELECT "+ t8040fieldselect + " from t8040 where t8040f003 = '"+sEPC+"'";
            var vQuery = oRemoteDB.Database.SqlQuery<Person_REC>(sSQL);
            return vQuery;
        }
        public bool Insert(DatabaseContext oRemoteDB, Person_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            try
            {
                poRecord.RecordID = 0;
                poRecord.RecordTimestamp = DateTime.Now;
                poRecord.RecordStatus = 0;

                //----------
                GlobalFunction.TrimNull(poRecord);
                string sSQL = "" +
                    "INSERT INTO t8040 " +
                    "      ( " +
                    "      t8040r002, t8040r003, " +
                    "      t8040f001, t8040f002," +
                    "      t8040f003 " +
                    "      ) " +
                    "      VALUES " +
                    "      (" +
                    "      @pt8040r002, @pt8040r003, " +
                    "      @pt8040f001, @pt8040f002, " +
                    "      @pt8040f003 " +
                    "      )" +
                    ";" +
                    "SELECT @pt8040r001 = SCOPE_IDENTITY(); "
                    ;

                List<SqlParameter> oParameters = new List<SqlParameter>();
                oParameters.Add(new SqlParameter { ParameterName = "@pt8040r001", SqlDbType = SqlDbType.BigInt, Direction = ParameterDirection.Output });
                oParameters.Add(new SqlParameter("@pt8040r002", poRecord.RecordTimestamp));
                oParameters.Add(new SqlParameter("@pt8040r003", poRecord.RecordStatus));

                oParameters.Add(new SqlParameter("@pt8040f001", poRecord.PersonID));
                oParameters.Add(new SqlParameter("@pt8040f002", poRecord.PersonName));
                oParameters.Add(new SqlParameter("@pt8040f003", poRecord.EPC));
                SqlParameter[] vSqlParameter = oParameters.ToArray();

                int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);

                if (nReturn == 1)
                {
                    poRecord.RecordID = Convert.ToInt64(vSqlParameter[0].Value);
                }
                else
                {
                    ErrorMessage = "Failed to insert record!";
                    bReturn = false;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                bReturn = false;
            }
            //------------------------------
            return bReturn;
        }

        public bool Update(DatabaseContext oRemoteDB, Person_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            //DateTime dRecordTimestamp = poRecord.RecordTimestamp;
            //----------
            poRecord.RecordID = 0;
            poRecord.RecordTimestamp = DateTime.Now;
            poRecord.RecordStatus = 0;
            //poRecord.RecordFlag = 0;
            //----------
            GlobalFunction.TrimNull(poRecord);
            string sSQL = "" +
                "UPDATE t8040 " +
                "   SET " +
                "      t8040f002 = @pt8040f002, " +
                "      t8040f003 = @pt8040f003 " +
                "   WHERE (t8040f001 = @pt8040f001) " +
                ";"
                ;

            List<SqlParameter> oParameters = new List<SqlParameter>();
            //oParameters.Add(new SqlParameter("@pRecordTimestamp", dRecordTimestamp));
            oParameters.Add(new SqlParameter("@pt8040f001", poRecord.PersonID));
            oParameters.Add(new SqlParameter("@pt8040f002", poRecord.PersonName));
            oParameters.Add(new SqlParameter("@pt8040f003", poRecord.EPC));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            //var oTransaction = oRemoteDB.Database.BeginTransaction();
            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
            if (nReturn == 1)
            {

            }
            else
            {
                //oTransaction.Rollback();
                ErrorMessage = "Failed to update record!" + "<br>" + "Record has been updated or deleted by another User.";
                bReturn = false;
                throw new Exception(ErrorMessage);
            }
            //------------------------------
            return bReturn;
        }
        public bool Delete(DatabaseContext oRemoteDB, Person_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            //poRecord.RecordTimestamp = DateTime.Now;
            //----------
            string sSQL = "" +
                "DELETE FROM t8040 " +
                "   WHERE (t8040f001 = @pt8040f001) " +
                ";"
                ;

            List<SqlParameter> oParameters = new List<SqlParameter>();
            oParameters.Add(new SqlParameter("@pt8040f001", poRecord.PersonID));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
            if (nReturn == 1)
            {

            }
            else
            {
                ErrorMessage = "Failed to delete record!" + "<br>" + "Record has been deleted by another Logging.";
                bReturn = false;
                throw new Exception(ErrorMessage);
            }
            //------------------------------
            return bReturn;
        }
    }
}