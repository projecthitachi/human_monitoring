﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace has.iot.Models
{
    public class AuditLog
    {
        public long id { get; set; }
        public string menu { get; set; }
        public string act { get; set; }
        public string usrId { get; set; }
        public string usrName { get; set; }


        public DateTime crtOn { get; set; }
        public string crtAt { get; set; }
        public string crtBy { get; set; }


        public DateTime? uptOn { get; set; }
        public string uptAt { get; set; }
        public string uptBy { get; set; }
    }
    public class AuditLogGrid
    {
        public string id { get; set; }
        public string menu { get; set; }
        public string act { get; set; }
        public string usrId { get; set; }
        public string usrName { get; set; }


        public DateTime crtOn { get; set; }
        public string crtAt { get; set; }
        public string crtBy { get; set; }


        public DateTime? uptOn { get; set; }
        public string uptAt { get; set; }
        public string uptBy { get; set; }
    }
    public class AuditLogDetail
    {
        public long id { get; set; }
        public long logId { get; set; }
        public string fld { get; set; }
        public string prevVal { get; set; }
        public string crtVal { get; set; }


        public DateTime crtOn { get; set; }
        public string crtAt { get; set; }
        public string crtBy { get; set; }


        public DateTime? uptOn { get; set; }
        public string uptAt { get; set; }
        public string uptBy { get; set; }
    }
    public class AuditLogDetailGrid
    {
        public string id { get; set; }
        public string logId { get; set; }
        public string fld { get; set; }
        public string prevVal { get; set; }
        public string crtVal { get; set; }


        public DateTime crtOn { get; set; }
        public string crtAt { get; set; }
        public string crtBy { get; set; }


        public DateTime? uptOn { get; set; }
        public string uptAt { get; set; }
        public string uptBy { get; set; }
    }
}