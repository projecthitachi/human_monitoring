﻿using has.iot.Components;
using has.iot.Models.System;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace has.iot.Models.Services
{
    public class Services
    {
        private long t8020r001;
        [Display(Name = "RecordID")]
        public virtual long RecordID { get { return t8020r001; } set { t8020r001 = value; } }

        private DateTime t8020r002;
        [Display(Name = "RecordTimestamp")]
        public virtual DateTime RecordTimestamp { get { return t8020r002; } set { t8020r002 = value; } }

        private Int32 t8020r003;
        [Display(Name = "Record Status")]
        public virtual Int32 RecordStatus { get { return t8020r003; } set { t8020r003 = value; } }

        private string t8020f001;
        [Display(Name = "Services Name")]
        public virtual string ServicesName { get { return t8020f001; } set { t8020f001 = value; } }

        private Int32 t8020f002;
        [Display(Name = "Config")]
        public virtual Int32 Config { get { return t8020f002; } set { t8020f002 = value; } }

        [Display(Name = "Status")]
        public virtual string StatusName { get; set; }
        [Display(Name = "Config")]
        public virtual string ConfigName { get; set; }
        public virtual string Alias { get; set; }
    }

    public class ServiceQuery
    {
        private string t8020FieldsSelect = @"
            t8020r001 as RecordID,
            t8020r002 as RecordTimestamp,
            t8020r003 as RecordStatus,
            t8020f001 as ServicesName,
            t8020f002 as Config,
            t8020f003 as Alias
        ";

        public string ErrorMessage = "";

        public DbRawSqlQuery<Services> ServicesGetList(DatabaseContext oRemoteDB)
        {
            string sSQL = "SELECT " + t8020FieldsSelect + ", t8990f002 as ConfigName from t8020 LEFT JOIN t8990 on t8990r001 = t8020f002;";
            var vQuery = oRemoteDB.Database.SqlQuery<Services>(sSQL);
            return vQuery;
        }
        public DbRawSqlQuery<Services> ServicesGetByRecordID(DatabaseContext oRemoteDB, long plRecordID)
        {
            string sSQL = "SELECT " + t8020FieldsSelect + ", t8990f002 as ConfigName from t8020 LEFT JOIN t8990 on t8990r001 = t8020f002 where t8020r001 = '" + plRecordID + "';";
            var vQuery = oRemoteDB.Database.SqlQuery<Services>(sSQL);
            return vQuery;
        }

        public bool Insert(DatabaseContext oRemoteDB, Services poRecord)
        {
            bool bReturn = true;
            //------------------------------
            try
            {
                poRecord.RecordID = 0;
                poRecord.RecordTimestamp = DateTime.Now;
                poRecord.RecordStatus = 0;

                //----------
                GlobalFunction.TrimNull(poRecord);
                string sSQL = "" +
                    "INSERT INTO t8020 " +
                    "      ( " +
                    "      t8020r002, t8020r003, " +
                    "      t8020f001, t8020f002, t8020f003 " +
                    "      ) " +
                    "      VALUES " +
                    "      (" +
                    "      @pt8020r002, @pt8020r003, " +
                    "      @pt8020f001, @pt8020f002, @pt8020f003 " +
                    "      )" +
                    ";" +
                    "SELECT @pt8020r001 = SCOPE_IDENTITY(); "
                    ;

                List<SqlParameter> oParameters = new List<SqlParameter>();
                oParameters.Add(new SqlParameter { ParameterName = "@pt8020r001", SqlDbType = SqlDbType.BigInt, Direction = ParameterDirection.Output });
                oParameters.Add(new SqlParameter("@pt8020r002", poRecord.RecordTimestamp));
                oParameters.Add(new SqlParameter("@pt8020r003", poRecord.RecordStatus));

                oParameters.Add(new SqlParameter("@pt8020f001", poRecord.ServicesName));
                oParameters.Add(new SqlParameter("@pt8020f002", poRecord.Config));
                oParameters.Add(new SqlParameter("@pt8020f003", poRecord.Alias));
                SqlParameter[] vSqlParameter = oParameters.ToArray();

                int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);

                if (nReturn == 1)
                {
                    poRecord.RecordID = Convert.ToInt64(vSqlParameter[0].Value);
                }
                else
                {
                    ErrorMessage = "Failed to insert record!";
                    bReturn = false;
                    throw new Exception(ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                bReturn = false;
            }
            //------------------------------
            return bReturn;
        }
        public bool Update(DatabaseContext oRemoteDB, Services poRecord)
        {
            bool bReturn = true;
            //------------------------------
            //DateTime dRecordTimestamp = poRecord.RecordTimestamp;
            //----------
            //poRecord.RecordID = 0;
            poRecord.RecordTimestamp = DateTime.Now;
            poRecord.RecordStatus = 0;
            //poRecord.RecordFlag = 0;
            //----------
            GlobalFunction.TrimNull(poRecord);
            string sSQL = "" +
                "UPDATE t8020 " +
                "   SET " +
                "      t8020f001 = @pt8020f001, " +
                "      t8020f002 = @pt8020f002, " +
                "      t8020f003 = @pt8020f003 " +
                "   WHERE (t8020r001 = @pt8020r001) " +
                ";"
                ;

            List<SqlParameter> oParameters = new List<SqlParameter>();
            //oParameters.Add(new SqlParameter("@pRecordTimestamp", dRecordTimestamp));
            oParameters.Add(new SqlParameter("@pt8020r001", poRecord.RecordID));
            oParameters.Add(new SqlParameter("@pt8020f001", poRecord.ServicesName));
            oParameters.Add(new SqlParameter("@pt8020f002", poRecord.Config));
            oParameters.Add(new SqlParameter("@pt8020f003", poRecord.Alias));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            //var oTransaction = oRemoteDB.Database.BeginTransaction();
            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
            if (nReturn == 1)
            {

            }
            else
            {
                //oTransaction.Rollback();
                ErrorMessage = "Failed to update record!" + "<br>" + "Record has been updated or deleted by another User.";
                bReturn = false;
                throw new Exception(ErrorMessage);
            }
            //------------------------------
            return bReturn;
        }
        public bool Delete(DatabaseContext oRemoteDB, Services poRecord)
        {
            bool bReturn = true;
            //------------------------------
            //poRecord.RecordTimestamp = DateTime.Now;
            //----------
            string sSQL = "" +
                "DELETE FROM t8020 " +
                "   WHERE (t8020f001 = @pt8020f001) " +
                ";"
                ;

            List<SqlParameter> oParameters = new List<SqlParameter>();
            oParameters.Add(new SqlParameter("@pt8020f001", poRecord.ServicesName));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
            if (nReturn == 1)
            {

            }
            else
            {
                ErrorMessage = "Failed to delete record!" + "<br>" + "Record has been deleted by another Logging.";
                bReturn = false;
                throw new Exception(ErrorMessage);
            }
            //------------------------------
            return bReturn;
        }
    }
}