﻿using has.iot.Components;
using has.iot.Models.System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace has.iot.Models
{
    public class Users_REC
    {
        public long recordID { get; set; }
        public DateTime recordTimestamp { get; set; }
        public int recordStatus { get; set; }
        //------------------------------
        [Key]
        public string uuid { get; set; }
        public string password { get; set; }
        public DateTime birthDate { get; set; }
        public int age { get; set; }
        public string gender { get; set; }
        public string race { get; set; }
        public string raceSpecify { get; set; }
        public decimal height { get; set; }
        public decimal weight { get; set; }

    }
    public class UsersView_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public long id { get; set; }
        public string uuid { get; set; }
        public string usrId { get; set; }
        public string usrName { get; set; }
        public string usrPass { get; set; }
        public string fstName { get; set; }
        public string lstName { get; set; }
        public string rlId { get; set; }
        public string rlName { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime crtOn { get; set; }
        public string crtAt { get; set; }
        public string crtBy { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime uptOn { get; set; }
        public string uptAt { get; set; }
        public string uptBy { get; set; }
        public string sess { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime sessLt { get; set; }

    }
    public class UsersData
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public long recordID { get; set; }
        public DateTime recordTimestamp { get; set; }
        public int recordStatus { get; set; }
        //------------------------------
        [Key]
        public string uuid { get; set; }
        public string password { get; set; }
        public DateTime? birthDate { get; set; }
        public int age { get; set; }
        public string gender { get; set; }
        public string race { get; set; }
        public string raceSpecify { get; set; }
        public decimal height { get; set; }
        public decimal weight { get; set; }

        public Persons person { get; set; }
    }
    public class Persons
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public DateTime RecordTimestamp { get; set; }
        //------------------------------
        [Key]
        public string personID { get; set; }
        public string personName { get; set; }
        public string LastName { get; set; }
        public string Age { get; set; }
        public string Gender { get; set; }
        public string HealthRemarks { get; set; }
        public string CaseRemarks { get; set; }
        public string PeriodStart { get; set; }
        public string PeriodEnd { get; set; }
        public string DefaultWearable { get; set; }
        public string WearableActivated { get; set; }
        public string SosButton { get; set; }
        public string avatarID { get; set; }
        public string defaultZone { get; set; }
        public string photo { get; set; }
    }

    public class SmartDevices_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public string DeviceID { get; set; }
        public string DeviceName { get; set; }
        public string IPAddress { get; set; }
        public string ObjectName { get; set; }
    }
    public class Feedback_REC
    {
        public long recordID { get; set; }

        [BsonDateTimeOptions(Kind=DateTimeKind.Local)]
        public DateTime recordTimestamp { get; set; }

        public int recordStatus { get; set; }
        //------------------------------
        [Key]
        public string uuid { get; set; }
        public int age { get; set; }
        public string gender { get; set; }
        public string race { get; set; }

        public int? locationID { get; set; }
        public string location { get; set; }

        public int? thermalSensationID { get; set; }
        public string thermalSensation { get; set; }

        public int? thermalComfortSatisfactionID { get; set; }
        public string thermalComfortSatisfaction { get; set; }

        public int? dressCodeID { get; set; }
        public string dressCode { get; set; }
    }
    public class Alert_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public long recordID { get; set; }

        [BsonDateTimeOptions(Kind=DateTimeKind.Local)]
        public DateTime recordTimestamp { get; set; }

        public int recordStatus { get; set; }

        public string temperature { get; set; }
        public string humidity { get; set; }
        public string co2 { get; set; }
        public string airVelocity { get; set; }
        public string skinTemperature { get; set; }
        public string heartRate { get; set; }
        public string uuid { get; set; }
        public string thermalComfortSatisfaction { get; set; }
        public string thermalSensation { get; set; }
        public string location { get; set; }
    }
    public class FeedbackView_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public long recordID { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime recordTimestamp { get; set; }

        public int recordStatus { get; set; }
        //------------------------------
        [Key]
        public string uuid { get; set; }
        public int age { get; set; }
        public string gender { get; set; }
        public string race { get; set; }

        public int? locationID { get; set; }
        public string location { get; set; }

        public int? thermalSensationID { get; set; }
        public string thermalSensation { get; set; }

        public int? thermalComfortSatisfactionID { get; set; }
        public string thermalComfortSatisfaction { get; set; }

        public int? dressCodeID { get; set; }
        public string dressCode { get; set; }
    }
    public class QuestionsAnswerFeedback_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public long recordID { get; set; }
        public DateTime recordTimestamp { get; set; }
        public int recordStatus { get; set; }
        //------------------------------
        public string parentID { get; set; }

        [Key]
        public string question_id { get; set; }
        public string question { get; set; }
        public List<string> question_type { get; set; }
        public List<string> question_time { get; set; }
        public List<Answer_REC> answer { get; set; }
    }
    public class QuestionsAnswer_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public long recordID { get; set; }
        public DateTime recordTimestamp { get; set; }
        public int recordStatus { get; set; }
        //------------------------------
        [Key]
        public string question_id { get; set; }
        public string question { get; set; }
        public List<string> question_type { get; set; }
        public List<string> question_time { get; set; }
        public List<Answer_REC> answer { get; set; }
    }
    public class Answer_REC
    {
        public long recordID { get; set; }
        public DateTime recordTimestamp { get; set; }
        public int recordStatus { get; set; }
        //------------------------------
        public int index { get; set; }
        public string answer { get; set; }
        public string color { get; set; }
        public int isGroup { get; set; }
        public string groupName { get; set; }
        public List<Answer_REC> answerDetail { get; set; }
    }

    public class Sensor_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public long RecordID { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        public string DeviceID { get; set; }
        public string DeviceName { get; set; }
        public string Node { get; set; }
        public string Chanel { get; set; }
        public string PresentValue { get; set; }
        public string Units { get; set; }
    }
    public class MobileNotification_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public long recordID { get; set; }
        public DateTime recordTimestamp { get; set; }
        public int recordStatus { get; set; }
        //------------------------------
        [Key]
        public string notifTime { get; set; }
    }
}