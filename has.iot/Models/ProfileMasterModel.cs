﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace has.iot.Models
{
    public class ProfileMaster_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public string id { get; set; }
        public string personID { get; set; }
        public string personName { get; set; }
        public string LastName { get; set; }
        public string Age { get; set; }
        public string Gender { get; set; }
        public string HealthRemarks { get; set; }
        public string CaseRemarks { get; set; }
        public string PeriodStart { get; set; }
        public string PeriodEnd { get; set; }
        public string DefaultWearable { get; set; }
        public string WearableActivated { get; set; }
        public string SosButton { get; set; }
        public string avatarID { get; set; }
        public string defaultZone { get; set; }
        public string photo { get; set; }
        public string stationNo { get; set; }
        public string status { get; set; }
    }

    public class ProfileMaster
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public string personID { get; set; }
        public string personName { get; set; }
        public string LastName { get; set; }
        public string Age { get; set; }
        public string Gender { get; set; }
        public string HealthRemarks { get; set; }
        public string CaseRemarks { get; set; }
        public string PeriodStart { get; set; }
        public string PeriodEnd { get; set; }
        public string DefaultWearable { get; set; }
        public string WearableActivated { get; set; }
        public string SosButton { get; set; }
        public string avatarID { get; set; }
        public string defaultZone { get; set; }
        public string photo { get; set; }
        public string stationNo { get; set; }
        public string status { get; set; }
    }

    public class AvatarLocation
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public string avatarName { get; set; }
        public string locationName { get; set; }
        public string location { get; set; }
        public string left { get; set; }
        public string top { get; set; }
    }
}