﻿using has.iot.Models.System;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;

namespace has.iot.Models
{
    public class EventsLog
    {
        private long t9003r001;
        [Display(Name = "RecordID")]
        public virtual long RecordID { get { return t9003r001; } set { t9003r001 = value; } }

        private DateTime t9003r002;
        [Display(Name = "RecordTimestamp")]
        public virtual DateTime RecordTimestamp { get { return t9003r002; } set { t9003r002 = value; } }

        private Int32 t9003r003;
        [Display(Name = "Record Status")]
        public virtual Int32 RecordStatus { get { return t9003r003; } set { t9003r003 = value; } }

        private string t9003f001;
        [Display(Name = "ID")]
        public virtual string Category { get { return t9003f001; } set { t9003f001 = value; } }

        private string t9003f002;
        [Display(Name = "Name")]
        public virtual string Device { get { return t9003f002; } set { t9003f002 = value; } }

        private string t9003f003;
        [Display(Name = "Type")]
        public virtual string Type { get { return t9003f003; } set { t9003f003 = value; } }

        private string t9003f004;
        [Display(Name = "Messages")]
        public virtual string Messages { get { return t9003f004; } set { t9003f004 = value; } }
    }
    public class EventsLogQuery
    {
        private string t9003FieldsSelect = @"
            t9003r001 as RecordID,
            t9003r002 as RecordTimestamp,
            t9003r003 as RecordStatus,
            t9003f001 as Category,
            t9003f002 as Device,
            t9003f003 as Type,
            t9003f004 as Messages
        ";
        public DbRawSqlQuery<EventsLog> EventsLogGetList(DatabaseContext oRemoteDB)
        {
            string sSQL = "SELECT " + t9003FieldsSelect + " from t9003;";
            var vQuery = oRemoteDB.Database.SqlQuery<EventsLog>(sSQL);
            return vQuery;
        }
    }
}