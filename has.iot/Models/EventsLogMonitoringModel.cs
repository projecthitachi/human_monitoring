﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.IdGenerators;
using System;

namespace has.iot.Models
{
    public class EventsLogMonitoring
    {
        [BsonId]
        public ObjectId _id { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime RecordTimestamp { get; set; }
        public string EventType { get; set; }
        public string TagID { get; set; }
        public string personName { get; set; }
        public string NRIC { get; set; }
        public decimal Temp { get; set; }
        public int HR { get; set; }
        public string Location { get; set; }
        public string Tampered { get; set; }
        public string Acknowledge { get; set; }
        public string ActionRemarks { get; set; }
        public string Status { get; set; }
        public string SOS { get; set; }
        public int Battery { get; set; }
        public string Geofencing { get; set; }
        public string stationNo { get; set; }
        public string Blood { get; set; }
        public string Calories { get; set; }
        public string FallDown { get; set; }
    }
    public class EventsLogMonitoringView
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public ObjectId _id { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public string EventType { get; set; }
        public string TagID { get; set; }
        public string personName { get; set; }
        public string NRIC { get; set; }
        public decimal Temp { get; set; }
        public int HR { get; set; }
        public string Location { get; set; }
        public string Tampered { get; set; }
        public string Acknowledge { get; set; }
        public string ActionRemarks { get; set; }
        public string Status { get; set; }
        public string SOS { get; set; }
        public int Battery { get; set; }
        public string Geofencing { get; set; }
        public string dataID { get; set; }
        public string stationNo { get; set; }
        public string Blood { get; set; }
        public string Calories { get; set; }
        public string FallDown { get; set; }
    }
    public class EventsLogMonitoringTemp
    {
        [BsonId]
        public ObjectId _id { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime RecordTimestamp { get; set; }
        public int Count { get; set; }
        public string EventType { get; set; }
        public string TagID { get; set; }
        public string personName { get; set; }
        public string NRIC { get; set; }
        public decimal Temp { get; set; }
        public int HR { get; set; }
        public string Location { get; set; }
        public string Tampered { get; set; }
        public string Acknowledge { get; set; }
        public string ActionRemarks { get; set; }
        public string Status { get; set; }
        public string SOS { get; set; }
        public int Battery { get; set; }
        public string Geofencing { get; set; }
        public string stationNo { get; set; }
        public string Blood { get; set; }
        public string Calories { get; set; }
        public string FallDown { get; set; }
    }
    public class BLESmartWatchData
    {
        [BsonId]
        public ObjectId _id { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime RecordTimestamp { get; set; }
        public string mac { get; set; }
        public string person { get; set; }
        public string location { get; set; }
        public string type { get; set; }
        public int battery { get; set; }
        public int heartrate { get; set; }
        public int step { get; set; }
        public int sleep { get; set; }
        public int calories { get; set; }
        public int blood { get; set; }
        public decimal temperature { get; set; }
        //public virtual int sos { get; set; }
        //public virtual int unwear { get; set; }
        public bool sos { get; set; }
        public bool unwear { get; set; }
        public bool falling { get; set; }
        public int nearby { get; set; }
        public bool battery_update { get; set; }
        public bool blood_update { get; set; }
        public bool temperature_update { get; set; }
        public string rssi { get; set; }
    }


    public class RealtimeDashboard
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public string EventType { get; set; }
        public string TagID { get; set; }
        public string personName { get; set; }
        public string NRIC { get; set; }
        public decimal Temp { get; set; }
        public int HR { get; set; }
        public string Location { get; set; }
        public string Tampered { get; set; }
        public string Acknowledge { get; set; }
        public string ActionRemarks { get; set; }
        public string Status { get; set; }
        public string SOS { get; set; }
        public int Battery { get; set; }
        public string Geofencing { get; set; }
        public string stationNo { get; set; }
        public string Blood { get; set; }
        public string Calories { get; set; }
        public string FallDown { get; set; }
    }


    public class BLESmartWatchDataGrid
    {
        public int ErrorNo { get; set; }
        public string ErrorMessage { get; set; }

        public BLESmartWatchDataGrid()
        {
            ErrorNo = 0;
            ErrorMessage = "";
        }
        [BsonId]
        public ObjectId _id { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime RecordTimestamp { get; set; }
        public string mac { get; set; }
        public string person { get; set; }
        public string location { get; set; }
        public string type { get; set; }
        public int battery { get; set; }
        public int heartrate { get; set; }
        public int step { get; set; }
        public int sleep { get; set; }
        public int calories { get; set; }
        public int blood { get; set; }
        public decimal temperature { get; set; }
        //public virtual int sos { get; set; }
        //public virtual int unwear { get; set; }
        public bool sos { get; set; }
        public bool unwear { get; set; }
        public bool falling { get; set; }
        public int nearby { get; set; }
        public bool battery_update { get; set; }
        public bool blood_update { get; set; }
        public bool temperature_update { get; set; }
    }

    public class temperatureChart
    {
        public string EventDate { get; set; }
        public int Baseline { get; set; }
        public decimal Current { get; set; }
    }

    public class realtimeChart
    {
        public decimal temperature { get; set; }
        public int heartrate { get; set; }
    }

    public class heartRateChart
    {
        public string EventDate { get; set; }
        public int Baseline { get; set; }
        public int Current { get; set; }
    }

    public class Location
    {
        public string personID { get; set; }
        public string location { get; set; }
        public string left { get; set; }
        public string top { get; set; }
        public int battery { get; set; }
        public int heartrate { get; set; }
        public decimal temperature { get; set; }
        public bool sos { get; set; }
    }

    public class updateAcknowledge
    {
        public string id { get; set; }
        public string remarks { get; set; }
    }

    public class countRealtimeDashboard
    {
        public int total_heartrate { get; set; }
        public int total_temperature { get; set; }
        public int total_battery { get; set; }
        public int total_tampered { get; set; }
        public int total_geofencing{ get; set; }
    }

    public class countRealtimeAlert
    {
        [BsonId]
        public ObjectId _id { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime RecordTimestamp { get; set; }
        public string alertName { get; set; }
        public int totalAlert { get; set; }
        public int statusAlert { get; set; }
    }

    public class DashboardView
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public string stationNo { get; set; }
        public string EventType { get; set; }
        public decimal Temp { get; set; }
        public int HR { get; set; }
        public string Location { get; set; }
        public string Tampered { get; set; }
        public string Acknowledge { get; set; }
        public string ActionRemarks { get; set; }
        public string Status { get; set; }
        public string SOS { get; set; }
        public int Battery { get; set; }
        public string Geofencing { get; set; }
        public int Blood { get; set; }
        public string FallDown { get; set; }
    }
}