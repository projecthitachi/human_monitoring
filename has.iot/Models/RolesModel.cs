﻿using has.iot.Components;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace has.iot.Models
{
    public class featuresAccess
    {
        public string modul { get; set; }
        public bool c { get; set; }
        public bool r { get; set; }
        public bool u { get; set; }
        public bool d { get; set; }
        public bool e { get; set; }
    }
    public class features
    {
        public bool c { get; set; }
        public bool r { get; set; }
        public bool u { get; set; }
        public bool d { get; set; }
        public bool e { get; set; }
    }
    public class Moduls
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public long id { get; set; }
        public string title { get; set; }
        public string alias { get; set; }
        public string icon { get; set; }
        public string controller { get; set; }
        public string type { get; set; }
        public string function { get; set; }
        public string features { get; set; }
        public long child { get; set; }
        public long order { get; set; }
        public bool active { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime crtOn { get; set; }
        public string crtAt { get; set; }
        public string crtBy { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime uptOn { get; set; }
        public string uptAt { get; set; }
        public string uptBy { get; set; }
    }
    public class Role
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public long id { get; set; }
        public string rlId { get; set; }
        public string rlName { get; set; }
        public string acsMod { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime crtOn { get; set; }
        public string crtAt { get; set; }
        public string crtBy { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime uptOn { get; set; }
        public string uptAt { get; set; }
        public string uptBy { get; set; }
    }
    public class RoleGrid
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public long id { get; set; }
        public string rlId { get; set; }
        public string rlName { get; set; }
        public string acsMod { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime crtOn { get; set; }
        public string crtAt { get; set; }
        public string crtBy { get; set; }

        public string uptOn { get; set; }
        public string uptAt { get; set; }
        public string uptBy { get; set; }
    }

    public class ApiResponses
    {
        public int errorcode { get; set; }
        public string title { get; set; }
        public string message { get; set; }
        public object data { get; set; }
        public ApiResponses()
        {
            errorcode = 0;
            title = "";
            message = "";
            data = new object();
        }

    }

    public class Sequence
    {
        [BsonId]
        public ObjectId _id { get; set; }

        public string name { get; set; }

        public long value { get; set; }

        public long GetNextSequenceValue(string sequenceName, string dbName)
        {
            IMongoDatabase database = Constant.client.GetDatabase(dbName);
            var collection = database.GetCollection<Sequence>("seq");
            var filter = Builders<Sequence>.Filter.Eq(a => a.name, sequenceName);

            if (collection.Find(filter).Limit(1).SingleOrDefault() == null)
            {
                Sequence data = new Sequence();
                data.name = sequenceName;
                data.value = 1;
                collection.InsertOne(data);
                return 1;
            }
            else
            {
                var update = Builders<Sequence>.Update.Inc(a => a.value, 1);
                var sequence = collection.FindOneAndUpdate(filter, update);
                return sequence.value + 1;
            }
        }
    }

    public class RoleTable
    {
        public int ErrorNo { get; set; }
        public string ErrorMessage { get; set; }

        public RoleTable()
        {
            ErrorNo = 0;
            ErrorMessage = "";
        }
        [BsonId]
        public ObjectId _id { get; set; }
        public long id { get; set; }
        public string rlId { get; set; }
        public string rlName { get; set; }
        public string acsMod { get; set; }

        public string crtOn { get; set; }
        public string crtAt { get; set; }
        public string crtBy { get; set; }

        public string uptOn { get; set; }
        public string uptAt { get; set; }
        public string uptBy { get; set; }
    }

    public class GridResponses
    {
        public string Errors { get; set; }
        public long Total { get; set; }
        public object Data { get; set; }
        public GridResponses()
        {
            Errors = null;
            Total = 0;
            Data = new object();
        }

    }
}