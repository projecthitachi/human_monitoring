﻿using has.iot.Models.System;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace has.iot.Models
{
    public class Brands_REC
    {
        [Key]
        public long RecordID { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        //------------------------------
        public string CategoryID { get; set; }
        public string BrandName { get; set; }
        public string Protocol { get; set; }
        public string Image { get; set; }

        public string CategoryName { get; set; }
    }
    public class Brands
    {
        public int ErrorNo { get; set; }
        public string ErrorMessage { get; set; }

        public Brands()
        {
            ErrorNo = 0;
            ErrorMessage = "";
        }

        #region "t70200fieldselect"
        private string t70200fieldselect = @"
            t70200r001 as RecordID,
            t70200r002 as RecordTimestamp,
            t70200r003 as RecordStatus,
            t70200f001 as CategoryID,
            t70200f002 as BrandName,
            t70200f003 as Protocol,
            t70200f004 as Image";
        #endregion

        public DbRawSqlQuery<Brands_REC> GetList(string sBrands)
        {
            DatabaseContext oRemoteDB = new DatabaseContext();
            string sSQL = "SELECT " + t70200fieldselect + " FROM t70200 where t70200f001 = '"+sBrands+"' ORDER BY t70200r001 DESC;";
            var vQuery = oRemoteDB.Database.SqlQuery<Brands_REC>(sSQL);
            return vQuery;
        }
        public DbRawSqlQuery<Brands_REC> GetOne(long RecordID)
        {
            DatabaseContext oRemoteDB = new DatabaseContext();
            string sSQL = "SELECT " + t70200fieldselect + " FROM t70200 WHERE t70200r001 = " + RecordID;
            var vQuery = oRemoteDB.Database.SqlQuery<Brands_REC>(sSQL);
            return vQuery;
        }
        public DbRawSqlQuery<Brands_REC> GetListBrand()
        {
            DatabaseContext oRemoteDB = new DatabaseContext();
            string sSQL = "SELECT " + t70200fieldselect + ",t70100f002 as CategoryName FROM t70200 LEFT JOIN t70100 on t70100f001 = t70200f001 ORDER BY t70200r001 DESC;";
            var vQuery = oRemoteDB.Database.SqlQuery<Brands_REC>(sSQL);
            return vQuery;
        }
        public long Insert(DatabaseContext oRemoteDB, Brands_REC poRecord)
        {
            long bReturn = 0;
            //------------------------------
            poRecord.RecordID = 0;
            poRecord.RecordTimestamp = DateTime.Now;
            poRecord.RecordStatus = 0;

            //GlobalFunction.TrimNull(poRecord);
            //----------
            string sSQL = "" +
                "INSERT INTO t70200 " +
                "      ( " +
                "      t70200r002, t70200r003, " +
                "      t70200f001, t70200f002, t70200f003,t70200f004) " +
                "      VALUES " +
                "      (" +
                "      @pt70200r002, @pt70200r003, " +
                "      @pt70200f001, @pt70200f002, @pt70200f003,@pt70200f003,t70200f004)" +
                ";" +
                "SELECT @pt70200r001 = SCOPE_IDENTITY(); "
                ;

            List<SqlParameter> oParameters = new List<SqlParameter>();
            oParameters.Add(new SqlParameter { ParameterName = "@pt70200r001", SqlDbType = SqlDbType.BigInt, Direction = ParameterDirection.Output });
            oParameters.Add(new SqlParameter("@pt70200r002", poRecord.RecordTimestamp));
            oParameters.Add(new SqlParameter("@pt70200r003", poRecord.RecordStatus));

            oParameters.Add(new SqlParameter("@pt70200f001", poRecord.CategoryID));
            oParameters.Add(new SqlParameter("@pt70200f002", poRecord.BrandName));
            oParameters.Add(new SqlParameter("@pt70200f003", poRecord.Protocol));
            oParameters.Add(new SqlParameter("@pt70200f004", poRecord.Image));
            SqlParameter[] vSqlParameter = oParameters.ToArray();

            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);

            if (nReturn == 1)
            {
                bReturn = Convert.ToInt64(vSqlParameter[0].Value);
            }
            else
            {
                ErrorMessage = "Failed to insert record!";
                bReturn = 0;
                throw new Exception(ErrorMessage);
            }
            //------------------------------
            return bReturn;
        }
        public bool Update(Brands_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            try
            {
                DateTime dRecordTimestamp = poRecord.RecordTimestamp;
                //----------
                //poRecord.RecordID = 0;
                poRecord.RecordTimestamp = DateTime.Now;
                //poRecord.RecordStatus = 0;
                //poRecord.RecordFlag = 0;
                //----------
                //GlobalFunction.TrimNull(poRecord);
                string sSQL = "" +
                    "UPDATE t70200 " +
                    "   SET " +
                    "      t70200f002 = @pt70200f002, " +
                    "      t70200f003 = @pt70200f003, " +
                    "      t70200f004 = @pt70200f004 " +
                    "   WHERE (t70200f001 = @pt70200f001) " +
                    ";"
                    ;

                List<SqlParameter> oParameters = new List<SqlParameter>();
                //oParameters.Add(new SqlParameter("@pRecordTimestamp", dRecordTimestamp));
                oParameters.Add(new SqlParameter("@pt70200f001", poRecord.CategoryID));
                oParameters.Add(new SqlParameter("@pt70200f002", poRecord.BrandName));
                oParameters.Add(new SqlParameter("@pt70200f003", poRecord.Protocol));
                oParameters.Add(new SqlParameter("@pt70200f004", poRecord.Image));
                SqlParameter[] vSqlParameter = oParameters.ToArray();

                DatabaseContext oRemoteDB = new DatabaseContext();
                var oTransaction = oRemoteDB.Database.BeginTransaction();
                int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
                if (nReturn == 1)
                {
                    oTransaction.Commit();
                }
                else
                {
                    oTransaction.Rollback();
                    ErrorMessage = "Failed to update record!" + "<br>" + "Record has been updated or deleted by another User.";
                    bReturn = false;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                bReturn = false;
            }
            //------------------------------
            return bReturn;
        }
        public bool Delete(Brands_REC poRecord)
        {
            bool bReturn = true;
            //------------------------------
            try
            {
                //poRecord.RecordID = 0;
                poRecord.RecordTimestamp = DateTime.Now;
                //poRecord.RecordStatus = 0;
                //poRecord.RecordFlag = 0;
                //----------
                string sSQL = "" +
                    "DELETE t70200 " +
                    "   WHERE (t70200r001 = @pt70200r001) " +
                    ";"
                    ;

                List<SqlParameter> oParameters = new List<SqlParameter>();
                oParameters.Add(new SqlParameter("@pt70200r001", poRecord.RecordID));
                SqlParameter[] vSqlParameter = oParameters.ToArray();

                DatabaseContext oRemoteDB = new DatabaseContext();
                var oTransaction = oRemoteDB.Database.BeginTransaction();
                int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
                if (nReturn == 1)
                {
                    oTransaction.Commit();
                }
                else
                {
                    oTransaction.Rollback();
                    ErrorMessage = "Failed to delete record!" + "<br>" + "Record has been deleted by another Logging.";

                    bReturn = false;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                bReturn = false;
            }
            //------------------------------
            return bReturn;
        }

    }
}