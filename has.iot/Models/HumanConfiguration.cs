﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace has.iot.Models
{
    public class HumanConfiguration
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public DateTime recordTimestamp { get; set; }
        public string activatedHR { get; set; }
        public int whenBelowHR { get; set; }
        public int whenAboveHR { get; set; }
        public int conPeriodHR { get; set; }
        public int resetAlertHR { get; set; }
        public string activatedTM { get; set; }
        public decimal whenBelowTM { get; set; }
        public decimal whenAboveTM { get; set; }
        public int conPeriodTM { get; set; }
        public int resetAlertTM { get; set; }
        public string activatedSOS { get; set; }
        public int resetAlertSOS { get; set; }
        public string activatedTEM { get; set; }
        public int resetAlertTEM { get; set; }
        public string activatedBattery { get; set; }
        public int whenBelowBattery { get; set; }
        public int conPeriodBattery { get; set; }
        public int resetAlertBattery { get; set; }
        public string activatedGeofencing { get; set; }
        public int conPeriodGeofencing { get; set; }
        public int resetAlertGeofencing { get; set; }
        public string activatedFallDown { get; set; }
        public int conPeriodFallDown { get; set; }
        public int resetAlertFallDown { get; set; }
        public string activatedBP { get; set; }
        public int whenBelowBP { get; set; }
        public int whenAboveBP { get; set; }
        public int whenBelowBP2 { get; set; }
        public int whenAboveBP2 { get; set; }
        public int conPeriodBP { get; set; }
        public int resetAlertBP { get; set; }

    }

    public class HumanConfigurationTemp
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public DateTime recordTimestamp { get; set; }
        public string status { get; set; }
        public string tipe { get; set; }
        public decimal below { get; set; }
        public decimal above { get; set; }
        public int periode { get; set; }
        public int reset { get; set; }
        public string currentStatus { get; set; }
    }
}