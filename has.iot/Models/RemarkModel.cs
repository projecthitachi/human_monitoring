﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace has.iot.Models
{
    public class RemarkModelView
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public string Remarks { get; set; }
        public string status { get; set; }
    }
    public class RemarkModel
    {
        public int ErrorNo { get; set; }
        public string ErrorMessage { get; set; }

        public RemarkModel()
        {
            ErrorNo = 0;
            ErrorMessage = "";
        }
        [BsonId]
        public ObjectId _id { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public string Remarks { get; set; }
        public string status { get; set; }
    }
    public class RemarkModelDatabase
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public string Remarks { get; set; }
        public string status { get; set; }
    }
}