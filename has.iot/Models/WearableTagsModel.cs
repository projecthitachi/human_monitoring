﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace has.iot.Models
{
    public class WearableTagModel_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public string tagID { get; set; }
        public string active { get; set; }
        public string personID { get; set; }
        public string stationNo { get; set; }
    }
    public class WearableTagModel
    {
        public int ErrorNo { get; set; }
        public string ErrorMessage { get; set; }

        public WearableTagModel()
        {
            ErrorNo = 0;
            ErrorMessage = "";
        }
        [BsonId]
        public ObjectId _id { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public string tagID { get; set; }
        public string active { get; set; }
        public string personID { get; set; }
        public string stationNo { get; set; }
    }
    public class WearableTagModelDatabase
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public string tagID { get; set; }
        public string active { get; set; }
        public string personID { get; set; }
        public string stationNo { get; set; }
    }
}