﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace has.iot.Models
{
    public class Users
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public long id { get; set; }
        public string uuid { get; set; }
        public string usrId { get; set; }
        public string usrName { get; set; }
        public string usrPass { get; set; }
        public string fstName { get; set; }
        public string lstName { get; set; }
        public string rlId { get; set; }
        public string rlName { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime crtOn { get; set; }
        public string crtAt { get; set; }
        public string crtBy { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime uptOn { get; set; }
        public string uptAt { get; set; }
        public string uptBy { get; set; }
        public string sess { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime sessLt { get; set; }
    }
    public class UsersLockout
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public string usrId { get; set; }
        public int attempts { get; set; }
        public int totAttem { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime lockedTm { get; set; }
    }
    public class UsersGrid
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public long id { get; set; }
        public string usrId { get; set; }
        public string usrName { get; set; }
        public string usrPass { get; set; }
        public string fstName { get; set; }
        public string lstName { get; set; }
        public string rlId { get; set; }
        public string rlName { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime crtOn { get; set; }
        public string crtAt { get; set; }
        public string crtBy { get; set; }

        public string uptOn { get; set; }
        public string uptAt { get; set; }
        public string uptBy { get; set; }
    }
}