﻿using Kendo.Mvc;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Configuration;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;

namespace has.iot.Components
{
    public class Constant
    {
        public const string DateFormat = "MM-dd-yyyy";
        public const string DateFormatGrid = "{0:MM-dd-yyyy}";
        public static string Admin = "Administrator";
        public static MqttClient MqttClient = null;
        public static MongoClient client = new MongoClient(WebConfigurationManager.AppSettings["mongodb_server"].ToString());
        public static string ExportDirectory = "/Components/Export/AirCond/";
        public static FilterDescriptor KendoChangeComposite(IEnumerable<IFilterDescriptor> filters)
        {
            FilterDescriptor filt = new FilterDescriptor();
            foreach (var filter in filters)
            {
                if (filter is CompositeFilterDescriptor)
                    KendoChangeComposite(((CompositeFilterDescriptor)filter).FilterDescriptors);
                else
                    filt = ((FilterDescriptor)filter);
            }
            return filt;
        }
        public static string MD5Hash(string text)
        {
            MD5 md5 = new MD5CryptoServiceProvider();

            //compute hash from the bytes of text  
            md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text));

            //get hash result after compute it  
            byte[] result = md5.Hash;

            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                //change it into 2 hexadecimal digits  
                //for each byte  
                strBuilder.Append(result[i].ToString("x2"));
            }

            return strBuilder.ToString();
        }
        public static string keypass = System.Web.Configuration.WebConfigurationManager.AppSettings["keypass"];
        public static string keycert = System.Web.Configuration.WebConfigurationManager.AppSettings["keycert"];
        public static string bypass = System.Web.Configuration.WebConfigurationManager.AppSettings["bypass"];
        public static string hardtimout = System.Web.Configuration.WebConfigurationManager.AppSettings["hardtimout"];
        public static string protocol = System.Web.Configuration.WebConfigurationManager.AppSettings["protocol"];
        public static string version = System.Web.Configuration.WebConfigurationManager.AppSettings["version"];
        public static string refreshIntrv = System.Web.Configuration.WebConfigurationManager.AppSettings["interval"];
        private static string mongoDbConnection = System.Web.Configuration.WebConfigurationManager.AppSettings["mongodb_server"];

        public static string dbName = System.Web.Configuration.WebConfigurationManager.AppSettings["mongodb_name"].ToString();
        public static string formulaInj = "=+-@";

        public static TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById("Singapore Standard Time");

        //--------- GROUP PHONE NO
        public const string MSG_GROUP_PHONE_NO_VALIDATE_SUCCESS = "Success. Group Phone is registered.";
        public const string MSG_GROUP_PHONE_NO_VALIDATE_FAILED = "Unregistered Group Phone No.";
        public const string MSG_ACTIVATE_GROUP_SUCCESS = "Group Phone Activated.";

        //--------- OTP
        public const string MSG_OTP_VALIDATE_SUCCESS = "Success. OTP is valid.";
        public const string MSG_OTP_VALIDATE_FAILED = "Invalid OTP.";
        public const string MSG_OTP_EXPIRED = "OTP is expired.";
        public const int OTP_EXPIRED_PERIOD = 120;//in minute

        //--------- BLE GATEWAY
        public const string MSG_BLE_GATEWAY_REGISTRATION_SUCCESS = "Gateway Registered.";
        public const string MSG_BLE_GATEWAY_REGISTRATION_FAILED = "Unregistered Gateway.";

        //--------- BLE TAG
        public const string MSG_BLE_TAG_REGISTRATION_SUCCESS = "Tag Registered.";
        public const string MSG_BLE_TAG_REGISTRATION_FAILED = "Unregistered Tag.";

        //--------- USER PARTICULAR
        public const string MSG_USER_PARTICULAR_SUBMIT_SUCCESS = "User Particular registration success.";

        //--------- CHANGE MASTER MOBILE NO
        public const string MSG_MASTER_MOBILE_NO_NEW_NUMBER_ALREADY_REGISTERED = "<newMstMblNo> New Master Mobile No is exist.";
        public const string MSG_MASTER_MOBILE_NO_NOT_FOUND = "Master Mobile No not registered.";
        public const string MSG_MASTER_MOBILE_NO_CHANGE_SUCCESS = "Master Mobile No changed successfull.";

        //--------- PSHN Action
        public const string MSG_PSHN_NOT_EXIST = "PSHN Id <pshnId> is not exist";
        //--------- BLE TAG
        public const string MSG_BLE_TAG_REGISTRATION_INUSE = "Wristband ID <tagId> is used by PSHN ID <pshnId>.";
        public const string MSG_BLE_TAG_VALIDATE_SUCCESS = "Validate success.";
        public const string MSG_BLE_TAG_UNREGISTERED = "Unregistered Wristband ID <tagId>.";
        public const string MSG_BLE_TAG_NOT_ACTIVE = "Wristband ID is not active";
        public const string MSG_REGISTRATION_BLE_TAG_UNREGISTERED = "Unregistered Wristband ID <tagId>.";
        //--------- ERROR CODE
        public const int ERRCODE_CANT_REACH_DATABASE = 7000;
        public const int ERRCODE_INTERNAL_SERVER_ERROR = 8000;
        public const int ERRCODE_VALIDATION = 8001;
        public const int ERRCODE_ONEMAP = 8003;
        public const int ERRCODE_FCM = 8004;

        public const int MOBILEAPP_FCM_VERSION = 124;


        public static string GetUserLogin()
        {
            string usrID = System.Web.HttpContext.Current.Session["UserName"] as string;

            return usrID;
        }

        static string GetClientID()
        {
            Random random = new Random();
            const string chars = "ABCDEFGHJKMNPQRSTUVWXYZ23456789";
            string MyRandom = new string(Enumerable.Repeat(chars, 14)
              .Select(s => s[random.Next(s.Length)]).ToArray());
            return "mqtt_" + MyRandom + "_1";
        }

        public static string Hash(string input)
        {
            using (SHA1Managed sha1 = new SHA1Managed())
            {
                var hash = sha1.ComputeHash(Encoding.UTF8.GetBytes(input));
                var sb = new StringBuilder(hash.Length * 2);

                foreach (byte b in hash)
                {
                    // can be "x2" if you want lowercase
                    sb.Append(b.ToString("X2"));
                }

                return sb.ToString();
            }
        }

        public static string GetIPAddress()
        {
            return HttpContext.Current.Request.UserHostAddress.ToString();
        }
    }
}