﻿using has.iot.Components;
using has.iot.Models;
using MongoDB.Driver;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;
using static has.iot.Components.GlobalFunction;

namespace has.stayathome.monitoring.Components
{
    public class FilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            filterContext.RequestContext.HttpContext.Response.AddHeader("Access-Control-Allow-Origin", "*");
            base.OnActionExecuting(filterContext);
        }
    }

    public class PrivatePageAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string UserID = System.Web.HttpContext.Current.Session["UserID"] as string;
            string RoleId = System.Web.HttpContext.Current.Session["RoleID"] as string;
            string UserAgent = System.Web.HttpContext.Current.Session["UserAgent"] as string;
            string IPAddress = System.Web.HttpContext.Current.Session["IPAddress"] as string;
            if (string.IsNullOrEmpty(RoleId))
            {

                //GlobalFunctions.writelog("no session or session time out ");
                filterContext.Result = new HttpStatusCodeResult(403, "session time out, redirecting to login page in 10 seccond");
                filterContext.RequestContext.HttpContext.Response.Redirect(Constant.protocol + "://" + System.Web.HttpContext.Current.Request.Url.Host.ToString() + "/shnapp/Systems/Login", true);
            }
            else
            {

                Uri URL = new Uri(System.Web.HttpContext.Current.Request.Url.ToString());
                string URIsegment = (URL.Segments.Length > 2) ? URL.Segments[URL.Segments.Length - 1].Replace("/", "") : "dashboard";

                if (URL.Segments.Length == 5)
                {
                    URIsegment = URL.Segments[URL.Segments.Length - 2].Replace("/", "");
                }
                var DbName = Constant.dbName;
                IMongoDatabase database = Constant.client.GetDatabase(DbName);
                IMongoCollection<Role> CollRole = database.GetCollection<Role>("Roles");
                IMongoCollection<Moduls> Collections = database.GetCollection<Moduls>("Moduls");
                IMongoCollection<Users> CollUsr = database.GetCollection<Users>("Users");

                var usrBld = Builders<Users>.Filter;
                var usrFlt = usrBld.Eq<string>("usrId", UserID);
                var usrRes = CollUsr.Find(usrFlt).FirstOrDefault();
                if (usrRes == null)
                {
                    filterContext.Result = new HttpStatusCodeResult(403);
                }
                else
                {
                    if (Constant.bypass == "false")
                    {
                        string cookies = filterContext.RequestContext.HttpContext.Request.Cookies.Get("Shnapp_SessionID").Value;
                        string UserAgents = filterContext.RequestContext.HttpContext.Request.UserAgent;
                        string ip = Constant.GetIPAddress();
                        if (UserAgent != UserAgents || usrRes.sess != cookies)
                        {
                            filterContext.Result = new HttpStatusCodeResult(401, "someone logged in with your account");
                        }
                        else if ((usrRes.sessLt - DateTime.Now).TotalMinutes <= 0)
                        {
                            filterContext.Result = new HttpStatusCodeResult(403, "session time out, redirecting to login page in 10 seccond");
                            filterContext.RequestContext.HttpContext.Response.Redirect(Constant.protocol + "://" + System.Web.HttpContext.Current.Request.Url.Host.ToString() + "/shnapp/Systems/Login", true);
                        }

                    }
                }

                var evntBld = Builders<Role>.Filter;
                var evntFlt = evntBld.Eq<string>("rlId", RoleId);
                var evntRes = CollRole.Find(evntFlt).FirstOrDefault();
                if (evntRes == null)
                {
                    filterContext.Result = new HttpStatusCodeResult(403);
                }

                var vResult = Collections.Find(f => f.active == true).SortBy(s => s.order).ToList();
                List<featuresAccess> result = JsonConvert.DeserializeObject<List<featuresAccess>>(evntRes.acsMod);
                foreach (featuresAccess obj in result)
                {
                    if (URIsegment.ToLower() == obj.modul.ToLower() || URL.Segments.Length == 4 && URIsegment.ToLower().StartsWith(obj.modul.ToLower()))
                    {
                        string fitureAccess = URIsegment.Substring(obj.modul.Length, URIsegment.Length - obj.modul.Length);
                        if (fitureAccess == "" && !obj.e || fitureAccess == "Add" && !obj.c || fitureAccess == "Create" && !obj.c || fitureAccess == "Detail" && !obj.u || fitureAccess == "Update" && !obj.u || fitureAccess == "Read" && !obj.r || fitureAccess == "Delete" && !obj.d || fitureAccess == "Ack" && !obj.u)
                        {
                            filterContext.Result = new HttpStatusCodeResult(403);
                            //throw new HttpException(403, "Forbiden");
                        }

                    }
                    else if (URL.Segments.Length == 5 && URIsegment.ToLower().StartsWith(obj.modul.ToLower()))
                    {
                        string fitureAccess = URIsegment.Substring(obj.modul.Length, URIsegment.Length - obj.modul.Length);
                        if (fitureAccess == "" && !obj.e || fitureAccess == "Add" && !obj.c || fitureAccess == "Create" && !obj.c || fitureAccess == "Detail" && !obj.u || fitureAccess == "Update" && !obj.u || fitureAccess == "Read" && !obj.r || fitureAccess == "Delete" && !obj.d || fitureAccess == "Ack" && !obj.u)
                        {
                            filterContext.Result = new HttpStatusCodeResult(403);
                            //throw new HttpException(403, "Forbiden");
                        }
                    }
                }
            }
        }
    }
}