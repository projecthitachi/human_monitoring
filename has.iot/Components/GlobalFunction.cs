﻿using has.iot.Models;
using has.iot.Models.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Microsoft.AspNet.SignalR;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;
using has.iot.Components;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using MongoDB.Driver;
using Kendo.Mvc;
using System.Web.Routing;
using MongoDB.Bson;
using Kendo.Mvc.UI;

namespace has.iot.Components
{
    public class IsLoggedInAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpSessionStateBase session = filterContext.HttpContext.Session;
            string controllerName = ((ControllerBase)filterContext.Controller).ControllerContext.RouteData.Values["controller"].ToString();
            string actionName = ((ControllerBase)filterContext.Controller).ControllerContext.RouteData.Values["action"].ToString();

            if ((session != null && session["UserID"] == null) && (controllerName != "Systems" && actionName != "Login"))
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                {
                    controller = "Systems",
                    action = "Login",
                    area = ""
                }));
            }
            base.OnActionExecuting(filterContext);
        }
    }

    public class GlobalFunction
    {

        public static void TrimNull<T>(T poRecord)
        {
            var stringProperties = poRecord.GetType().GetProperties().Where(p => p.PropertyType == typeof(string));
            foreach (var stringProperty in stringProperties)
            {
                string currentValue = (string)stringProperty.GetValue(poRecord, null);
                if (currentValue == null)
                {
                    currentValue = "";
                }
                stringProperty.SetValue(poRecord, currentValue.Trim(), null);
            }
        }
        
        public static bool IsValidJson(string strInput)
        {
            strInput = strInput.Trim();
            if ((strInput.StartsWith("{") && strInput.EndsWith("}")) || //For object
                (strInput.StartsWith("[") && strInput.EndsWith("]"))) //For array
            {
                try
                {
                    var obj = JToken.Parse(strInput);
                    return true;
                }
                catch (JsonReaderException jex)
                {
                    return false;
                }
                catch (Exception ex) //some other exception
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public static string mqttServer = null;
        public static MqttClient MqttClient = null;
        public static void MqttListener()
        {
            mqttServer = WebConfigurationManager.AppSettings["server_mqtt"].ToString();
            MqttClient = new MqttClient(mqttServer);
            //MqttClient.MqttMsgPublishReceived += client_MqttMsgPublishReceived;
            string clientId = Guid.NewGuid().ToString();
            MqttClient.Connect(clientId);
        }    
        public static long GeneralTablesGetIDByType(string text, string type)
        {
            Models.GeneralTablesQuery oQuery = new Models.GeneralTablesQuery();
            Models.GeneralTables oData = oQuery.GeneralTablesGetIDByType(text, type).SingleOrDefault();
            if (oData != null)
                return oData.RecordID;
            else
                return 0;
        }
        public static FilterDescriptor KendoChangeComposite(IEnumerable<IFilterDescriptor> filters)
        {
            FilterDescriptor filt = new FilterDescriptor();
            foreach (var filter in filters)
            {
                if (filter is CompositeFilterDescriptor)
                    KendoChangeComposite(((CompositeFilterDescriptor)filter).FilterDescriptors);
                else
                    filt = ((FilterDescriptor)filter);
            }
            return filt;
        }

        public static UsersData GetLoginData(string userdata)
        {
            IMongoDatabase database = Constant.client.GetDatabase(Constant.dbName);
            var Collectusers = database.GetCollection<UsersView_REC>("Users");
            var collection = database.GetCollection<Persons>("Person");

            JObject obj = JObject.Parse(userdata);
            ObjectId _id = ObjectId.Parse(obj["_id"].ToString());
            string uuid = obj["uuid"].ToString();

            UsersView_REC res = Collectusers.Find(x => x._id == _id).SingleOrDefault();
            Persons resPerson = collection.Find(x => x.personID == uuid).SingleOrDefault();

            string resJson = JsonConvert.SerializeObject(res);
            obj = JObject.Parse(resJson);
            obj.Property("_id").Remove();

            string objString = JsonConvert.SerializeObject(obj);


            UsersData voData = JsonConvert.DeserializeObject<UsersData>(objString);
            voData._id = _id;
            voData.person = resPerson;


            return voData;
        }

        public static string GetIPAddress()
        {
            return HttpContext.Current.Request.UserHostAddress.ToString();
        }

        public static void AuditLogDetail(long logId, string crtBy, string prevVal, string crtVal, string fld)
        {
            IMongoDatabase database = Constant.client.GetDatabase(Constant.dbName);
            var collection = database.GetCollection<AuditLogDetail>("AuditLogDetail");
            AuditLogDetail Adl = new AuditLogDetail();
            Adl.logId = logId;
            Adl.crtAt = GlobalFunction.GetIPAddress();
            Adl.crtOn = DateTime.Now;
            Adl.crtBy = crtBy;
            Adl.prevVal = prevVal;
            Adl.crtVal = crtVal;
            Adl.uptOn = DateTime.Now;
            Adl.fld = fld;
            collection.InsertOne(Adl);
        }

        public static FilterDefinition<T> gridFilter<T>([DataSourceRequest] DataSourceRequest poRequest)
        {
            var Bld = Builders<T>.Filter;
            var FltAnd = Bld.And();
            var FltOr = Bld.Or();
            bool and = false;
            bool or = false;

            if (poRequest.Filters.Count > 0)
            {
                if (poRequest.Filters[0].GetType().ToString() == "Kendo.Mvc.CompositeFilterDescriptor")
                {

                    foreach (Kendo.Mvc.CompositeFilterDescriptor voFIlter in poRequest.Filters)
                    {
                        if (voFIlter.LogicalOperator.ToString() == "And")
                        {
                            #region And
                            for (int i = 0; i < voFIlter.FilterDescriptors.Count; i++)
                            {
                                if (voFIlter.FilterDescriptors[i].GetType().ToString() == "Kendo.Mvc.CompositeFilterDescriptor")
                                {
                                    Kendo.Mvc.CompositeFilterDescriptor poCompositeFIlter = voFIlter.FilterDescriptors[i] as Kendo.Mvc.CompositeFilterDescriptor;
                                    if (poCompositeFIlter.LogicalOperator.ToString() == "And")
                                    {
                                        #region "And"
                                        for (int a = 0; a < poCompositeFIlter.FilterDescriptors.Count; a++)
                                        {
                                            if (poCompositeFIlter.FilterDescriptors[a].GetType().ToString() == "Kendo.Mvc.CompositeFilterDescriptor")
                                            {
                                                Kendo.Mvc.CompositeFilterDescriptor poFIlter = poCompositeFIlter.FilterDescriptors[a] as Kendo.Mvc.CompositeFilterDescriptor;
                                                if (poFIlter.LogicalOperator.ToString() == "And")
                                                {
                                                    #region "And"
                                                    foreach (Kendo.Mvc.FilterDescriptor doFilter in poFIlter.FilterDescriptors)
                                                    {
                                                        string field = doFilter.Member;
                                                        if (field == "ts" || field == "tagTs" || field == "crtOn" || field == "picRcv")
                                                        {
                                                            string Opr = doFilter.Operator.ToString();
                                                            string value = doFilter.Value.ToString();

                                                            if (Opr == "IsGreaterThanOrEqualTo")
                                                            {
                                                                FltAnd &= Bld.Gte<DateTime>(field, DateTime.Parse(value));
                                                            }
                                                            else if (Opr == "IsLessThanOrEqualTo")
                                                            {
                                                                FltAnd &= Bld.Lte<DateTime>(field, DateTime.Parse(value));
                                                            }
                                                        }
                                                        else if (field == "dateStart" || field == "dateEnd" || field == "dob")
                                                        {
                                                            string Opr = doFilter.Operator.ToString();
                                                            string value = doFilter.Value.ToString();
                                                            var voValue = DateTime.Parse(value).Date;
                                                            if (Opr == "IsGreaterThanOrEqualTo")
                                                            {
                                                                FltAnd &= Bld.Gte<DateTime>(field, voValue);
                                                            }
                                                            else if (Opr == "IsLessThanOrEqualTo")
                                                            {
                                                                FltAnd &= Bld.Lte<DateTime>(field, voValue.AddHours(23));
                                                            }
                                                        }
                                                        else
                                                        {
                                                            string Opr = doFilter.Operator.ToString();
                                                            Opr = (Opr == "IsEqualTo") ? "$eq" : Opr;
                                                            string value = doFilter.Value.ToString();
                                                            if (field == "tmprd" || field == "procs" || field == "gtwMtn")
                                                            {
                                                                FltAnd &= Bld.Eq<bool>(field, Convert.ToBoolean(value.ToLower()));
                                                            }
                                                            else if (field == "rssi")
                                                            {
                                                                FltAnd &= Bld.Eq<double>(field, Convert.ToDouble(value.ToLower()));
                                                            }
                                                            else if (field == "btrLvl" || field == "btry")
                                                            {
                                                                if (value.ToLower() == "low" || value.ToLower() == "normal")
                                                                {
                                                                    FltAnd &= Bld.Eq<string>(field, value.ToLower());
                                                                }
                                                                else
                                                                {
                                                                    FltAnd &= Bld.Eq<int>(field, Convert.ToInt32(value.ToLower()));
                                                                }
                                                            }
                                                            else if (field == "picNotRcvMin")
                                                            {
                                                                if (value.ToLower() == "exist")
                                                                {
                                                                    FltAnd &= Bld.Not(Bld.Eq<string>("pics", null));
                                                                }
                                                                else if (value.ToLower() == "not exist")
                                                                {
                                                                    FltAnd &= Bld.Eq<byte[]>("pic", null);
                                                                }
                                                            }
                                                            else if (field == "mstMblNo")
                                                            {
                                                                string val = value.Substring(2, value.Length - 2);
                                                                FltAnd &= Bld.Eq<string>(field, val) | Bld.Eq<string>(field, value);
                                                            }
                                                            else if (field == "mblNo")
                                                            {
                                                                string val = value.Substring(2, value.Length - 2);
                                                                FltAnd &= Bld.Eq<string>(field, val) | Bld.Eq<string>(field, value);
                                                            }
                                                            else
                                                            {
                                                                FltAnd &= Bld.Eq<string>(field, value);
                                                            }
                                                        }
                                                    }
                                                    #endregion
                                                    and = true;
                                                }
                                                else
                                                {
                                                    #region "Or"
                                                    foreach (Kendo.Mvc.FilterDescriptor doFilter in poFIlter.FilterDescriptors)
                                                    {
                                                        string field = doFilter.Member;
                                                        if (field == "ts" || field == "tagTs" || field == "crtOn" || field == "picRcv")
                                                        {
                                                            string Opr = doFilter.Operator.ToString();
                                                            string value = doFilter.Value.ToString();

                                                            if (Opr == "IsGreaterThanOrEqualTo")
                                                            {
                                                                FltAnd &= Bld.Gte<DateTime>(field, DateTime.Parse(value));
                                                            }
                                                            else if (Opr == "IsLessThanOrEqualTo")
                                                            {
                                                                FltAnd &= Bld.Lte<DateTime>(field, DateTime.Parse(value));
                                                            }
                                                        }
                                                        else if (field == "dateStart" || field == "dateEnd" || field == "dob")
                                                        {
                                                            string Opr = doFilter.Operator.ToString();
                                                            string value = doFilter.Value.ToString();
                                                            var voValue = DateTime.Parse(value).Date;
                                                            if (Opr == "IsGreaterThanOrEqualTo")
                                                            {
                                                                FltAnd &= Bld.Gte<DateTime>(field, voValue);
                                                            }
                                                            else if (Opr == "IsLessThanOrEqualTo")
                                                            {
                                                                FltAnd &= Bld.Lte<DateTime>(field, voValue.AddHours(23));
                                                            }
                                                        }
                                                        else
                                                        {
                                                            string Opr = doFilter.Operator.ToString();
                                                            Opr = (Opr == "IsEqualTo") ? "$eq" : Opr;
                                                            string value = doFilter.Value.ToString();
                                                            if (field == "tmprd" || field == "procs" || field == "gtwMtn")
                                                            {
                                                                FltOr |= Bld.Eq<bool>(field, Convert.ToBoolean(value.ToLower()));
                                                            }
                                                            else if (field == "rssi")
                                                            {
                                                                FltOr |= Bld.Eq<double>(field, Convert.ToDouble(value.ToLower()));
                                                            }
                                                            else if (field == "btrLvl" || field == "btry")
                                                            {
                                                                if (value.ToLower() == "low" || value.ToLower() == "normal")
                                                                {
                                                                    FltOr |= Bld.Eq<string>(field, value.ToLower());
                                                                }
                                                                else
                                                                {
                                                                    FltOr |= Bld.Eq<int>(field, Convert.ToInt32(value.ToLower()));
                                                                }
                                                            }
                                                            else if (field == "picNotRcvMin")
                                                            {
                                                                if (value.ToLower() == "exist")
                                                                {
                                                                    FltOr |= Bld.Not(Bld.Eq<string>("pics", null));
                                                                }
                                                                else if (value.ToLower() == "not exist")
                                                                {
                                                                    FltOr |= Bld.Eq<byte[]>("pic", null);
                                                                }
                                                            }
                                                            else
                                                            {
                                                                FltOr |= Bld.Eq<string>(field, value);
                                                            }
                                                        }
                                                    }
                                                    #endregion
                                                    or = true;
                                                }
                                            }
                                            else
                                            {
                                                Kendo.Mvc.FilterDescriptor poFIlter = poCompositeFIlter.FilterDescriptors[a] as Kendo.Mvc.FilterDescriptor;
                                                string field = poFIlter.Member;
                                                if (field == "ts" || field == "tagTs" || field == "crtOn" || field == "picRcv")
                                                {
                                                    string Opr = poFIlter.Operator.ToString();
                                                    string value = poFIlter.Value.ToString();

                                                    if (Opr == "IsGreaterThanOrEqualTo")
                                                    {
                                                        FltAnd &= Bld.Gte<DateTime>(field, DateTime.Parse(value));
                                                    }
                                                    else if (Opr == "IsLessThanOrEqualTo")
                                                    {
                                                        FltAnd &= Bld.Lte<DateTime>(field, DateTime.Parse(value));
                                                    }
                                                }
                                                else if (field == "dateStart" || field == "dateEnd" || field == "dob")
                                                {
                                                    string Opr = poFIlter.Operator.ToString();
                                                    string value = poFIlter.Value.ToString();
                                                    var voValue = DateTime.Parse(value).Date;
                                                    if (Opr == "IsGreaterThanOrEqualTo")
                                                    {
                                                        FltAnd &= Bld.Gte<DateTime>(field, voValue);
                                                    }
                                                    else if (Opr == "IsLessThanOrEqualTo")
                                                    {
                                                        FltAnd &= Bld.Lte<DateTime>(field, voValue.AddHours(23));
                                                    }
                                                }
                                                else
                                                {
                                                    string Opr = poFIlter.Operator.ToString();
                                                    Opr = (Opr == "IsEqualTo") ? "$eq" : Opr;
                                                    string value = poFIlter.Value.ToString();
                                                    if (field == "tmprd" || field == "procs" || field == "gtwMtn")
                                                    {
                                                        FltAnd &= Bld.Eq<bool>(field, Convert.ToBoolean(value.ToLower()));
                                                    }
                                                    else if (field == "rssi")
                                                    {
                                                        FltAnd &= Bld.Eq<double>(field, Convert.ToDouble(value.ToLower()));
                                                    }
                                                    else if (field == "btrLvl" || field == "btry")
                                                    {
                                                        if (value.ToLower() == "low" || value.ToLower() == "normal")
                                                        {
                                                            FltAnd &= Bld.Eq<string>(field, value.ToLower());
                                                        }
                                                        else
                                                        {
                                                            FltAnd &= Bld.Eq<int>(field, Convert.ToInt32(value.ToLower()));
                                                        }
                                                    }
                                                    else if (field == "picNotRcvMin")
                                                    {
                                                        if (value.ToLower() == "exist")
                                                        {
                                                            FltAnd &= Bld.Not(Bld.Eq<string>("pics", null));
                                                        }
                                                        else if (value.ToLower() == "not exist")
                                                        {
                                                            FltAnd &= Bld.Eq<byte[]>("pic", null);
                                                        }
                                                    }
                                                    else if (field == "mstMblNo")
                                                    {
                                                        string val = value.Substring(2, value.Length - 2);
                                                        FltAnd &= Bld.Eq<string>(field, val) | Bld.Eq<string>(field, value);
                                                    }
                                                    else if (field == "mblNo")
                                                    {
                                                        string val = value.Substring(2, value.Length - 2);
                                                        FltAnd &= Bld.Eq<string>(field, val) | Bld.Eq<string>(field, value);
                                                    }
                                                    else
                                                    {
                                                        FltAnd &= Bld.Eq<string>(field, value);
                                                    }
                                                }
                                            }
                                        }
                                        #endregion
                                        and = true;
                                    }
                                    else
                                    {
                                        #region "Or"
                                        for (int a = 0; a < poCompositeFIlter.FilterDescriptors.Count; a++)
                                        {
                                            if (poCompositeFIlter.FilterDescriptors[a].GetType().ToString() == "Kendo.Mvc.CompositeFilterDescriptor")
                                            {
                                                Kendo.Mvc.CompositeFilterDescriptor poFIlter = poCompositeFIlter.FilterDescriptors[a] as Kendo.Mvc.CompositeFilterDescriptor;
                                                if (poFIlter.LogicalOperator.ToString() == "And")
                                                {
                                                    #region And
                                                    foreach (Kendo.Mvc.FilterDescriptor doFilter in poFIlter.FilterDescriptors)
                                                    {
                                                        string field = doFilter.Member;
                                                        if (field == "ts" || field == "tagTs" || field == "crtOn" || field == "picRcv")
                                                        {
                                                            string Opr = doFilter.Operator.ToString();
                                                            string value = doFilter.Value.ToString();

                                                            if (Opr == "IsGreaterThanOrEqualTo")
                                                            {
                                                                FltAnd &= Bld.Gte<DateTime>(field, DateTime.Parse(value));
                                                            }
                                                            else if (Opr == "IsLessThanOrEqualTo")
                                                            {
                                                                FltAnd &= Bld.Lte<DateTime>(field, DateTime.Parse(value));
                                                            }
                                                        }
                                                        else if (field == "dateStart" || field == "dateEnd" || field == "dob")
                                                        {
                                                            string Opr = doFilter.Operator.ToString();
                                                            string value = doFilter.Value.ToString();
                                                            var voValue = DateTime.Parse(value).Date;
                                                            if (Opr == "IsGreaterThanOrEqualTo")
                                                            {
                                                                FltAnd &= Bld.Gte<DateTime>(field, voValue);
                                                            }
                                                            else if (Opr == "IsLessThanOrEqualTo")
                                                            {
                                                                FltAnd &= Bld.Lte<DateTime>(field, voValue.AddHours(23));
                                                            }
                                                        }
                                                        else
                                                        {
                                                            string Opr = doFilter.Operator.ToString();
                                                            Opr = (Opr == "IsEqualTo") ? "$eq" : Opr;
                                                            string value = doFilter.Value.ToString();
                                                            if (field == "tmprd" || field == "procs" || field == "gtwMtn")
                                                            {
                                                                FltAnd &= Bld.Eq<bool>(field, Convert.ToBoolean(value.ToLower()));
                                                            }
                                                            else if (field == "rssi")
                                                            {
                                                                FltAnd &= Bld.Eq<double>(field, Convert.ToDouble(value.ToLower()));
                                                            }
                                                            else if (field == "btrLvl" || field == "btry")
                                                            {
                                                                if (value.ToLower() == "low" || value.ToLower() == "normal")
                                                                {
                                                                    FltAnd &= Bld.Eq<string>(field, value.ToLower());
                                                                }
                                                                else
                                                                {
                                                                    FltAnd &= Bld.Eq<int>(field, Convert.ToInt32(value.ToLower()));
                                                                }
                                                            }
                                                            else if (field == "picNotRcvMin")
                                                            {
                                                                if (value.ToLower() == "exist")
                                                                {
                                                                    FltAnd &= Bld.Not(Bld.Eq<string>("pics", null));
                                                                }
                                                                else if (value.ToLower() == "not exist")
                                                                {
                                                                    FltAnd &= Bld.Eq<byte[]>("pic", null);
                                                                }
                                                            }
                                                            else
                                                            {
                                                                FltAnd &= Bld.Eq<string>(field, value);
                                                            }
                                                        }
                                                    }
                                                    #endregion
                                                    and = true;
                                                }
                                                else
                                                {
                                                    #region Or
                                                    for (int b = 0; b < poFIlter.FilterDescriptors.Count; b++)
                                                    {
                                                        if (poFIlter.FilterDescriptors[b].GetType().ToString() == "Kendo.Mvc.CompositeFilterDescriptor")
                                                        {
                                                            Kendo.Mvc.CompositeFilterDescriptor paFIlter = poFIlter.FilterDescriptors[a] as Kendo.Mvc.CompositeFilterDescriptor;
                                                            foreach (Kendo.Mvc.FilterDescriptor doFilter in paFIlter.FilterDescriptors)
                                                            {
                                                                string field = doFilter.Member;
                                                                if (field == "ts" || field == "tagTs" || field == "crtOn" || field == "picRcv")
                                                                {
                                                                    string Opr = doFilter.Operator.ToString();
                                                                    string value = doFilter.Value.ToString();

                                                                    if (Opr == "IsGreaterThanOrEqualTo")
                                                                    {
                                                                        FltAnd &= Bld.Gte<DateTime>(field, DateTime.Parse(value));
                                                                    }
                                                                    else if (Opr == "IsLessThanOrEqualTo")
                                                                    {
                                                                        FltAnd &= Bld.Lte<DateTime>(field, DateTime.Parse(value));
                                                                    }
                                                                }
                                                                else if (field == "dateStart" || field == "dateEnd" || field == "dob")
                                                                {
                                                                    string Opr = doFilter.Operator.ToString();
                                                                    string value = doFilter.Value.ToString();
                                                                    var voValue = DateTime.Parse(value).Date;
                                                                    if (Opr == "IsGreaterThanOrEqualTo")
                                                                    {
                                                                        FltAnd &= Bld.Gte<DateTime>(field, voValue);
                                                                    }
                                                                    else if (Opr == "IsLessThanOrEqualTo")
                                                                    {
                                                                        FltAnd &= Bld.Lte<DateTime>(field, voValue.AddHours(23));
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    string Opr = doFilter.Operator.ToString();
                                                                    Opr = (Opr == "IsEqualTo") ? "$eq" : Opr;
                                                                    string value = doFilter.Value.ToString();
                                                                    if (field == "tmprd" || field == "procs" || field == "gtwMtn")
                                                                    {
                                                                        FltOr |= Bld.Eq<bool>(field, Convert.ToBoolean(value.ToLower()));
                                                                    }
                                                                    else if (field == "rssi")
                                                                    {
                                                                        FltOr |= Bld.Eq<double>(field, Convert.ToDouble(value.ToLower()));
                                                                    }
                                                                    else if (field == "btrLvl" || field == "btry")
                                                                    {
                                                                        if (value.ToLower() == "low" || value.ToLower() == "normal")
                                                                        {
                                                                            FltOr |= Bld.Eq<string>(field, value.ToLower());
                                                                        }
                                                                        else
                                                                        {
                                                                            FltOr |= Bld.Eq<int>(field, Convert.ToInt32(value.ToLower()));
                                                                        }
                                                                    }
                                                                    else if (field == "picNotRcvMin")
                                                                    {
                                                                        if (value.ToLower() == "exist")
                                                                        {
                                                                            FltOr |= Bld.Not(Bld.Eq<string>("pics", null));
                                                                        }
                                                                        else if (value.ToLower() == "not exist")
                                                                        {
                                                                            FltOr |= Bld.Eq<byte[]>("pic", null);
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        FltOr |= Bld.Eq<string>(field, value);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            Kendo.Mvc.FilterDescriptor doFilter = poFIlter.FilterDescriptors[b] as Kendo.Mvc.FilterDescriptor;
                                                            string field = doFilter.Member;
                                                            if (field == "ts" || field == "tagTs" || field == "crtOn" || field == "picRcv")
                                                            {
                                                                string Opr = doFilter.Operator.ToString();
                                                                string value = doFilter.Value.ToString();

                                                                if (Opr == "IsGreaterThanOrEqualTo")
                                                                {
                                                                    FltAnd &= Bld.Gte<DateTime>(field, DateTime.Parse(value));
                                                                }
                                                                else if (Opr == "IsLessThanOrEqualTo")
                                                                {
                                                                    FltAnd &= Bld.Lte<DateTime>(field, DateTime.Parse(value));
                                                                }
                                                            }
                                                            else if (field == "dateStart" || field == "dateEnd" || field == "dob")
                                                            {
                                                                string Opr = doFilter.Operator.ToString();
                                                                string value = doFilter.Value.ToString();
                                                                var voValue = DateTime.Parse(value).Date;
                                                                if (Opr == "IsGreaterThanOrEqualTo")
                                                                {
                                                                    FltAnd &= Bld.Gte<DateTime>(field, voValue);
                                                                }
                                                                else if (Opr == "IsLessThanOrEqualTo")
                                                                {
                                                                    FltAnd &= Bld.Lte<DateTime>(field, voValue.AddHours(23));
                                                                }
                                                            }
                                                            else
                                                            {
                                                                string Opr = doFilter.Operator.ToString();
                                                                Opr = (Opr == "IsEqualTo") ? "$eq" : Opr;
                                                                string value = doFilter.Value.ToString();
                                                                if (field == "tmprd" || field == "procs" || field == "gtwMtn")
                                                                {
                                                                    FltOr |= Bld.Eq<bool>(field, Convert.ToBoolean(value.ToLower()));
                                                                }
                                                                else if (field == "rssi")
                                                                {
                                                                    FltOr |= Bld.Eq<double>(field, Convert.ToDouble(value.ToLower()));
                                                                }
                                                                else if (field == "btrLvl" || field == "btry")
                                                                {
                                                                    if (value.ToLower() == "low" || value.ToLower() == "normal")
                                                                    {
                                                                        FltOr |= Bld.Eq<string>(field, value.ToLower());
                                                                    }
                                                                    else
                                                                    {
                                                                        FltOr |= Bld.Eq<int>(field, Convert.ToInt32(value.ToLower()));
                                                                    }
                                                                }
                                                                else if (field == "picNotRcvMin")
                                                                {
                                                                    if (value.ToLower() == "exist")
                                                                    {
                                                                        FltOr |= Bld.Not(Bld.Eq<string>("pics", null));
                                                                    }
                                                                    else if (value.ToLower() == "not exist")
                                                                    {
                                                                        FltOr |= Bld.Eq<byte[]>("pic", null);
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    FltOr |= Bld.Eq<string>(field, value);
                                                                }
                                                            }
                                                        }

                                                    }
                                                    #endregion
                                                    or = true;
                                                }
                                            }
                                            else
                                            {
                                                Kendo.Mvc.FilterDescriptor poFIlter = poCompositeFIlter.FilterDescriptors[a] as Kendo.Mvc.FilterDescriptor;
                                                string field = poFIlter.Member;
                                                if (field == "ts" || field == "tagTs" || field == "crtOn" || field == "picRcv")
                                                {
                                                    string Opr = poFIlter.Operator.ToString();
                                                    string value = poFIlter.Value.ToString();

                                                    if (Opr == "IsGreaterThanOrEqualTo")
                                                    {
                                                        FltAnd &= Bld.Gte<DateTime>(field, DateTime.Parse(value));
                                                    }
                                                    else if (Opr == "IsLessThanOrEqualTo")
                                                    {
                                                        FltAnd &= Bld.Lte<DateTime>(field, DateTime.Parse(value));
                                                    }
                                                }
                                                else if (field == "dateStart" || field == "dateEnd" || field == "dob")
                                                {
                                                    string Opr = poFIlter.Operator.ToString();
                                                    string value = poFIlter.Value.ToString();
                                                    var voValue = DateTime.Parse(value).Date;
                                                    if (Opr == "IsGreaterThanOrEqualTo")
                                                    {
                                                        FltAnd &= Bld.Gte<DateTime>(field, voValue);
                                                    }
                                                    else if (Opr == "IsLessThanOrEqualTo")
                                                    {
                                                        FltAnd &= Bld.Lte<DateTime>(field, voValue.AddHours(23));
                                                    }
                                                }
                                                else
                                                {
                                                    string Opr = poFIlter.Operator.ToString();
                                                    Opr = (Opr == "IsEqualTo") ? "$eq" : Opr;
                                                    string value = poFIlter.Value.ToString();
                                                    if (field == "tmprd" || field == "procs" || field == "gtwMtn")
                                                    {
                                                        FltOr |= Bld.Eq<bool>(field, Convert.ToBoolean(value.ToLower()));
                                                    }
                                                    else if (field == "rssi")
                                                    {
                                                        FltOr |= Bld.Eq<double>(field, Convert.ToDouble(value.ToLower()));
                                                    }
                                                    else if (field == "btrLvl" || field == "btry")
                                                    {
                                                        if (value.ToLower() == "low" || value.ToLower() == "normal")
                                                        {
                                                            FltOr |= Bld.Eq<string>(field, value.ToLower());
                                                        }
                                                        else
                                                        {
                                                            FltOr |= Bld.Eq<int>(field, Convert.ToInt32(value.ToLower()));
                                                        }
                                                    }
                                                    else if (field == "picNotRcvMin")
                                                    {
                                                        if (value.ToLower() == "exist")
                                                        {
                                                            FltOr |= Bld.Not(Bld.Eq<string>("pics", null));
                                                        }
                                                        else if (value.ToLower() == "not exist")
                                                        {
                                                            FltOr |= Bld.Eq<byte[]>("pic", null);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        FltOr |= Bld.Eq<string>(field, value);
                                                    }
                                                }
                                            }
                                        }
                                        #endregion
                                        or = true;
                                    }
                                }
                                else
                                {
                                    Kendo.Mvc.FilterDescriptor poFIlter = voFIlter.FilterDescriptors[i] as Kendo.Mvc.FilterDescriptor;
                                    string field = poFIlter.Member;
                                    if (field == "ts" || field == "tagTs" || field == "crtOn" || field == "picRcv")
                                    {
                                        if (poFIlter.Value != null)
                                        {
                                            string Opr = poFIlter.Operator.ToString();
                                            string value = poFIlter.Value.ToString();

                                            if (Opr == "IsGreaterThanOrEqualTo")
                                            {
                                                FltAnd &= Bld.Gte<DateTime>(field, DateTime.Parse(value));
                                            }
                                            else if (Opr == "IsLessThanOrEqualTo")
                                            {
                                                FltAnd &= Bld.Lte<DateTime>(field, DateTime.Parse(value));
                                            }
                                        }
                                    }
                                    else if (field == "dateStart" || field == "dateEnd" || field == "dob")
                                    {
                                        string Opr = poFIlter.Operator.ToString();
                                        string value = poFIlter.Value.ToString();
                                        var voValue = DateTime.Parse(value).Date;
                                        if (Opr == "IsGreaterThanOrEqualTo")
                                        {
                                            FltAnd &= Bld.Gte<DateTime>(field, voValue);
                                        }
                                        else if (Opr == "IsLessThanOrEqualTo")
                                        {
                                            FltAnd &= Bld.Lte<DateTime>(field, voValue.AddHours(23));
                                        }
                                    }
                                    else
                                    {
                                        string Opr = poFIlter.Operator.ToString();
                                        Opr = (Opr == "IsEqualTo") ? "$eq" : Opr;
                                        string value = poFIlter.Value.ToString();
                                        if (field == "tmprd" || field == "procs" || field == "gtwMtn")
                                        {
                                            FltAnd &= Bld.Eq<bool>(field, Convert.ToBoolean(value.ToLower()));
                                        }
                                        else if (field == "rssi")
                                        {
                                            FltAnd &= Bld.Eq<double>(field, Convert.ToDouble(value.ToLower()));
                                        }
                                        else if (field == "btrLvl" || field == "btry")
                                        {
                                            if (value.ToLower() == "low" || value.ToLower() == "normal")
                                            {
                                                FltAnd &= Bld.Eq<string>(field, value.ToLower());
                                            }
                                            else
                                            {
                                                FltAnd &= Bld.Eq<int>(field, Convert.ToInt32(value.ToLower()));
                                            }
                                        }
                                        else if (field == "picNotRcvMin")
                                        {
                                            if (value.ToLower() == "exist")
                                            {
                                                FltAnd &= Bld.Not(Bld.Eq<string>("pics", null));
                                            }
                                            else if (value.ToLower() == "not exist")
                                            {
                                                FltAnd &= Bld.Eq<byte[]>("pic", null);
                                            }
                                        }
                                        else if (field == "mstMblNo")
                                        {
                                            string val = value.Substring(2, value.Length - 2);
                                            FltAnd &= Bld.Eq<string>(field, val) | Bld.Eq<string>(field, value);
                                        }
                                        else if (field == "mblNo")
                                        {
                                            string val = value.Substring(2, value.Length - 2);
                                            FltAnd &= Bld.Eq<string>(field, val) | Bld.Eq<string>(field, value);
                                        }
                                        else
                                        {
                                            FltAnd &= Bld.Eq<string>(field, value);
                                        }
                                    }
                                }
                            }
                            #endregion
                            and = true;
                        }
                        else
                        {
                            #region Or
                            for (int i = 0; i < voFIlter.FilterDescriptors.Count; i++)
                            {
                                if (voFIlter.FilterDescriptors[i].GetType().ToString() == "Kendo.Mvc.CompositeFilterDescriptor")
                                {
                                    Kendo.Mvc.CompositeFilterDescriptor poCompositeFIlter = voFIlter.FilterDescriptors[i] as Kendo.Mvc.CompositeFilterDescriptor;

                                    for (int a = 0; a < poCompositeFIlter.FilterDescriptors.Count; a++)
                                    {
                                        if (poCompositeFIlter.FilterDescriptors[a].GetType().ToString() == "Kendo.Mvc.CompositeFilterDescriptor")
                                        {
                                            Kendo.Mvc.CompositeFilterDescriptor poFIlter = poCompositeFIlter.FilterDescriptors[a] as Kendo.Mvc.CompositeFilterDescriptor;
                                            foreach (Kendo.Mvc.FilterDescriptor doFilter in poFIlter.FilterDescriptors)
                                            {
                                                string field = doFilter.Member;
                                                if (field == "ts" || field == "tagTs" || field == "crtOn" || field == "picRcv")
                                                {
                                                    string Opr = doFilter.Operator.ToString();
                                                    string value = doFilter.Value.ToString();

                                                    if (Opr == "IsGreaterThanOrEqualTo")
                                                    {
                                                        FltAnd &= Bld.Gte<DateTime>(field, DateTime.Parse(value));
                                                    }
                                                    else if (Opr == "IsLessThanOrEqualTo")
                                                    {
                                                        FltAnd &= Bld.Lte<DateTime>(field, DateTime.Parse(value));
                                                    }
                                                }
                                                else if (field == "dateStart" || field == "dateEnd" || field == "dob")
                                                {
                                                    string Opr = doFilter.Operator.ToString();
                                                    string value = doFilter.Value.ToString();
                                                    var voValue = DateTime.Parse(value).Date;
                                                    if (Opr == "IsGreaterThanOrEqualTo")
                                                    {
                                                        FltAnd &= Bld.Gte<DateTime>(field, voValue);
                                                    }
                                                    else if (Opr == "IsLessThanOrEqualTo")
                                                    {
                                                        FltAnd &= Bld.Lte<DateTime>(field, voValue.AddHours(23));
                                                    }
                                                }
                                                else
                                                {
                                                    string Opr = doFilter.Operator.ToString();
                                                    Opr = (Opr == "IsEqualTo") ? "$eq" : Opr;
                                                    string value = doFilter.Value.ToString();

                                                    if (field == "tmprd" || field == "procs" || field == "gtwMtn")
                                                    {
                                                        FltOr |= Bld.Eq<bool>(field, Convert.ToBoolean(value.ToLower()));
                                                    }
                                                    else if (field == "rssi")
                                                    {
                                                        FltOr |= Bld.Eq<double>(field, Convert.ToDouble(value.ToLower()));
                                                    }
                                                    else if (field == "btrLvl" || field == "btry")
                                                    {
                                                        if (value.ToLower() == "low" || value.ToLower() == "normal")
                                                        {
                                                            FltOr |= Bld.Eq<string>(field, value.ToLower());
                                                        }
                                                        else
                                                        {
                                                            FltOr |= Bld.Eq<int>(field, Convert.ToInt32(value.ToLower()));
                                                        }
                                                    }
                                                    else if (field == "picNotRcvMin")
                                                    {
                                                        if (value.ToLower() == "exist")
                                                        {
                                                            FltOr |= Bld.Not(Bld.Eq<string>("pics", null));
                                                        }
                                                        else if (value.ToLower() == "not exist")
                                                        {
                                                            FltOr |= Bld.Eq<byte[]>("pic", null);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        FltOr |= Bld.Eq<string>(field, value);
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            Kendo.Mvc.FilterDescriptor poFIlter = poCompositeFIlter.FilterDescriptors[a] as Kendo.Mvc.FilterDescriptor;
                                            string field = poFIlter.Member;
                                            if (field == "ts" || field == "tagTs" || field == "crtOn" || field == "picRcv")
                                            {
                                                string Opr = poFIlter.Operator.ToString();
                                                string value = poFIlter.Value.ToString();

                                                if (Opr == "IsGreaterThanOrEqualTo")
                                                {
                                                    FltAnd &= Bld.Gte<DateTime>(field, DateTime.Parse(value));
                                                }
                                                else if (Opr == "IsLessThanOrEqualTo")
                                                {
                                                    FltAnd &= Bld.Lte<DateTime>(field, DateTime.Parse(value));
                                                }
                                            }
                                            else if (field == "dateStart" || field == "dateEnd" || field == "dob")
                                            {
                                                string Opr = poFIlter.Operator.ToString();
                                                string value = poFIlter.Value.ToString();
                                                var voValue = DateTime.Parse(value).Date;
                                                if (Opr == "IsGreaterThanOrEqualTo")
                                                {
                                                    FltAnd &= Bld.Gte<DateTime>(field, voValue);
                                                }
                                                else if (Opr == "IsLessThanOrEqualTo")
                                                {
                                                    FltAnd &= Bld.Lte<DateTime>(field, voValue.AddHours(23));
                                                }
                                            }
                                            else
                                            {
                                                string Opr = poFIlter.Operator.ToString();
                                                Opr = (Opr == "IsEqualTo") ? "$eq" : Opr;
                                                string value = poFIlter.Value.ToString();
                                                if (field == "tmprd" || field == "procs" || field == "gtwMtn")
                                                {
                                                    FltOr |= Bld.Eq<bool>(field, Convert.ToBoolean(value.ToLower()));
                                                }
                                                else if (field == "rssi")
                                                {
                                                    FltOr |= Bld.Eq<double>(field, Convert.ToDouble(value.ToLower()));
                                                }
                                                else if (field == "btrLvl" || field == "btry")
                                                {
                                                    if (value.ToLower() == "low" || value.ToLower() == "normal")
                                                    {
                                                        FltOr |= Bld.Eq<string>(field, value.ToLower());
                                                    }
                                                    else
                                                    {
                                                        FltOr |= Bld.Eq<int>(field, Convert.ToInt32(value.ToLower()));
                                                    }
                                                }
                                                else if (field == "picNotRcvMin")
                                                {
                                                    if (value.ToLower() == "exist")
                                                    {
                                                        FltOr |= Bld.Not(Bld.Eq<string>("pics", null));
                                                    }
                                                    else if (value.ToLower() == "not exist")
                                                    {
                                                        FltOr |= Bld.Eq<byte[]>("pic", null);
                                                    }
                                                }
                                                else
                                                {
                                                    FltOr |= Bld.Eq<string>(field, value);
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    Kendo.Mvc.FilterDescriptor poFIlter = voFIlter.FilterDescriptors[i] as Kendo.Mvc.FilterDescriptor;
                                    string field = poFIlter.Member;
                                    if (field == "ts" || field == "tagTs" || field == "crtOn" || field == "picRcv")
                                    {
                                        string Opr = poFIlter.Operator.ToString();
                                        string value = poFIlter.Value.ToString();

                                        if (Opr == "IsGreaterThanOrEqualTo")
                                        {
                                            FltOr |= Bld.Gte<DateTime>(field, DateTime.Parse(value));
                                        }
                                        else if (Opr == "IsLessThanOrEqualTo")
                                        {
                                            FltOr |= Bld.Lte<DateTime>(field, DateTime.Parse(value));
                                        }
                                    }
                                    else if (field == "dateStart" || field == "dateEnd" || field == "dob")
                                    {
                                        string Opr = poFIlter.Operator.ToString();
                                        string value = poFIlter.Value.ToString();
                                        var voValue = DateTime.Parse(value).Date;
                                        if (Opr == "IsGreaterThanOrEqualTo")
                                        {
                                            FltOr |= Bld.Gte<DateTime>(field, voValue);
                                        }
                                        else if (Opr == "IsLessThanOrEqualTo")
                                        {
                                            FltOr |= Bld.Lte<DateTime>(field, voValue.AddHours(23));
                                        }
                                    }
                                    else
                                    {
                                        string Opr = poFIlter.Operator.ToString();
                                        Opr = (Opr == "IsEqualTo") ? "$eq" : Opr;
                                        string value = poFIlter.Value.ToString();

                                        if (field == "tmprd" || field == "procs" || field == "gtwMtn")
                                        {
                                            FltOr |= Bld.Eq<bool>(field, Convert.ToBoolean(value.ToLower()));
                                        }
                                        else if (field == "rssi")
                                        {
                                            FltOr |= Bld.Eq<double>(field, Convert.ToDouble(value.ToLower()));
                                        }
                                        else if (field == "btrLvl" || field == "btry")
                                        {
                                            if (value.ToLower() == "low" || value.ToLower() == "normal")
                                            {
                                                FltOr |= Bld.Eq<string>(field, value.ToLower());
                                            }
                                            else
                                            {
                                                FltOr |= Bld.Eq<int>(field, Convert.ToInt32(value.ToLower()));
                                            }
                                        }
                                        else if (field == "picNotRcvMin")
                                        {
                                            if (value.ToLower() == "exist")
                                            {
                                                FltOr |= Bld.Not(Bld.Eq<string>("pics", null));
                                            }
                                            else if (value.ToLower() == "not exist")
                                            {
                                                FltOr |= Bld.Eq<byte[]>("pic", null);
                                            }
                                        }
                                        else
                                        {
                                            FltOr |= Bld.Eq<string>(field, value);
                                        }
                                    }
                                }
                            }
                            #endregion
                            or = true;
                        }
                    }
                }
                else
                {
                    foreach (Kendo.Mvc.FilterDescriptor poFIlter in poRequest.Filters)
                    {
                        string field = poFIlter.Member;
                        string Opr = poFIlter.Operator.ToString();
                        Opr = (Opr == "IsEqualTo") ? "$eq" : Opr;
                        string value = poFIlter.Value.ToString();
                        if (field == "tmprd" || field == "procs" || field == "gtwMtn")
                        {
                            FltAnd &= Bld.Eq<bool>(field, Convert.ToBoolean(value.ToLower()));
                        }
                        else if (field == "rssi")
                        {
                            FltAnd &= Bld.Eq<double>(field, Convert.ToDouble(value.ToLower()));
                        }
                        else if (field == "btrLvl" || field == "btry")
                        {
                            if (value.ToLower() == "low" || value.ToLower() == "normal")
                            {
                                FltAnd &= Bld.Eq<string>(field, value.ToLower());
                            }
                            else
                            {
                                FltAnd &= Bld.Eq<int>(field, Convert.ToInt32(value.ToLower()));
                            }
                        }
                        else if (field == "picNotRcvMin")
                        {
                            if (value.ToLower() == "exist")
                            {
                                FltAnd &= Bld.Not(Bld.Eq<string>("pics", null));
                            }
                            else if (value.ToLower() == "not exist")
                            {
                                FltAnd &= Bld.Eq<byte[]>("pic", null);
                            }
                        }
                        else if (field == "mstMblNo")
                        {
                            string val = value.Substring(2, value.Length - 2);
                            FltAnd &= Bld.Eq<string>(field, val) | Bld.Eq<string>(field, value);
                        }
                        else if (field == "mblNo")
                        {
                            string val = value.Substring(2, value.Length - 2);
                            FltAnd &= Bld.Eq<string>(field, val) | Bld.Eq<string>(field, value);
                        }
                        else
                        {
                            FltAnd &= Bld.Eq<string>(field, value);
                        }
                    }
                    and = true;
                }
            }

            var Flt = Bld.Empty;
            if (and && or)
            {
                Flt = FltAnd & FltOr;
            }
            else if (and && !or)
            {
                Flt = FltAnd;
            }
            else if (!and && or)
            {
                Flt = FltOr;
            }

            return Flt;
        }

        public static FilterDefinition<T> gridFilterCount<T>([DataSourceRequest] DataSourceRequest poRequest)
        {
            var Bld = Builders<T>.Filter;
            var FltAnd = Bld.And();
            var FltOr = Bld.Or();
            bool and = false;
            bool or = false;

            if (poRequest.Filters.Count > 0)
            {
                if (poRequest.Filters[0].GetType().ToString() == "Kendo.Mvc.CompositeFilterDescriptor")
                {

                    foreach (Kendo.Mvc.CompositeFilterDescriptor voFIlter in poRequest.Filters)
                    {
                        if (voFIlter.LogicalOperator.ToString() == "And")
                        {
                            #region And
                            for (int i = 0; i < voFIlter.FilterDescriptors.Count; i++)
                            {
                                if (voFIlter.FilterDescriptors[i].GetType().ToString() == "Kendo.Mvc.CompositeFilterDescriptor")
                                {
                                    Kendo.Mvc.CompositeFilterDescriptor poCompositeFIlter = voFIlter.FilterDescriptors[i] as Kendo.Mvc.CompositeFilterDescriptor;
                                    if (poCompositeFIlter.LogicalOperator.ToString() == "And")
                                    {
                                        #region "And"
                                        for (int a = 0; a < poCompositeFIlter.FilterDescriptors.Count; a++)
                                        {
                                            if (poCompositeFIlter.FilterDescriptors[a].GetType().ToString() == "Kendo.Mvc.CompositeFilterDescriptor")
                                            {
                                                Kendo.Mvc.CompositeFilterDescriptor poFIlter = poCompositeFIlter.FilterDescriptors[a] as Kendo.Mvc.CompositeFilterDescriptor;
                                                if (poFIlter.LogicalOperator.ToString() == "And")
                                                {
                                                    #region "And"
                                                    foreach (Kendo.Mvc.FilterDescriptor doFilter in poFIlter.FilterDescriptors)
                                                    {
                                                        string field = doFilter.Member;
                                                        if (field == "ts" || field == "tagTs" || field == "crtOn" || field == "picRcv")
                                                        {
                                                            string Opr = doFilter.Operator.ToString();
                                                            string value = doFilter.Value.ToString();

                                                            if (Opr == "IsGreaterThanOrEqualTo")
                                                            {
                                                                FltAnd &= Bld.Gte<DateTime>(field, DateTime.Parse(value));
                                                            }
                                                            else if (Opr == "IsLessThanOrEqualTo")
                                                            {
                                                                FltAnd &= Bld.Lte<DateTime>(field, DateTime.Parse(value));
                                                            }
                                                        }
                                                        else if (field == "dateStart" || field == "dateEnd" || field == "dob")
                                                        {
                                                            string Opr = doFilter.Operator.ToString();
                                                            string value = doFilter.Value.ToString();
                                                            var voValue = DateTime.Parse(value).Date;
                                                            if (Opr == "IsGreaterThanOrEqualTo")
                                                            {
                                                                FltAnd &= Bld.Gte<DateTime>(field, voValue);
                                                            }
                                                            else if (Opr == "IsLessThanOrEqualTo")
                                                            {
                                                                FltAnd &= Bld.Lte<DateTime>(field, voValue.AddHours(23));
                                                            }
                                                        }
                                                        else
                                                        {
                                                            string Opr = doFilter.Operator.ToString();
                                                            Opr = (Opr == "IsEqualTo") ? "$eq" : Opr;
                                                            string value = doFilter.Value.ToString();
                                                            if (field == "tmprd" || field == "temprd" || field == "procs" || field == "gtwMtn" || field == "rawData" || field == "ack" || field == "notify" || field == "act" || field == "suspend" || field == "trueTemprd" || field == "crntAlt" || field == "crntData")
                                                            {
                                                                bool val = (value.ToLower() == "yes") ? true : false;
                                                                field = (field == "act") ? "shnAct" : field;
                                                                if (field == "suspend")
                                                                {
                                                                    field = (value.ToLower() == "yes") ? "suspndAlrt" : field;
                                                                    field = (value.ToLower() == "no") ? "suspndNoAlrt" : field;
                                                                    field = (value.ToLower() == "gps only") ? "suspndGpsAlrt" : field;
                                                                    val = (value.ToLower() == "yes") ? true : val;
                                                                    val = (value.ToLower() == "no") ? true : val;
                                                                    val = (value.ToLower() == "gps only") ? true : val;
                                                                }
                                                                FltAnd &= Bld.Eq<bool>(field, val);
                                                            }
                                                            else if (field == "rssi")
                                                            {
                                                                FltAnd &= Bld.Eq<double>(field, Convert.ToDouble(value.ToLower()));
                                                            }
                                                            else if (field == "btrLvl" || field == "btry")
                                                            {
                                                                if (value.ToLower() == "low")
                                                                {
                                                                    FltAnd &= Bld.Lte<int>(field, Convert.ToInt32("97"));
                                                                }
                                                                else if (value.ToLower() == "normal")
                                                                {
                                                                    FltAnd &= Bld.Gte<int>(field, Convert.ToInt32("97"));
                                                                }
                                                                else
                                                                {
                                                                    FltAnd &= Bld.Eq<int>(field, Convert.ToInt32(value.ToLower()));
                                                                }
                                                            }
                                                            else if (field == "picNotRcvMin")
                                                            {
                                                                if (value.ToLower() == "exist")
                                                                {
                                                                    FltAnd &= Bld.Not(Bld.Eq<byte[]>("pic", null));
                                                                }
                                                                else if (value.ToLower() == "not exist")
                                                                {
                                                                    FltAnd &= Bld.Eq<byte[]>("pic", null);
                                                                }
                                                            }
                                                            else if (field == "tagId" || field == "gtwId")
                                                            {
                                                                string[] vals = value.Split('-');
                                                                string val = (vals.Length > 1) ? Convert.ToInt64(Convert.ToInt64(vals[2]).ToString()).ToString() : value;
                                                                FltAnd &= Bld.Eq<string>(field, val);
                                                            }
                                                            //else if (field == "evTypeName")
                                                            //{
                                                            //    string[] vals = value.Split('-');
                                                            //    string val = (vals.Length > 0) ? vals[0] : value;
                                                            //    FltAnd &= Bld.Eq<string>("evType", val) | Bld.Eq<string>("evTypeId", val);
                                                            //}
                                                            else if (field == "mstMblNo")
                                                            {
                                                                string val = value.Substring(2, value.Length - 2);
                                                                FltAnd &= Bld.Eq<string>(field, val) | Bld.Eq<string>(field, value);
                                                            }
                                                            else
                                                            {
                                                                FltAnd &= Bld.Eq<string>(field, value);
                                                            }
                                                        }
                                                    }
                                                    #endregion
                                                    and = true;
                                                }
                                                else
                                                {
                                                    #region "Or"
                                                    foreach (Kendo.Mvc.FilterDescriptor doFilter in poFIlter.FilterDescriptors)
                                                    {
                                                        string field = doFilter.Member;
                                                        if (field == "ts" || field == "tagTs" || field == "crtOn" || field == "picRcv")
                                                        {
                                                            string Opr = doFilter.Operator.ToString();
                                                            string value = doFilter.Value.ToString();

                                                            if (Opr == "IsGreaterThanOrEqualTo")
                                                            {
                                                                FltAnd &= Bld.Gte<DateTime>(field, DateTime.Parse(value));
                                                            }
                                                            else if (Opr == "IsLessThanOrEqualTo")
                                                            {
                                                                FltAnd &= Bld.Lte<DateTime>(field, DateTime.Parse(value));
                                                            }
                                                        }
                                                        else if (field == "dateStart" || field == "dateEnd" || field == "dob")
                                                        {
                                                            string Opr = doFilter.Operator.ToString();
                                                            string value = doFilter.Value.ToString();
                                                            var voValue = DateTime.Parse(value).Date;
                                                            if (Opr == "IsGreaterThanOrEqualTo")
                                                            {
                                                                FltAnd &= Bld.Gte<DateTime>(field, voValue);
                                                            }
                                                            else if (Opr == "IsLessThanOrEqualTo")
                                                            {
                                                                FltAnd &= Bld.Lte<DateTime>(field, voValue.AddHours(23));
                                                            }
                                                        }
                                                        else
                                                        {
                                                            string Opr = doFilter.Operator.ToString();
                                                            Opr = (Opr == "IsEqualTo") ? "$eq" : Opr;
                                                            string value = doFilter.Value.ToString();
                                                            if (field == "tmprd" || field == "temprd" || field == "procs" || field == "gtwMtn" || field == "rawData" || field == "ack" || field == "notify" || field == "act" || field == "suspend" || field == "trueTemprd" || field == "crntAlt" || field == "crntData")
                                                            {
                                                                bool val = (value.ToLower() == "yes") ? true : false;
                                                                field = (field == "act") ? "shnAct" : field;
                                                                if (field == "suspend")
                                                                {
                                                                    field = (value.ToLower() == "yes") ? "suspndAlrt" : field;
                                                                    field = (value.ToLower() == "no") ? "suspndNoAlrt" : field;
                                                                    field = (value.ToLower() == "gps only") ? "suspndGpsAlrt" : field;
                                                                    val = (value.ToLower() == "yes") ? true : val;
                                                                    val = (value.ToLower() == "no") ? true : val;
                                                                    val = (value.ToLower() == "gps only") ? true : val;
                                                                }
                                                                FltOr |= Bld.Eq<bool>(field, val);
                                                            }
                                                            else if (field == "rssi")
                                                            {
                                                                FltOr |= Bld.Eq<double>(field, Convert.ToDouble(value.ToLower()));
                                                            }
                                                            else if (field == "btrLvl" || field == "btry")
                                                            {
                                                                if (value.ToLower() == "low")
                                                                {
                                                                    FltOr |= Bld.Lte<int>(field, Convert.ToInt32("97"));
                                                                }
                                                                else if (value.ToLower() == "normal")
                                                                {
                                                                    FltOr |= Bld.Gte<int>(field, Convert.ToInt32("97"));
                                                                }
                                                                else
                                                                {
                                                                    FltOr |= Bld.Eq<int>(field, Convert.ToInt32(value.ToLower()));
                                                                }
                                                            }
                                                            else if (field == "picNotRcvMin")
                                                            {
                                                                if (value.ToLower() == "exist")
                                                                {
                                                                    FltOr |= Bld.Not(Bld.Eq<byte[]>("pic", null));
                                                                }
                                                                else if (value.ToLower() == "not exist")
                                                                {
                                                                    FltOr |= Bld.Eq<byte[]>("pic", null);
                                                                }
                                                            }
                                                            else if (field == "tagId" || field == "gtwId")
                                                            {
                                                                string[] vals = value.Split('-');
                                                                string val = (vals.Length > 1) ? Convert.ToInt64(vals[2]).ToString() : value;
                                                                FltOr |= Bld.Eq<string>(field, val);
                                                            }
                                                            //else if (field == "evTypeName")
                                                            //{
                                                            //    string[] vals = value.Split('-');
                                                            //    string val = (vals.Length > 0) ? vals[0] : value;
                                                            //    FltOr |= Bld.Eq<string>("evType", val) | Bld.Eq<string>("evTypeId", val);
                                                            //}
                                                            else if (field == "mstMblNo")
                                                            {
                                                                string val = value.Substring(2, value.Length - 2);
                                                                FltOr |= Bld.Eq<string>(field, val) | Bld.Eq<string>(field, value);
                                                            }
                                                            else
                                                            {
                                                                FltOr |= Bld.Eq<string>(field, value);
                                                            }
                                                        }
                                                    }
                                                    #endregion
                                                    or = true;
                                                }
                                            }
                                            else
                                            {
                                                Kendo.Mvc.FilterDescriptor poFIlter = poCompositeFIlter.FilterDescriptors[a] as Kendo.Mvc.FilterDescriptor;
                                                string field = poFIlter.Member;
                                                if (field == "ts" || field == "tagTs" || field == "crtOn" || field == "picRcv")
                                                {
                                                    string Opr = poFIlter.Operator.ToString();
                                                    string value = poFIlter.Value.ToString();

                                                    if (Opr == "IsGreaterThanOrEqualTo")
                                                    {
                                                        FltAnd &= Bld.Gte<DateTime>(field, DateTime.Parse(value));
                                                    }
                                                    else if (Opr == "IsLessThanOrEqualTo")
                                                    {
                                                        FltAnd &= Bld.Lte<DateTime>(field, DateTime.Parse(value));
                                                    }
                                                }
                                                else if (field == "dateStart" || field == "dateEnd" || field == "dob")
                                                {
                                                    string Opr = poFIlter.Operator.ToString();
                                                    string value = poFIlter.Value.ToString();
                                                    var voValue = DateTime.Parse(value).Date;
                                                    if (Opr == "IsGreaterThanOrEqualTo")
                                                    {
                                                        FltAnd &= Bld.Gte<DateTime>(field, voValue);
                                                    }
                                                    else if (Opr == "IsLessThanOrEqualTo")
                                                    {
                                                        FltAnd &= Bld.Lte<DateTime>(field, voValue.AddHours(23));
                                                    }
                                                }
                                                else
                                                {
                                                    string Opr = poFIlter.Operator.ToString();
                                                    Opr = (Opr == "IsEqualTo") ? "$eq" : Opr;
                                                    string value = poFIlter.Value.ToString();
                                                    if (field == "tmprd" || field == "temprd" || field == "procs" || field == "gtwMtn" || field == "rawData" || field == "ack" || field == "notify" || field == "act" || field == "suspend" || field == "trueTemprd" || field == "crntAlt" || field == "crntData")
                                                    {
                                                        bool val = (value.ToLower() == "yes") ? true : false;
                                                        field = (field == "act") ? "shnAct" : field;
                                                        if (field == "suspend")
                                                        {
                                                            field = (value.ToLower() == "yes") ? "suspndAlrt" : field;
                                                            field = (value.ToLower() == "no") ? "suspndNoAlrt" : field;
                                                            field = (value.ToLower() == "gps only") ? "suspndGpsAlrt" : field;
                                                            val = (value.ToLower() == "yes") ? true : val;
                                                            val = (value.ToLower() == "no") ? true : val;
                                                            val = (value.ToLower() == "gps only") ? true : val;
                                                        }
                                                        FltAnd &= Bld.Eq<bool>(field, val);
                                                    }
                                                    else if (field == "rssi")
                                                    {
                                                        FltAnd &= Bld.Eq<double>(field, Convert.ToDouble(value.ToLower()));
                                                    }
                                                    else if (field == "btrLvl" || field == "btry")
                                                    {
                                                        if (value.ToLower() == "low")
                                                        {
                                                            FltAnd &= Bld.Lte<int>(field, Convert.ToInt32("97"));
                                                        }
                                                        else if (value.ToLower() == "normal")
                                                        {
                                                            FltAnd &= Bld.Gte<int>(field, Convert.ToInt32("97"));
                                                        }
                                                        else
                                                        {
                                                            FltAnd &= Bld.Eq<int>(field, Convert.ToInt32(value.ToLower()));
                                                        }
                                                    }
                                                    else if (field == "picNotRcvMin")
                                                    {
                                                        if (value.ToLower() == "exist")
                                                        {
                                                            FltAnd &= Bld.Not(Bld.Eq<byte[]>("pic", null));
                                                        }
                                                        else if (value.ToLower() == "not exist")
                                                        {
                                                            FltAnd &= Bld.Eq<byte[]>("pic", null);
                                                        }
                                                    }
                                                    else if (field == "tagId" || field == "gtwId")
                                                    {
                                                        string[] vals = value.Split('-');
                                                        string val = (vals.Length > 1) ? Convert.ToInt64(vals[2]).ToString() : value;
                                                        FltAnd &= Bld.Eq<string>(field, val);
                                                    }
                                                    //else if (field == "evTypeName")
                                                    //{
                                                    //    string[] vals = value.Split('-');
                                                    //    string val = (vals.Length > 0) ? vals[0] : value;
                                                    //    FltAnd &= Bld.Eq<string>("evType", val) | Bld.Eq<string>("evTypeId", val);
                                                    //}
                                                    else if (field == "mstMblNo")
                                                    {
                                                        string val = value.Substring(2, value.Length - 2);
                                                        FltAnd &= Bld.Eq<string>(field, val) | Bld.Eq<string>(field, value);
                                                    }
                                                    else
                                                    {
                                                        FltAnd &= Bld.Eq<string>(field, value);
                                                    }
                                                }
                                            }
                                        }
                                        #endregion
                                        and = true;
                                    }
                                    else
                                    {
                                        #region "Or"
                                        for (int a = 0; a < poCompositeFIlter.FilterDescriptors.Count; a++)
                                        {
                                            if (poCompositeFIlter.FilterDescriptors[a].GetType().ToString() == "Kendo.Mvc.CompositeFilterDescriptor")
                                            {
                                                Kendo.Mvc.CompositeFilterDescriptor poFIlter = poCompositeFIlter.FilterDescriptors[a] as Kendo.Mvc.CompositeFilterDescriptor;
                                                if (poFIlter.LogicalOperator.ToString() == "And")
                                                {
                                                    #region And
                                                    foreach (Kendo.Mvc.FilterDescriptor doFilter in poFIlter.FilterDescriptors)
                                                    {
                                                        string field = doFilter.Member;
                                                        if (field == "ts" || field == "tagTs" || field == "crtOn" || field == "picRcv")
                                                        {
                                                            string Opr = doFilter.Operator.ToString();
                                                            string value = doFilter.Value.ToString();

                                                            if (Opr == "IsGreaterThanOrEqualTo")
                                                            {
                                                                FltAnd &= Bld.Gte<DateTime>(field, DateTime.Parse(value));
                                                            }
                                                            else if (Opr == "IsLessThanOrEqualTo")
                                                            {
                                                                FltAnd &= Bld.Lte<DateTime>(field, DateTime.Parse(value));
                                                            }
                                                        }
                                                        else if (field == "dateStart" || field == "dateEnd" || field == "dob")
                                                        {
                                                            string Opr = doFilter.Operator.ToString();
                                                            string value = doFilter.Value.ToString();
                                                            var voValue = DateTime.Parse(value).Date;
                                                            if (Opr == "IsGreaterThanOrEqualTo")
                                                            {
                                                                FltAnd &= Bld.Gte<DateTime>(field, voValue);
                                                            }
                                                            else if (Opr == "IsLessThanOrEqualTo")
                                                            {
                                                                FltAnd &= Bld.Lte<DateTime>(field, voValue.AddHours(23));
                                                            }
                                                        }
                                                        else
                                                        {
                                                            string Opr = doFilter.Operator.ToString();
                                                            Opr = (Opr == "IsEqualTo") ? "$eq" : Opr;
                                                            string value = doFilter.Value.ToString();
                                                            if (field == "tmprd" || field == "temprd" || field == "procs" || field == "gtwMtn" || field == "rawData" || field == "ack" || field == "notify" || field == "act" || field == "suspend" || field == "trueTemprd" || field == "crntAlt" || field == "crntData")
                                                            {
                                                                bool val = (value.ToLower() == "yes") ? true : false;
                                                                field = (field == "act") ? "shnAct" : field;
                                                                if (field == "suspend")
                                                                {
                                                                    field = (value.ToLower() == "yes") ? "suspndAlrt" : field;
                                                                    field = (value.ToLower() == "no") ? "suspndNoAlrt" : field;
                                                                    field = (value.ToLower() == "gps only") ? "suspndGpsAlrt" : field;
                                                                    val = (value.ToLower() == "yes") ? true : val;
                                                                    val = (value.ToLower() == "no") ? true : val;
                                                                    val = (value.ToLower() == "gps only") ? true : val;
                                                                }
                                                                FltAnd &= Bld.Eq<bool>(field, val);
                                                            }
                                                            else if (field == "rssi")
                                                            {
                                                                FltAnd &= Bld.Eq<double>(field, Convert.ToDouble(value.ToLower()));
                                                            }
                                                            else if (field == "btrLvl" || field == "btry")
                                                            {
                                                                if (value.ToLower() == "low")
                                                                {
                                                                    FltAnd &= Bld.Lte<int>(field, Convert.ToInt32("97"));
                                                                }
                                                                else if (value.ToLower() == "normal")
                                                                {
                                                                    FltAnd &= Bld.Gte<int>(field, Convert.ToInt32("97"));
                                                                }
                                                                else
                                                                {
                                                                    FltAnd &= Bld.Eq<int>(field, Convert.ToInt32(value.ToLower()));
                                                                }
                                                            }
                                                            else if (field == "picNotRcvMin")
                                                            {
                                                                if (value.ToLower() == "exist")
                                                                {
                                                                    FltAnd &= Bld.Not(Bld.Eq<byte[]>("pic", null));
                                                                }
                                                                else if (value.ToLower() == "not exist")
                                                                {
                                                                    FltAnd &= Bld.Eq<byte[]>("pic", null);
                                                                }
                                                            }
                                                            else if (field == "tagId" || field == "gtwId")
                                                            {
                                                                string[] vals = value.Split('-');
                                                                string val = (vals.Length > 1) ? Convert.ToInt64(vals[2]).ToString() : value;
                                                                FltAnd &= Bld.Eq<string>(field, val);
                                                            }
                                                            //else if (field == "evTypeName")
                                                            //{
                                                            //    string[] vals = value.Split('-');
                                                            //    string val = (vals.Length > 0) ? vals[0] : value;
                                                            //    FltAnd &= Bld.Eq<string>("evType", val) | Bld.Eq<string>("evTypeId", val);
                                                            //}
                                                            else if (field == "mstMblNo")
                                                            {
                                                                string val = value.Substring(2, value.Length - 2);
                                                                FltAnd &= Bld.Eq<string>(field, val) | Bld.Eq<string>(field, value);
                                                            }
                                                            else
                                                            {
                                                                FltAnd &= Bld.Eq<string>(field, value);
                                                            }
                                                        }
                                                    }
                                                    #endregion
                                                    and = true;
                                                }
                                                else
                                                {
                                                    #region Or
                                                    for (int b = 0; b < poFIlter.FilterDescriptors.Count; b++)
                                                    {
                                                        if (poFIlter.FilterDescriptors[b].GetType().ToString() == "Kendo.Mvc.CompositeFilterDescriptor")
                                                        {
                                                            Kendo.Mvc.CompositeFilterDescriptor paFIlter = poFIlter.FilterDescriptors[a] as Kendo.Mvc.CompositeFilterDescriptor;
                                                            foreach (Kendo.Mvc.FilterDescriptor doFilter in paFIlter.FilterDescriptors)
                                                            {
                                                                string field = doFilter.Member;
                                                                if (field == "ts" || field == "tagTs" || field == "crtOn" || field == "picRcv")
                                                                {
                                                                    string Opr = doFilter.Operator.ToString();
                                                                    string value = doFilter.Value.ToString();

                                                                    if (Opr == "IsGreaterThanOrEqualTo")
                                                                    {
                                                                        FltAnd &= Bld.Gte<DateTime>(field, DateTime.Parse(value));
                                                                    }
                                                                    else if (Opr == "IsLessThanOrEqualTo")
                                                                    {
                                                                        FltAnd &= Bld.Lte<DateTime>(field, DateTime.Parse(value));
                                                                    }
                                                                }
                                                                else if (field == "dateStart" || field == "dateEnd" || field == "dob")
                                                                {
                                                                    string Opr = doFilter.Operator.ToString();
                                                                    string value = doFilter.Value.ToString();
                                                                    var voValue = DateTime.Parse(value).Date;
                                                                    if (Opr == "IsGreaterThanOrEqualTo")
                                                                    {
                                                                        FltAnd &= Bld.Gte<DateTime>(field, voValue);
                                                                    }
                                                                    else if (Opr == "IsLessThanOrEqualTo")
                                                                    {
                                                                        FltAnd &= Bld.Lte<DateTime>(field, voValue.AddHours(23));
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    string Opr = doFilter.Operator.ToString();
                                                                    Opr = (Opr == "IsEqualTo") ? "$eq" : Opr;
                                                                    string value = doFilter.Value.ToString();
                                                                    if (field == "tmprd" || field == "temprd" || field == "procs" || field == "gtwMtn" || field == "rawData" || field == "ack" || field == "notify" || field == "act" || field == "suspend" || field == "trueTemprd" || field == "crntAlt" || field == "crntData")
                                                                    {
                                                                        bool val = (value.ToLower() == "yes") ? true : false;
                                                                        field = (field == "act") ? "shnAct" : field;
                                                                        if (field == "suspend")
                                                                        {
                                                                            field = (value.ToLower() == "yes") ? "suspndAlrt" : field;
                                                                            field = (value.ToLower() == "no") ? "suspndNoAlrt" : field;
                                                                            field = (value.ToLower() == "gps only") ? "suspndGpsAlrt" : field;
                                                                            val = (value.ToLower() == "yes") ? true : val;
                                                                            val = (value.ToLower() == "no") ? true : val;
                                                                            val = (value.ToLower() == "gps only") ? true : val;
                                                                        }
                                                                        FltOr |= Bld.Eq<bool>(field, val);
                                                                    }
                                                                    else if (field == "rssi")
                                                                    {
                                                                        FltOr |= Bld.Eq<double>(field, Convert.ToDouble(value.ToLower()));
                                                                    }
                                                                    else if (field == "btrLvl" || field == "btry")
                                                                    {
                                                                        if (value.ToLower() == "low")
                                                                        {
                                                                            FltOr |= Bld.Lte<int>(field, Convert.ToInt32("97"));
                                                                        }
                                                                        else if (value.ToLower() == "normal")
                                                                        {
                                                                            FltOr |= Bld.Gte<int>(field, Convert.ToInt32("97"));
                                                                        }
                                                                        else
                                                                        {
                                                                            FltOr |= Bld.Eq<int>(field, Convert.ToInt32(value.ToLower()));
                                                                        }
                                                                    }
                                                                    else if (field == "picNotRcvMin")
                                                                    {
                                                                        if (value.ToLower() == "exist")
                                                                        {
                                                                            FltOr |= Bld.Not(Bld.Eq<byte[]>("pic", null));
                                                                        }
                                                                        else if (value.ToLower() == "not exist")
                                                                        {
                                                                            FltOr |= Bld.Eq<byte[]>("pic", null);
                                                                        }
                                                                    }
                                                                    else if (field == "tagId" || field == "gtwId")
                                                                    {
                                                                        string[] vals = value.Split('-');
                                                                        string val = (vals.Length > 1) ? Convert.ToInt64(vals[2]).ToString() : value;
                                                                        FltOr |= Bld.Eq<string>(field, val);
                                                                    }
                                                                    //else if (field == "evTypeName")
                                                                    //{
                                                                    //    string[] vals = value.Split('-');
                                                                    //    string val = (vals.Length > 0) ? vals[0] : value;
                                                                    //    FltOr |= Bld.Eq<string>("evType", val) | Bld.Eq<string>("evTypeId", val);
                                                                    //}
                                                                    else if (field == "mstMblNo")
                                                                    {
                                                                        string val = value.Substring(2, value.Length - 2);
                                                                        FltOr |= Bld.Eq<string>(field, val) | Bld.Eq<string>(field, value);
                                                                    }
                                                                    else
                                                                    {
                                                                        FltOr |= Bld.Eq<string>(field, value);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            Kendo.Mvc.FilterDescriptor doFilter = poFIlter.FilterDescriptors[b] as Kendo.Mvc.FilterDescriptor;
                                                            string field = doFilter.Member;
                                                            if (field == "ts" || field == "tagTs" || field == "crtOn" || field == "picRcv")
                                                            {
                                                                string Opr = doFilter.Operator.ToString();
                                                                string value = doFilter.Value.ToString();

                                                                if (Opr == "IsGreaterThanOrEqualTo")
                                                                {
                                                                    FltAnd &= Bld.Gte<DateTime>(field, DateTime.Parse(value));
                                                                }
                                                                else if (Opr == "IsLessThanOrEqualTo")
                                                                {
                                                                    FltAnd &= Bld.Lte<DateTime>(field, DateTime.Parse(value));
                                                                }
                                                            }
                                                            else if (field == "dateStart" || field == "dateEnd" || field == "dob")
                                                            {
                                                                string Opr = doFilter.Operator.ToString();
                                                                string value = doFilter.Value.ToString();
                                                                var voValue = DateTime.Parse(value).Date;
                                                                if (Opr == "IsGreaterThanOrEqualTo")
                                                                {
                                                                    FltAnd &= Bld.Gte<DateTime>(field, voValue);
                                                                }
                                                                else if (Opr == "IsLessThanOrEqualTo")
                                                                {
                                                                    FltAnd &= Bld.Lte<DateTime>(field, voValue.AddHours(23));
                                                                }
                                                            }
                                                            else
                                                            {
                                                                string Opr = doFilter.Operator.ToString();
                                                                Opr = (Opr == "IsEqualTo") ? "$eq" : Opr;
                                                                string value = doFilter.Value.ToString();
                                                                if (field == "tmprd" || field == "temprd" || field == "procs" || field == "gtwMtn" || field == "rawData" || field == "ack" || field == "notify" || field == "act" || field == "suspend" || field == "trueTemprd" || field == "crntAlt" || field == "crntData")
                                                                {
                                                                    bool val = (value.ToLower() == "yes") ? true : false;
                                                                    field = (field == "act") ? "shnAct" : field;
                                                                    if (field == "suspend")
                                                                    {
                                                                        field = (value.ToLower() == "yes") ? "suspndAlrt" : field;
                                                                        field = (value.ToLower() == "no") ? "suspndNoAlrt" : field;
                                                                        field = (value.ToLower() == "gps only") ? "suspndGpsAlrt" : field;
                                                                        val = (value.ToLower() == "yes") ? true : val;
                                                                        val = (value.ToLower() == "no") ? true : val;
                                                                        val = (value.ToLower() == "gps only") ? true : val;
                                                                    }
                                                                    FltOr |= Bld.Eq<bool>(field, val);
                                                                }
                                                                else if (field == "rssi")
                                                                {
                                                                    FltOr |= Bld.Eq<double>(field, Convert.ToDouble(value.ToLower()));
                                                                }
                                                                else if (field == "btrLvl" || field == "btry")
                                                                {
                                                                    if (value.ToLower() == "low")
                                                                    {
                                                                        FltOr |= Bld.Lte<int>(field, Convert.ToInt32("97"));
                                                                    }
                                                                    else if (value.ToLower() == "normal")
                                                                    {
                                                                        FltOr |= Bld.Gte<int>(field, Convert.ToInt32("97"));
                                                                    }
                                                                    else
                                                                    {
                                                                        FltOr |= Bld.Eq<int>(field, Convert.ToInt32(value.ToLower()));
                                                                    }
                                                                }
                                                                else if (field == "picNotRcvMin")
                                                                {
                                                                    if (value.ToLower() == "exist")
                                                                    {
                                                                        FltOr |= Bld.Not(Bld.Eq<byte[]>("pic", null));
                                                                    }
                                                                    else if (value.ToLower() == "not exist")
                                                                    {
                                                                        FltOr |= Bld.Eq<byte[]>("pic", null);
                                                                    }
                                                                }
                                                                else if (field == "tagId" || field == "gtwId")
                                                                {
                                                                    string[] vals = value.Split('-');
                                                                    string val = (vals.Length > 1) ? Convert.ToInt64(vals[2]).ToString() : value;
                                                                    FltOr |= Bld.Eq<string>(field, val);
                                                                }
                                                                //else if (field == "evTypeName")
                                                                //{
                                                                //    string[] vals = value.Split('-');
                                                                //    string val = (vals.Length > 0) ? vals[0] : value;
                                                                //    FltOr |= Bld.Eq<string>("evType", val) | Bld.Eq<string>("evTypeId", val);
                                                                //}
                                                                else if (field == "mstMblNo")
                                                                {
                                                                    string val = value.Substring(2, value.Length - 2);
                                                                    FltOr |= Bld.Eq<string>(field, val) | Bld.Eq<string>(field, value);
                                                                }
                                                                else
                                                                {
                                                                    FltOr |= Bld.Eq<string>(field, value);
                                                                }
                                                            }
                                                        }

                                                    }
                                                    #endregion
                                                    or = true;
                                                }
                                            }
                                            else
                                            {
                                                Kendo.Mvc.FilterDescriptor poFIlter = poCompositeFIlter.FilterDescriptors[a] as Kendo.Mvc.FilterDescriptor;
                                                string field = poFIlter.Member;
                                                if (field == "ts" || field == "tagTs" || field == "crtOn" || field == "picRcv")
                                                {
                                                    string Opr = poFIlter.Operator.ToString();
                                                    string value = poFIlter.Value.ToString();

                                                    if (Opr == "IsGreaterThanOrEqualTo")
                                                    {
                                                        FltAnd &= Bld.Gte<DateTime>(field, DateTime.Parse(value));
                                                    }
                                                    else if (Opr == "IsLessThanOrEqualTo")
                                                    {
                                                        FltAnd &= Bld.Lte<DateTime>(field, DateTime.Parse(value));
                                                    }
                                                }
                                                else if (field == "dateStart" || field == "dateEnd" || field == "dob")
                                                {
                                                    string Opr = poFIlter.Operator.ToString();
                                                    string value = poFIlter.Value.ToString();
                                                    var voValue = DateTime.Parse(value).Date;
                                                    if (Opr == "IsGreaterThanOrEqualTo")
                                                    {
                                                        FltAnd &= Bld.Gte<DateTime>(field, voValue);
                                                    }
                                                    else if (Opr == "IsLessThanOrEqualTo")
                                                    {
                                                        FltAnd &= Bld.Lte<DateTime>(field, voValue.AddHours(23));
                                                    }
                                                }
                                                else
                                                {
                                                    string Opr = poFIlter.Operator.ToString();
                                                    Opr = (Opr == "IsEqualTo") ? "$eq" : Opr;
                                                    string value = poFIlter.Value.ToString();
                                                    if (field == "tmprd" || field == "temprd" || field == "procs" || field == "gtwMtn" || field == "rawData" || field == "ack" || field == "notify" || field == "act" || field == "suspend" || field == "trueTemprd" || field == "crntAlt" || field == "crntData")
                                                    {
                                                        bool val = (value.ToLower() == "yes") ? true : false;
                                                        field = (field == "act") ? "shnAct" : field;
                                                        if (field == "suspend")
                                                        {
                                                            field = (value.ToLower() == "yes") ? "suspndAlrt" : field;
                                                            field = (value.ToLower() == "no") ? "suspndNoAlrt" : field;
                                                            field = (value.ToLower() == "gps only") ? "suspndGpsAlrt" : field;
                                                            val = (value.ToLower() == "yes") ? true : val;
                                                            val = (value.ToLower() == "no") ? true : val;
                                                            val = (value.ToLower() == "gps only") ? true : val;
                                                        }
                                                        FltOr |= Bld.Eq<bool>(field, val);
                                                    }
                                                    else if (field == "rssi")
                                                    {
                                                        FltOr |= Bld.Eq<double>(field, Convert.ToDouble(value.ToLower()));
                                                    }
                                                    else if (field == "btrLvl" || field == "btry")
                                                    {
                                                        if (value.ToLower() == "low")
                                                        {
                                                            FltOr |= Bld.Lte<int>(field, Convert.ToInt32("97"));
                                                        }
                                                        else if (value.ToLower() == "normal")
                                                        {
                                                            FltOr |= Bld.Gte<int>(field, Convert.ToInt32("97"));
                                                        }
                                                        else
                                                        {
                                                            FltOr |= Bld.Eq<int>(field, Convert.ToInt32(value.ToLower()));
                                                        }
                                                    }
                                                    else if (field == "picNotRcvMin")
                                                    {
                                                        if (value.ToLower() == "exist")
                                                        {
                                                            FltOr |= Bld.Not(Bld.Eq<byte[]>("pic", null));
                                                        }
                                                        else if (value.ToLower() == "not exist")
                                                        {
                                                            FltOr |= Bld.Eq<byte[]>("pic", null);
                                                        }
                                                    }
                                                    else if (field == "tagId" || field == "gtwId")
                                                    {
                                                        string[] vals = value.Split('-');
                                                        string val = (vals.Length > 1) ? Convert.ToInt64(vals[2]).ToString() : value;
                                                        FltOr |= Bld.Eq<string>(field, val);
                                                    }
                                                    //else if (field == "evTypeName")
                                                    //{
                                                    //    string[] vals = value.Split('-');
                                                    //    string val = (vals.Length > 0) ? vals[0] : value;
                                                    //    FltOr |= Bld.Eq<string>("evType", val) | Bld.Eq<string>("evTypeId", val);
                                                    //}
                                                    else if (field == "mstMblNo")
                                                    {
                                                        string val = value.Substring(2, value.Length - 2);
                                                        FltOr |= Bld.Eq<string>(field, val) | Bld.Eq<string>(field, value);
                                                    }
                                                    else
                                                    {
                                                        FltOr |= Bld.Eq<string>(field, value);
                                                    }
                                                }
                                            }
                                        }
                                        #endregion
                                        or = true;
                                    }
                                }
                                else
                                {
                                    Kendo.Mvc.FilterDescriptor poFIlter = voFIlter.FilterDescriptors[i] as Kendo.Mvc.FilterDescriptor;
                                    string field = poFIlter.Member;
                                    if (field == "ts" || field == "tagTs" || field == "crtOn" || field == "picRcv")
                                    {
                                        if (poFIlter.Value != null)
                                        {
                                            string Opr = poFIlter.Operator.ToString();
                                            string value = poFIlter.Value.ToString();

                                            if (Opr == "IsGreaterThanOrEqualTo")
                                            {
                                                FltAnd &= Bld.Gte<DateTime>(field, DateTime.Parse(value));
                                            }
                                            else if (Opr == "IsLessThanOrEqualTo")
                                            {
                                                FltAnd &= Bld.Lte<DateTime>(field, DateTime.Parse(value));
                                            }
                                        }
                                    }
                                    else if (field == "dateStart" || field == "dateEnd" || field == "dob")
                                    {
                                        string Opr = poFIlter.Operator.ToString();
                                        string value = poFIlter.Value.ToString();
                                        var voValue = DateTime.Parse(value).Date;
                                        if (Opr == "IsGreaterThanOrEqualTo")
                                        {
                                            FltAnd &= Bld.Gte<DateTime>(field, voValue);
                                        }
                                        else if (Opr == "IsLessThanOrEqualTo")
                                        {
                                            FltAnd &= Bld.Lte<DateTime>(field, voValue.AddHours(23));
                                        }
                                    }
                                    else
                                    {
                                        string Opr = poFIlter.Operator.ToString();
                                        Opr = (Opr == "IsEqualTo") ? "$eq" : Opr;
                                        string value = poFIlter.Value.ToString();
                                        if (field == "tmprd" || field == "temprd" || field == "procs" || field == "gtwMtn" || field == "rawData" || field == "ack" || field == "notify" || field == "act" || field == "suspend" || field == "trueTemprd" || field == "crntAlt" || field == "crntData")
                                        {
                                            bool val = (value.ToLower() == "yes") ? true : false;
                                            field = (field == "act") ? "shnAct" : field;
                                            if (field == "suspend")
                                            {
                                                field = (value.ToLower() == "yes") ? "suspndAlrt" : field;
                                                field = (value.ToLower() == "no") ? "suspndNoAlrt" : field;
                                                field = (value.ToLower() == "gps only") ? "suspndGpsAlrt" : field;
                                                val = (value.ToLower() == "yes") ? true : val;
                                                val = (value.ToLower() == "no") ? true : val;
                                                val = (value.ToLower() == "gps only") ? true : val;
                                            }
                                            FltAnd &= Bld.Eq<bool>(field, val);
                                        }
                                        else if (field == "rssi")
                                        {
                                            FltAnd &= Bld.Eq<double>(field, Convert.ToDouble(value.ToLower()));
                                        }
                                        else if (field == "btrLvl" || field == "btry")
                                        {
                                            if (value.ToLower() == "low")
                                            {
                                                FltAnd &= Bld.Lte<int>(field, Convert.ToInt32("97"));
                                            }
                                            else if (value.ToLower() == "normal")
                                            {
                                                FltAnd &= Bld.Gte<int>(field, Convert.ToInt32("97"));
                                            }
                                            else
                                            {
                                                FltAnd &= Bld.Eq<int>(field, Convert.ToInt32(value.ToLower()));
                                            }
                                        }
                                        else if (field == "picNotRcvMin")
                                        {
                                            if (value.ToLower() == "exist")
                                            {
                                                FltAnd &= Bld.Not(Bld.Eq<byte[]>("pic", null));
                                            }
                                            else if (value.ToLower() == "not exist")
                                            {
                                                FltAnd &= Bld.Eq<byte[]>("pic", null);
                                            }
                                        }
                                        else if (field == "tagId" || field == "gtwId")
                                        {
                                            string[] vals = value.Split('-');
                                            string val = (vals.Length > 1) ? Convert.ToInt64(vals[2]).ToString() : value;
                                            FltAnd &= Bld.Eq<string>(field, val);
                                        }
                                        //else if (field == "evTypeName")
                                        //{
                                        //    string[] vals = value.Split('-');
                                        //    string val = (vals.Length > 0) ? vals[0] : value;
                                        //    FltAnd &= Bld.Eq<string>("evType", val) | Bld.Eq<string>("evTypeId", val);
                                        //}
                                        else if (field == "mstMblNo")
                                        {
                                            string val = value.Substring(2, value.Length - 2);
                                            FltAnd &= Bld.Eq<string>(field, val) | Bld.Eq<string>(field, value);
                                        }
                                        else
                                        {
                                            FltAnd &= Bld.Eq<string>(field, value);
                                        }
                                    }
                                }
                            }
                            #endregion
                            and = true;
                        }
                        else
                        {
                            #region Or
                            for (int i = 0; i < voFIlter.FilterDescriptors.Count; i++)
                            {
                                if (voFIlter.FilterDescriptors[i].GetType().ToString() == "Kendo.Mvc.CompositeFilterDescriptor")
                                {
                                    Kendo.Mvc.CompositeFilterDescriptor poCompositeFIlter = voFIlter.FilterDescriptors[i] as Kendo.Mvc.CompositeFilterDescriptor;

                                    for (int a = 0; a < poCompositeFIlter.FilterDescriptors.Count; a++)
                                    {
                                        if (poCompositeFIlter.FilterDescriptors[a].GetType().ToString() == "Kendo.Mvc.CompositeFilterDescriptor")
                                        {
                                            Kendo.Mvc.CompositeFilterDescriptor poFIlter = poCompositeFIlter.FilterDescriptors[a] as Kendo.Mvc.CompositeFilterDescriptor;
                                            foreach (Kendo.Mvc.FilterDescriptor doFilter in poFIlter.FilterDescriptors)
                                            {
                                                string field = doFilter.Member;
                                                if (field == "ts" || field == "tagTs" || field == "crtOn" || field == "picRcv")
                                                {
                                                    string Opr = doFilter.Operator.ToString();
                                                    string value = doFilter.Value.ToString();

                                                    if (Opr == "IsGreaterThanOrEqualTo")
                                                    {
                                                        FltAnd &= Bld.Gte<DateTime>(field, DateTime.Parse(value));
                                                    }
                                                    else if (Opr == "IsLessThanOrEqualTo")
                                                    {
                                                        FltAnd &= Bld.Lte<DateTime>(field, DateTime.Parse(value));
                                                    }
                                                }
                                                else if (field == "dateStart" || field == "dateEnd" || field == "dob")
                                                {
                                                    string Opr = doFilter.Operator.ToString();
                                                    string value = doFilter.Value.ToString();
                                                    var voValue = DateTime.Parse(value).Date;
                                                    if (Opr == "IsGreaterThanOrEqualTo")
                                                    {
                                                        FltAnd &= Bld.Gte<DateTime>(field, voValue);
                                                    }
                                                    else if (Opr == "IsLessThanOrEqualTo")
                                                    {
                                                        FltAnd &= Bld.Lte<DateTime>(field, voValue.AddHours(23));
                                                    }
                                                }
                                                else
                                                {
                                                    string Opr = doFilter.Operator.ToString();
                                                    Opr = (Opr == "IsEqualTo") ? "$eq" : Opr;
                                                    string value = doFilter.Value.ToString();

                                                    if (field == "tmprd" || field == "temprd" || field == "procs" || field == "gtwMtn" || field == "rawData" || field == "ack" || field == "notify" || field == "act" || field == "suspend" || field == "trueTemprd" || field == "crntAlt" || field == "crntData")
                                                    {
                                                        bool val = (value.ToLower() == "yes") ? true : false;
                                                        field = (field == "act") ? "shnAct" : field;
                                                        if (field == "suspend")
                                                        {
                                                            field = (value.ToLower() == "yes") ? "suspndAlrt" : field;
                                                            field = (value.ToLower() == "no") ? "suspndNoAlrt" : field;
                                                            field = (value.ToLower() == "gps only") ? "suspndGpsAlrt" : field;
                                                            val = (value.ToLower() == "yes") ? true : val;
                                                            val = (value.ToLower() == "no") ? true : val;
                                                            val = (value.ToLower() == "gps only") ? true : val;
                                                        }
                                                        FltOr |= Bld.Eq<bool>(field, val);
                                                    }
                                                    else if (field == "rssi")
                                                    {
                                                        FltOr |= Bld.Eq<double>(field, Convert.ToDouble(value.ToLower()));
                                                    }
                                                    else if (field == "btrLvl" || field == "btry")
                                                    {
                                                        if (value.ToLower() == "low")
                                                        {
                                                            FltOr |= Bld.Lte<int>(field, Convert.ToInt32("97"));
                                                        }
                                                        else if (value.ToLower() == "normal")
                                                        {
                                                            FltOr |= Bld.Gte<int>(field, Convert.ToInt32("97"));
                                                        }
                                                        else
                                                        {
                                                            FltOr |= Bld.Eq<int>(field, Convert.ToInt32(value.ToLower()));
                                                        }
                                                    }
                                                    else if (field == "picNotRcvMin")
                                                    {
                                                        if (value.ToLower() == "exist")
                                                        {
                                                            FltOr |= Bld.Not(Bld.Eq<byte[]>("pic", null));
                                                        }
                                                        else if (value.ToLower() == "not exist")
                                                        {
                                                            FltOr |= Bld.Eq<byte[]>("pic", null);
                                                        }
                                                    }
                                                    else if (field == "tagId" || field == "gtwId")
                                                    {
                                                        string[] vals = value.Split('-');
                                                        string val = (vals.Length > 1) ? Convert.ToInt64(vals[2]).ToString() : value;
                                                        FltOr |= Bld.Eq<string>(field, val);
                                                    }
                                                    //else if (field == "evTypeName")
                                                    //{
                                                    //    string[] vals = value.Split('-');
                                                    //    string val = (vals.Length > 0) ? vals[0] : value;
                                                    //    FltOr |= Bld.Eq<string>("evType", val) | Bld.Eq<string>("evTypeId", val);
                                                    //}
                                                    else if (field == "mstMblNo")
                                                    {
                                                        string val = value.Substring(2, value.Length - 2);
                                                        FltOr |= Bld.Eq<string>(field, val) | Bld.Eq<string>(field, value);
                                                    }
                                                    else
                                                    {
                                                        FltOr |= Bld.Eq<string>(field, value);
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            Kendo.Mvc.FilterDescriptor poFIlter = poCompositeFIlter.FilterDescriptors[a] as Kendo.Mvc.FilterDescriptor;
                                            string field = poFIlter.Member;
                                            if (field == "ts" || field == "tagTs" || field == "crtOn" || field == "picRcv")
                                            {
                                                string Opr = poFIlter.Operator.ToString();
                                                string value = poFIlter.Value.ToString();

                                                if (Opr == "IsGreaterThanOrEqualTo")
                                                {
                                                    FltAnd &= Bld.Gte<DateTime>(field, DateTime.Parse(value));
                                                }
                                                else if (Opr == "IsLessThanOrEqualTo")
                                                {
                                                    FltAnd &= Bld.Lte<DateTime>(field, DateTime.Parse(value));
                                                }
                                            }
                                            else if (field == "dateStart" || field == "dateEnd" || field == "dob")
                                            {
                                                string Opr = poFIlter.Operator.ToString();
                                                string value = poFIlter.Value.ToString();
                                                var voValue = DateTime.Parse(value).Date;
                                                if (Opr == "IsGreaterThanOrEqualTo")
                                                {
                                                    FltAnd &= Bld.Gte<DateTime>(field, voValue);
                                                }
                                                else if (Opr == "IsLessThanOrEqualTo")
                                                {
                                                    FltAnd &= Bld.Lte<DateTime>(field, voValue.AddHours(23));
                                                }
                                            }
                                            else
                                            {
                                                string Opr = poFIlter.Operator.ToString();
                                                Opr = (Opr == "IsEqualTo") ? "$eq" : Opr;
                                                string value = poFIlter.Value.ToString();
                                                if (field == "tmprd" || field == "temprd" || field == "procs" || field == "gtwMtn" || field == "rawData" || field == "ack" || field == "notify" || field == "act" || field == "suspend" || field == "trueTemprd" || field == "crntAlt" || field == "crntData")
                                                {
                                                    bool val = (value.ToLower() == "yes") ? true : false;
                                                    field = (field == "act") ? "shnAct" : field;
                                                    if (field == "suspend")
                                                    {
                                                        field = (value.ToLower() == "yes") ? "suspndAlrt" : field;
                                                        field = (value.ToLower() == "no") ? "suspndNoAlrt" : field;
                                                        field = (value.ToLower() == "gps only") ? "suspndGpsAlrt" : field;
                                                        val = (value.ToLower() == "yes") ? true : val;
                                                        val = (value.ToLower() == "no") ? true : val;
                                                        val = (value.ToLower() == "gps only") ? true : val;
                                                    }
                                                    FltOr |= Bld.Eq<bool>(field, val);
                                                }
                                                else if (field == "rssi")
                                                {
                                                    FltOr |= Bld.Eq<double>(field, Convert.ToDouble(value.ToLower()));
                                                }
                                                else if (field == "btrLvl" || field == "btry")
                                                {
                                                    if (value.ToLower() == "low")
                                                    {
                                                        FltOr |= Bld.Lte<int>(field, Convert.ToInt32("97"));
                                                    }
                                                    else if (value.ToLower() == "normal")
                                                    {
                                                        FltOr |= Bld.Gte<int>(field, Convert.ToInt32("97"));
                                                    }
                                                    else
                                                    {
                                                        FltOr |= Bld.Eq<int>(field, Convert.ToInt32(value.ToLower()));
                                                    }
                                                }
                                                else if (field == "picNotRcvMin")
                                                {
                                                    if (value.ToLower() == "exist")
                                                    {
                                                        FltOr |= Bld.Not(Bld.Eq<byte[]>("pic", null));
                                                    }
                                                    else if (value.ToLower() == "not exist")
                                                    {
                                                        FltOr |= Bld.Eq<byte[]>("pic", null);
                                                    }
                                                }
                                                else if (field == "tagId" || field == "gtwId")
                                                {
                                                    string[] vals = value.Split('-');
                                                    string val = (vals.Length > 1) ? Convert.ToInt64(vals[2]).ToString() : value;
                                                    FltOr |= Bld.Eq<string>(field, val);
                                                }
                                                //else if (field == "evTypeName")
                                                //{
                                                //    string[] vals = value.Split('-');
                                                //    string val = (vals.Length > 0) ? vals[0] : value;
                                                //    FltOr |= Bld.Eq<string>("evType", val) | Bld.Eq<string>("evTypeId", val);
                                                //}
                                                else if (field == "mstMblNo")
                                                {
                                                    string val = value.Substring(2, value.Length - 2);
                                                    FltOr |= Bld.Eq<string>(field, val) | Bld.Eq<string>(field, value);
                                                }
                                                else
                                                {
                                                    FltOr |= Bld.Eq<string>(field, value);
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    Kendo.Mvc.FilterDescriptor poFIlter = voFIlter.FilterDescriptors[i] as Kendo.Mvc.FilterDescriptor;
                                    string field = poFIlter.Member;
                                    if (field == "ts" || field == "tagTs" || field == "crtOn" || field == "picRcv")
                                    {
                                        string Opr = poFIlter.Operator.ToString();
                                        string value = poFIlter.Value.ToString();

                                        if (Opr == "IsGreaterThanOrEqualTo")
                                        {
                                            FltOr |= Bld.Gte<DateTime>(field, DateTime.Parse(value));
                                        }
                                        else if (Opr == "IsLessThanOrEqualTo")
                                        {
                                            FltOr |= Bld.Lte<DateTime>(field, DateTime.Parse(value));
                                        }
                                    }
                                    else if (field == "dateStart" || field == "dateEnd" || field == "dob")
                                    {
                                        string Opr = poFIlter.Operator.ToString();
                                        string value = poFIlter.Value.ToString();
                                        var voValue = DateTime.Parse(value).Date;
                                        if (Opr == "IsGreaterThanOrEqualTo")
                                        {
                                            FltOr |= Bld.Gte<DateTime>(field, voValue);
                                        }
                                        else if (Opr == "IsLessThanOrEqualTo")
                                        {
                                            FltOr |= Bld.Lte<DateTime>(field, voValue.AddHours(23));
                                        }
                                    }
                                    else
                                    {
                                        string Opr = poFIlter.Operator.ToString();
                                        Opr = (Opr == "IsEqualTo") ? "$eq" : Opr;
                                        string value = poFIlter.Value.ToString();

                                        if (field == "tmprd" || field == "temprd" || field == "procs" || field == "gtwMtn" || field == "rawData" || field == "ack" || field == "notify" || field == "act" || field == "suspend" || field == "trueTemprd" || field == "crntAlt" || field == "crntData")
                                        {
                                            bool val = (value.ToLower() == "yes") ? true : false;
                                            field = (field == "act") ? "shnAct" : field;
                                            if (field == "suspend")
                                            {
                                                field = (value.ToLower() == "yes") ? "suspndAlrt" : field;
                                                field = (value.ToLower() == "no") ? "suspndNoAlrt" : field;
                                                field = (value.ToLower() == "gps only") ? "suspndGpsAlrt" : field;
                                                val = (value.ToLower() == "yes") ? true : val;
                                                val = (value.ToLower() == "no") ? true : val;
                                                val = (value.ToLower() == "gps only") ? true : val;
                                            }
                                            FltOr |= Bld.Eq<bool>(field, val);
                                        }
                                        else if (field == "rssi")
                                        {
                                            FltOr |= Bld.Eq<double>(field, Convert.ToDouble(value.ToLower()));
                                        }
                                        else if (field == "btrLvl" || field == "btry")
                                        {
                                            if (value.ToLower() == "low")
                                            {
                                                FltOr |= Bld.Lte<int>(field, Convert.ToInt32("97"));
                                            }
                                            else if (value.ToLower() == "normal")
                                            {
                                                FltOr |= Bld.Gte<int>(field, Convert.ToInt32("97"));
                                            }
                                            else
                                            {
                                                FltOr |= Bld.Eq<int>(field, Convert.ToInt32(value.ToLower()));
                                            }
                                        }
                                        else if (field == "picNotRcvMin")
                                        {
                                            if (value.ToLower() == "exist")
                                            {
                                                FltOr |= Bld.Not(Bld.Eq<byte[]>("pic", null));
                                            }
                                            else if (value.ToLower() == "not exist")
                                            {
                                                FltOr |= Bld.Eq<byte[]>("pic", null);
                                            }
                                        }
                                        else if (field == "tagId" || field == "gtwId")
                                        {
                                            string[] vals = value.Split('-');
                                            string val = (vals.Length > 1) ? Convert.ToInt64(vals[2]).ToString() : value;
                                            FltOr |= Bld.Eq<string>(field, val);
                                        }
                                        //else if (field == "evTypeName")
                                        //{
                                        //    string[] vals = value.Split('-');
                                        //    string val = (vals.Length > 0) ? vals[0] : value;
                                        //    FltOr |= Bld.Eq<string>("evType", val) | Bld.Eq<string>("evTypeId", val);
                                        //}
                                        else if (field == "mstMblNo")
                                        {
                                            string val = value.Substring(2, value.Length - 2);
                                            FltOr |= Bld.Eq<string>(field, val) | Bld.Eq<string>(field, value);
                                        }
                                        else
                                        {
                                            FltOr |= Bld.Eq<string>(field, value);
                                        }
                                    }
                                }
                            }
                            #endregion
                            or = true;
                        }
                    }
                }
                else
                {
                    foreach (Kendo.Mvc.FilterDescriptor poFIlter in poRequest.Filters)
                    {
                        string field = poFIlter.Member;
                        string Opr = poFIlter.Operator.ToString();
                        Opr = (Opr == "IsEqualTo") ? "$eq" : Opr;
                        string value = poFIlter.Value.ToString();
                        if (field == "tmprd" || field == "temprd" || field == "procs" || field == "gtwMtn" || field == "rawData" || field == "ack" || field == "notify" || field == "act" || field == "suspend" || field == "trueTemprd" || field == "crntAlt" || field == "crntData")
                        {
                            bool val = (value.ToLower() == "yes") ? true : false;
                            field = (field == "act") ? "shnAct" : field;
                            if (field == "suspend")
                            {
                                field = (value.ToLower() == "yes") ? "suspndAlrt" : field;
                                field = (value.ToLower() == "no") ? "suspndNoAlrt" : field;
                                field = (value.ToLower() == "gps only") ? "suspndGpsAlrt" : field;
                                val = (value.ToLower() == "yes") ? true : val;
                                val = (value.ToLower() == "no") ? true : val;
                                val = (value.ToLower() == "gps only") ? true : val;
                            }
                            FltAnd &= Bld.Eq<bool>(field, val);
                        }
                        else if (field == "rssi")
                        {
                            FltAnd &= Bld.Eq<double>(field, Convert.ToDouble(value.ToLower()));
                        }
                        else if (field == "btrLvl" || field == "btry")
                        {
                            if (value.ToLower() == "low")
                            {
                                FltAnd &= Bld.Lte<int>(field, Convert.ToInt32("97"));
                            }
                            else if (value.ToLower() == "normal")
                            {
                                FltAnd &= Bld.Gte<int>(field, Convert.ToInt32("97"));
                            }
                            else
                            {
                                FltAnd &= Bld.Eq<int>(field, Convert.ToInt32(value.ToLower()));
                            }
                        }
                        else if (field == "picNotRcvMin")
                        {
                            if (value.ToLower() == "exist")
                            {
                                FltAnd &= Bld.Not(Bld.Eq<byte[]>("pic", null));
                            }
                            else if (value.ToLower() == "not exist")
                            {
                                FltAnd &= Bld.Eq<byte[]>("pic", null);
                            }
                        }
                        else if (field == "tagId" || field == "gtwId")
                        {
                            string[] vals = value.Split('-');
                            string val = (vals.Length > 1) ? Convert.ToInt64(vals[2]).ToString() : value;
                            FltAnd &= Bld.Eq<string>(field, val);
                        }
                        //else if (field == "evTypeName")
                        //{
                        //    string[] vals = value.Split('-');
                        //    string val = (vals.Length > 0) ? vals[0] : value;
                        //    FltAnd &= Bld.Eq<string>("evType", val) | Bld.Eq<string>("evTypeId", val);
                        //}
                        else if (field == "mstMblNo")
                        {
                            string val = value.Substring(2, value.Length - 2);
                            FltAnd &= Bld.Eq<string>(field, val) | Bld.Eq<string>(field, value);
                        }
                        else
                        {
                            FltAnd &= Bld.Eq<string>(field, value);
                        }
                    }
                    and = true;
                }
            }

            var Flt = Bld.Empty;
            if (and && or)
            {
                Flt = FltAnd & FltOr;
            }
            else if (and && !or)
            {
                Flt = FltAnd;
            }
            else if (!and && or)
            {
                Flt = FltOr;
            }

            return Flt;
        }
    }
}