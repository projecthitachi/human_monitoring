/*
 Navicat Premium Data Transfer

 Source Server         : Local
 Source Server Type    : MongoDB
 Source Server Version : 40401
 Source Host           : localhost:27017
 Source Schema         : HumanMonitoring

 Target Server Type    : MongoDB
 Target Server Version : 40401
 File Encoding         : 65001

 Date: 28/06/2021 22:37:48
*/


// ----------------------------
// Collection structure for SmartwatchGateway
// ----------------------------
db.getCollection("SmartwatchGateway").drop();
db.createCollection("SmartwatchGateway");

// ----------------------------
// Documents of SmartwatchGateway
// ----------------------------
db.getCollection("SmartwatchGateway").insert([ {
    _id: ObjectId("5fad77bf481c0000c2003b42"),
    RecordTimestamp: ISODate("2019-11-19T09:37:34.296Z"),
    gatewayID: "3022",
    gatewayName: "SW-GW-3022",
    ipAddress: "10.0.1.143",
    location: "F03Z08"
} ]);
db.getCollection("SmartwatchGateway").insert([ {
    _id: ObjectId("5fae5257481c0000c2003b43"),
    RecordTimestamp: ISODate("2019-11-19T09:37:34.296Z"),
    gatewayID: "3016",
    gatewayName: "SW-GW-3016",
    ipAddress: "10.0.1.144",
    location: "F03Z07"
} ]);
db.getCollection("SmartwatchGateway").insert([ {
    _id: ObjectId("603e1f05ac6b00006d0004b4"),
    RecordTimestamp: ISODate("2019-11-19T09:37:34.296Z"),
    gatewayID: "3012",
    gatewayName: "SW-GW-3012",
    ipAddress: "10.0.1.145",
    location: "F03ZLL"
} ]);
db.getCollection("SmartwatchGateway").insert([ {
    _id: ObjectId("607fdb7f9b550000cc007192"),
    RecordTimestamp: ISODate("2019-11-19T09:37:34.296Z"),
    gatewayID: "3019",
    gatewayName: "SW-GW-3019",
    ipAddress: "10.0.1.148",
    location: "F07Z01"
} ]);
db.getCollection("SmartwatchGateway").insert([ {
    _id: ObjectId("607fdb8a9b550000cc007193"),
    RecordTimestamp: ISODate("2019-11-19T09:37:34.296Z"),
    gatewayID: "2983",
    gatewayName: "SW-GW-2983",
    ipAddress: "10.0.1.175",
    location: "F07Z03"
} ]);
db.getCollection("SmartwatchGateway").insert([ {
    _id: ObjectId("607fdb939b550000cc007194"),
    RecordTimestamp: ISODate("2019-11-19T09:37:34.296Z"),
    gatewayID: "3011",
    gatewayName: "SW-GW-3011",
    ipAddress: "10.0.1.177",
    location: "F07Z05"
} ]);
db.getCollection("SmartwatchGateway").insert([ {
    _id: ObjectId("607fdb9a9b550000cc007195"),
    RecordTimestamp: ISODate("2019-11-19T09:37:34.296Z"),
    gatewayID: "3015",
    gatewayName: "SW-GW-3015",
    ipAddress: "10.0.1.178",
    location: "F07Z06"
} ]);
db.getCollection("SmartwatchGateway").insert([ {
    _id: ObjectId("607fdba09b550000cc007196"),
    RecordTimestamp: ISODate("2019-11-19T09:37:34.296Z"),
    gatewayID: "3018",
    gatewayName: "SW-GW-3018",
    ipAddress: "10.0.1.179",
    location: "F07Z07"
} ]);
db.getCollection("SmartwatchGateway").insert([ {
    _id: ObjectId("607fdba79b550000cc007197"),
    RecordTimestamp: ISODate("2019-11-19T09:37:34.296Z"),
    gatewayID: "3023",
    gatewayName: "SW-GW-3023",
    ipAddress: "10.0.1.180",
    location: "F07Z08"
} ]);
db.getCollection("SmartwatchGateway").insert([ {
    _id: ObjectId("607fdbae9b550000cc007198"),
    RecordTimestamp: ISODate("2019-11-19T09:37:34.296Z"),
    gatewayID: "3021",
    gatewayName: "SW-GW-3021",
    ipAddress: "10.0.1.191",
    location: "F07Z09"
} ]);
db.getCollection("SmartwatchGateway").insert([ {
    _id: ObjectId("607fdbb69b550000cc007199"),
    RecordTimestamp: ISODate("2019-11-19T09:37:34.296Z"),
    gatewayID: "3017",
    gatewayName: "SW-GW-3017",
    ipAddress: "10.0.1.190",
    location: "F07Z10"
} ]);
db.getCollection("SmartwatchGateway").insert([ {
    _id: ObjectId("607fdbbc9b550000cc00719a"),
    RecordTimestamp: ISODate("2019-11-19T09:37:34.296Z"),
    gatewayID: "3010",
    gatewayName: "SW-GW-3010",
    ipAddress: "10.0.1.189",
    location: "F07Z11"
} ]);
db.getCollection("SmartwatchGateway").insert([ {
    _id: ObjectId("607fdbc39b550000cc00719b"),
    RecordTimestamp: ISODate("2019-11-19T09:37:34.296Z"),
    gatewayID: "3158",
    gatewayName: "SW-GW-3158",
    ipAddress: "10.0.1.188",
    location: "F07Z12"
} ]);
db.getCollection("SmartwatchGateway").insert([ {
    _id: ObjectId("607fdbc89b550000cc00719c"),
    RecordTimestamp: ISODate("2019-11-19T09:37:34.296Z"),
    gatewayID: "3156",
    gatewayName: "SW-GW-3156",
    ipAddress: "10.0.1.187",
    location: "F07Z13"
} ]);
db.getCollection("SmartwatchGateway").insert([ {
    _id: ObjectId("607fdbcb9b550000cc00719d"),
    RecordTimestamp: ISODate("2019-11-19T09:37:34.296Z"),
    gatewayID: "3157",
    gatewayName: "SW-GW-3157",
    ipAddress: "10.0.1.147",
    location: "F07ZLL"
} ]);
db.getCollection("SmartwatchGateway").insert([ {
    _id: ObjectId("607fdc359b550000cc00719e"),
    RecordTimestamp: ISODate("2019-11-19T09:37:34.296Z"),
    gatewayID: "3014",
    gatewayName: "SW-GW-3014",
    ipAddress: "10.0.1.176",
    location: "F07Z04"
} ]);
db.getCollection("SmartwatchGateway").insert([ {
    _id: ObjectId("607fdc809b550000cc00719f"),
    RecordTimestamp: ISODate("2019-11-19T09:37:34.296Z"),
    gatewayID: "3020",
    gatewayName: "SW-GW-3020",
    ipAddress: "10.0.1.149",
    location: "F07Z02"
} ]);
