/*
 Navicat Premium Data Transfer

 Source Server         : Local
 Source Server Type    : MongoDB
 Source Server Version : 40401
 Source Host           : localhost:27017
 Source Schema         : HumanMonitoring

 Target Server Type    : MongoDB
 Target Server Version : 40401
 File Encoding         : 65001

 Date: 28/06/2021 23:11:22
*/


// ----------------------------
// Collection structure for EventsLog
// ----------------------------
db.getCollection("EventsLog").drop();
db.createCollection("EventsLog");

// ----------------------------
// Collection structure for HumanConfiguration
// ----------------------------
db.getCollection("HumanConfiguration").drop();
db.createCollection("HumanConfiguration");

// ----------------------------
// Collection structure for HumanConfigurationTemp
// ----------------------------
db.getCollection("HumanConfigurationTemp").drop();
db.createCollection("HumanConfigurationTemp");

// ----------------------------
// Collection structure for Person
// ----------------------------
db.getCollection("Person").drop();
db.createCollection("Person");

// ----------------------------
// Collection structure for PersonLocation
// ----------------------------
db.getCollection("PersonLocation").drop();
db.createCollection("PersonLocation");

// ----------------------------
// Collection structure for PersonTags
// ----------------------------
db.getCollection("PersonTags").drop();
db.createCollection("PersonTags");

// ----------------------------
// Collection structure for SmartwatchDataLogs
// ----------------------------
db.getCollection("SmartwatchDataLogs").drop();
db.createCollection("SmartwatchDataLogs");
