/*
 Navicat Premium Data Transfer

 Source Server         : localhost_27017
 Source Server Type    : MongoDB
 Source Server Version : 40200
 Source Host           : localhost:27017
 Source Schema         : HumanMonitoring

 Target Server Type    : MongoDB
 Target Server Version : 40200
 File Encoding         : 65001

 Date: 14/07/2021 13:47:02
*/


// ----------------------------
// Collection structure for EventsLogTemp
// ----------------------------
db.getCollection("EventsLogTemp").drop();
db.createCollection("EventsLogTemp");

// ----------------------------
// Documents of EventsLogTemp
// ----------------------------
db.getCollection("EventsLogTemp").insert([ {
    _id: ObjectId("60ee7a46172bcf21f0489f27"),
    RecordTimestamp: ISODate("2021-07-14T05:46:46.509Z"),
    Count: NumberInt("1"),
    EventType: "Tampered alert",
    TagID: "16777215",
    personName: "Ferdiansyah Dante",
    NRIC: "Person05",
    Temp: "26.8",
    HR: NumberInt("255"),
    Location: "F03Z08",
    Tampered: "Yes",
    Acknowledge: "No",
    ActionRemarks: "Sent response team",
    Status: "Pending",
    Geofencing: "No"
} ]);
