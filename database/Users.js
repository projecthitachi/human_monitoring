/*
 Navicat Premium Data Transfer

 Source Server         : Local
 Source Server Type    : MongoDB
 Source Server Version : 40401
 Source Host           : localhost:27017
 Source Schema         : HumanMonitoring

 Target Server Type    : MongoDB
 Target Server Version : 40401
 File Encoding         : 65001

 Date: 28/06/2021 22:37:36
*/


// ----------------------------
// Collection structure for Users
// ----------------------------
db.getCollection("Users").drop();
db.createCollection("Users");

// ----------------------------
// Documents of Users
// ----------------------------
db.getCollection("Users").insert([ {
    _id: ObjectId("5dad517f28bb38b5952162c2"),
    recordID: NumberLong("0"),
    recordTimestamp: ISODate("2019-10-21T06:34:39.053Z"),
    recordStatus: NumberInt("1"),
    uuid: "HAAAA1234",
    password: "827ccb0eea8a706c4c34a16891f84e7b",
    birthDate: ISODate("1972-12-31T16:00:00.000Z"),
    age: NumberInt("46"),
    gender: "Male",
    race: "Chinese",
    raceSpecify: null,
    height: "165",
    weight: "92"
} ]);
db.getCollection("Users").insert([ {
    _id: ObjectId("5daec601e55cf371160993f5"),
    recordID: NumberLong("0"),
    recordTimestamp: ISODate("2019-10-22T09:04:01.722Z"),
    recordStatus: NumberInt("1"),
    uuid: "H00004",
    password: "827ccb0eea8a706c4c34a16891f84e7b",
    birthDate: ISODate("1993-11-30T17:00:00.000Z"),
    age: NumberInt("25"),
    gender: "Male",
    race: "Japanese",
    raceSpecify: "",
    height: "175.0",
    weight: "80.0"
} ]);
db.getCollection("Users").insert([ {
    _id: ObjectId("5dd904cd165935568262715f"),
    recordID: NumberLong("0"),
    recordTimestamp: ISODate("2019-11-23T10:07:09.847Z"),
    recordStatus: NumberInt("0"),
    uuid: "H00114",
    password: "e10adc3949ba59abbe56e057f20f883e",
    birthDate: ISODate("1974-12-31T16:00:00.000Z"),
    age: NumberInt("44"),
    gender: "Female",
    race: "Chinese",
    raceSpecify: "",
    height: "168.0",
    weight: "60.0"
} ]);
